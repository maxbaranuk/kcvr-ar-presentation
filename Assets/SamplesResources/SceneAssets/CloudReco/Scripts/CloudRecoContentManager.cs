﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Meta.DataClasses.Merchandising.Products;
using Meta.DataClasses.Models;
using Models;
using Smooth.Foundations.Algebraics;
using Smooth.Slinq;
using UnityEngine;
using Utils.Algebraic;
using Vuforia;

namespace SamplesResources.SceneAssets.CloudReco.Scripts
{
    public class CloudRecoContentManager : MonoBehaviour
    {
        public ProductDetailPanel Panel;
        public LibraryList LibraryList;
        public ContentPositioningBehaviour PlaneBeh;
        private Product _currentProduct;
        private static CloudRecoContentManager _instance;
        public static CloudRecoContentManager Instance => _instance;
        public GameObject ItemsHolder;
        public SceneModel CurrentModel;
        
        private void Awake()
        {
            if (_instance == null)
                _instance = GetComponent<CloudRecoContentManager>();

        }

        private async void Start()
        {
            await  NetworkProvider.Autorization("ciklum_maxim_alekseenko", "123456Qw")
                .ContinueWithAsync(s => NetworkProvider.Token = s);
            AssetProvider.Load();
        }

        public async void HandleTargetFinderResult(Vuforia.TargetFinder.TargetSearchResult targetSearchResult)
        {
            Panel.SetLoading();
            var product = await MetaRepository.GetProduct(new Uri($"https://qa.vrinfinity.com/api/meta/products/{targetSearchResult.MetaData}"));
            if (product.IsError)
            {
                Panel.SetError(product.Error);
                return;
            }

            _currentProduct = product.Value;
            Panel.SetData(product.Value);
        }
  
        [UsedImplicitly]
        public void AddCurrentToLibrary()
        {
            AddProductToLibrary(_currentProduct);
            AssetProvider.Save();
        }
        
        public static void AddProductToLibrary(Product product)
        {
            _instance.LibraryList.AddItem(product);
            AssetProvider.LibraryProducts.Add(product);
        }

        public void ShowTargetInfo(bool showInfo)
        {
            Panel.gameObject.SetActive(showInfo);
        }
    }   
}
