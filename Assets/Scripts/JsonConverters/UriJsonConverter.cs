﻿using System;
using Newtonsoft.Json;
using UnityEngine;

namespace Meta.JsonConverters
{
    public class UriJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Uri);
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.String:
                    return new Uri((string)reader.Value);
                case JsonToken.Null:
                    return null;
                case JsonToken.None:
                    return null;
                default:
                    var msg = $"Unable to deserialize Uri from token type {reader.TokenType}";
                    throw new InvalidOperationException(msg);
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (null == value)
            {
                writer.WriteNull();
                return;
            }

            var uri = value as Uri;
            if (uri != null)
            {
                writer.WriteValue(uri.OriginalString);
                return;
            }
            var msg = $"Unable to serialize {value.GetType()} with {typeof (UriJsonConverter)}";
            throw new InvalidOperationException(msg);
        }     
    }
}