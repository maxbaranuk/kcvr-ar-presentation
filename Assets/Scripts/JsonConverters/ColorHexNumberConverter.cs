﻿using System;
using System.Globalization;
using Newtonsoft.Json;
using UnityEngine;
using Utils.Unity.Colors;

namespace Meta.JsonConverters
{
    public class ColorHexNumberConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(ColorConverter.ColorToHex((Color)value));
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (int) || objectType == typeof (Color);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return ColorConverter.HexToColor((string)reader.Value);
        }
    }
}