﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Meta.JsonConverters
{
    public class RectToUvJsonConverter : JsonConverter
    {
        private const string X_POSITION_PROPERTY_NAME = "x";
        private const string Y_POSITION_PROPERTY_NAME = "y";
        private const string WIDTH_PROPERTY_NAME = "w";
        private const string HEIGHT_PROPERTY_NAME = "z";

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (Rect);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);

            var xPos = jsonObject.Property(X_POSITION_PROPERTY_NAME).Value.ToObject<float>();
            var yPos = jsonObject.Property(Y_POSITION_PROPERTY_NAME).Value.ToObject<float>();
            var width = jsonObject.Property(WIDTH_PROPERTY_NAME).Value.ToObject<float>();
            var height = jsonObject.Property(HEIGHT_PROPERTY_NAME).Value.ToObject<float>();

            return new Rect(xPos, yPos, width, height);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var vector = (Rect) value;
            var jsonObject = new JObject
            {
                {"@type", "Vector"},
                {X_POSITION_PROPERTY_NAME, vector.x},
                {Y_POSITION_PROPERTY_NAME, vector.y},
                {WIDTH_PROPERTY_NAME, vector.width},
                {HEIGHT_PROPERTY_NAME, vector.height}
            };
            jsonObject.WriteTo(writer);
        }
    }
}