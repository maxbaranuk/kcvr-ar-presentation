﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Meta.JsonConverters
{
    public class BoundsJsonConverter : JsonConverter
    {
        private const string MIN_VECTOR_NAME = "minVector";
        private const string MAX_VECTOR_NAME = "maxVector";

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Bounds);
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);

            var minVector = Vector3JsonConverter.Parse(jsonObject.Property(MIN_VECTOR_NAME));
            var maxVector = Vector3JsonConverter.Parse(jsonObject.Property(MAX_VECTOR_NAME));

            return new Bounds((minVector + maxVector) / 2, maxVector - minVector);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var bounds = (Bounds) value;
            var minVector = bounds.center - bounds.size/2;
            var maxVector = bounds.center + bounds.size/2;

            serializer.Converters.Add(new Vector3JsonConverter());

            writer.WriteStartObject();
            writer.WritePropertyName("@type");
            writer.WriteValue("BoundingBox");
            writer.WritePropertyName(MIN_VECTOR_NAME);
            serializer.Serialize(writer, minVector);
            writer.WritePropertyName(MAX_VECTOR_NAME);
            serializer.Serialize(writer, maxVector);
            writer.WriteEndObject();
        }     
    }
}