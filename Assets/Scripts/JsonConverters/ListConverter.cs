﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Smooth.Pools;
using Smooth.Slinq;

namespace Meta.JsonConverters
{
    public class ListConverter<T, TConverter> : JsonConverter where TConverter : JsonConverter, new()
    {
        private static readonly TConverter _elementConverter;
        private static readonly JsonSerializer _serializer;

        static ListConverter()
        {
            _elementConverter = new TConverter();
            _serializer = new JsonSerializer();
            _serializer.Converters.Add(_elementConverter);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof (List<>) &&
                   objectType.GetGenericArguments().Single() == typeof (T);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var list = (List<T>) value;
            _serializer.Serialize(writer, list);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return null;

            if (reader.TokenType != JsonToken.StartArray)
                throw new InvalidOperationException($"Excepted StartArray token, received: {reader.TokenType}");

            var collection = existingValue as ICollection<T> ?? ListPool<T>.Instance.Borrow();

            var array = JArray.Load(reader);
            return array.Slinq()
                .Select(Deserialize, serializer)
                .AddTo(collection);
        }

        private T Deserialize(JToken token, JsonSerializer serializer)
        {
            if (!_elementConverter.CanRead)
                return token.Value<T>();

            using (var reader = token.CreateReader())
            {
                reader.Read();
                return (T) _elementConverter.ReadJson(reader, typeof (T), null, serializer);
            }
        }
    }
}