﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Smooth.Algebraics;
using Utils;

namespace Meta.JsonConverters
{
    public class EitherJsonConverter<TLeft, TRight> : JsonConverter
    {
        private const string LEFT_OPTION_NAME = "left";
        private const string RIGHT_OPTION_NAME = "right";

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var either = (Either<TLeft, TRight>) value;
            writer.WriteStartObject();
            if (either.isLeft)
            {
                writer.WritePropertyName(LEFT_OPTION_NAME);
                serializer.Serialize(writer, either.leftValue);
            }
            else
            {
                writer.WritePropertyName(RIGHT_OPTION_NAME);
                serializer.Serialize(writer, either.rightValue);
            }
            writer.WriteEndObject();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (Either<TLeft, TRight>);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jObject = JObject.Load(reader);
            var leftProperty = jObject.Property(LEFT_OPTION_NAME);
            if (leftProperty != null)
            {
                var lefObject = serializer.Deserialize<TLeft>(leftProperty.Value.CreateReader());
                return Either<TLeft, TRight>.Left(lefObject);
            }
            else
            {                
                var rightProperty = jObject.Property(RIGHT_OPTION_NAME);
                var rightObject = serializer.Deserialize<TRight>(rightProperty.Value.CreateReader());
                return Either<TLeft, TRight>.Right(rightObject);
            }
        }
    }
}