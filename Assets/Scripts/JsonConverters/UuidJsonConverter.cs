﻿using System;
using Newtonsoft.Json;
using Utils;

namespace Meta.JsonConverters
{
    public class UuidJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var uuid = (Uuid) value;
            writer.WriteValue(uuid.ToString());
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (Uuid);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return Uuid.Create((string) reader.Value);
        }
    }
}