﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utils.Collections;

namespace Meta.JsonConverters
{
    public class EmbeddedConverter<T> : JsonConverter
    {
        private static readonly Dictionary<Type, string> FIELD_NAMES = new Dictionary<Type, string>();
        private static readonly JsonSerializer CleanSerializer = new JsonSerializer();

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (List<T>);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jArrayDictionary =
                (Dictionary<string, JArray>)
                    serializer.Deserialize(reader, typeof (Dictionary<string, JArray>));

            var fieldName = jArrayDictionary.Keys.Single();
            FIELD_NAMES[objectType] = fieldName;

            return JsonConvert.DeserializeObject<List<T>>(jArrayDictionary[fieldName].ToString());
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(FIELD_NAMES.TryGet(value.GetType()).ValueOr("items"));
            CleanSerializer.Serialize(writer, value);
            writer.WriteEndObject();
        }
    }
}