﻿using System;
using Newtonsoft.Json;
using Utils;

namespace Meta.JsonConverters
{
    public class UniqueIdJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var id = (UniqueId) value;
            writer.WriteValue(id.ToString());
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof (UniqueId) == objectType;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return new UniqueId((string)reader.Value);
        }
    }
}