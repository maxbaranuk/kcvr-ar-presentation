﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Smooth.Algebraics;
using UnityEngine;

namespace Meta.JsonConverters
{
    public class OptionNullConverter<T> : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Option<>) &&
                   objectType.GetGenericArguments().Single() == typeof(T);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var option = (Option<T>)value;
            if(option.isSome)
                JToken.FromObject(option.value).WriteTo(writer);
            else
                writer.WriteNull();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return Option<T>.None;           
            return serializer.Deserialize<T>(reader).ToSome();
        }
    }

    public class OptionNullConverter<T, TConverter> : JsonConverter where TConverter : JsonConverter, new()
    {
        private static readonly TConverter _converter = new TConverter();

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Option<>) &&
                   objectType.GetGenericArguments().Single() == typeof(T);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var option = (Option<T>)value;
            if (option.isNone)
            {
                writer.WriteNull();
                return;
            }

            if (_converter.CanWrite)
                _converter.WriteJson(writer, option.value, serializer);
            else
                JToken.FromObject(option.value).WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return Option<T>.None;

            return _converter.CanRead
                ? ((T)_converter.ReadJson(reader, objectType, existingValue, serializer)).ToSome()
                : serializer.Deserialize<T>(reader).ToSome();
        }
    }
}