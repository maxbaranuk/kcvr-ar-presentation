﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Meta.JsonConverters
{
    public class Vector3JsonConverter : JsonConverter
    {
        private const string X_PROPERTY_NAME = "x";
        private const string Y_PROPERTY_NAME = "y";
        private const string Z_PROPERTY_NAME = "z";

        private static readonly Vector3JsonConverter Vector3Converter = new Vector3JsonConverter();

        public static Vector3 Parse(JProperty property)
        {
            var reader = property.Value.CreateReader();
            return (Vector3)Vector3Converter.ReadJson(reader, typeof(Vector3), null, null);
        }
 
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (Vector3);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);

            var x = jsonObject.Property(X_PROPERTY_NAME).Value.ToObject<float>();
            var y = jsonObject.Property(Y_PROPERTY_NAME).Value.ToObject<float>();
            var z = jsonObject.Property(Z_PROPERTY_NAME).Value.ToObject<float>();

            return new Vector3(x, y, z);
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var vector = (Vector3) value;
            var jsonObject = new JObject
            {
                {X_PROPERTY_NAME, vector.x},
                {Y_PROPERTY_NAME, vector.y},
                {Z_PROPERTY_NAME, vector.z}
            };
            jsonObject.WriteTo(writer);
        }
    }
}