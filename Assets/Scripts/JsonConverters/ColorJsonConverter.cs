﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Meta.JsonConverters
{
    public class ColorJsonConverter : JsonConverter
    {
        private const string R_PROPERTY_NAME = "r";
        private const string G_PROPERTY_NAME = "g";
        private const string B_PROPERTY_NAME = "b";
        private const string A_PROPERTY_NAME = "a";

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var color = (Color)value;
            var jsonObject = new JObject
            {
                {R_PROPERTY_NAME, color.r},
                {G_PROPERTY_NAME, color.g},
                {B_PROPERTY_NAME, color.b},
                {A_PROPERTY_NAME, color.a}
            };
            jsonObject.WriteTo(writer);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Color);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);

            var r = jsonObject.Property(R_PROPERTY_NAME).Value.ToObject<float>();
            var g = jsonObject.Property(G_PROPERTY_NAME).Value.ToObject<float>();
            var b = jsonObject.Property(B_PROPERTY_NAME).Value.ToObject<float>();
            var a = jsonObject.Property(A_PROPERTY_NAME).Value.ToObject<float>();

            return new Color(r, g, b, a);
        }
    }
}