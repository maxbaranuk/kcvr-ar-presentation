﻿using System;
using Meta.DataClasses.Models;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Smooth.Foundations.PatternMatching;
using Utils;

namespace Meta.JsonConverters
{
    public class ModelConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof (Model).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);
            var typeString = (string)jsonObject.Property(Model.TYPE_PARAMETER_NAME);
            var model = typeString.GetEnumOrNone<ModelType>().Match().To<Model>()
                .Some().Of(ModelType.Cuboid).Return(_ => new CuboidModel())
                .Some().Of(ModelType.MeshWithInternalTextures).Return(_ => new AssetBundleModel())
                .Some().Of(ModelType.MeshWithMaterials).Return(_ => new ObjModel())
                .Some().Of(ModelType.CuboidWithAtlas).Return(_ => new CuboidModelWithAtlas())
                .Else(_ => { throw new JsonReaderException($"Can't parse Model, unknown type: \"{typeString}\"."); })
                .Result();

            using (var jsonReader = jsonObject.CreateReader())
            {
                serializer.Populate(jsonReader, model);
            }

            return model;
        }

        public override bool CanWrite => false;
    }
}