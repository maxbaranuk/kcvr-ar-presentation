﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Meta.JsonConverters
{
    public class LinkDictionaryConverter : JsonConverter
    {
        private static readonly NestedUriParser NestedUriParser = new NestedUriParser();

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (Dictionary<string, Uri>);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jObjectDictionary =
                (Dictionary<string, JObject>)
                    serializer.Deserialize(reader, typeof (Dictionary<string, JObject>));

            var uriDictionary = jObjectDictionary.ToDictionary(kvp => kvp.Key,
                kvp => JsonConvert.DeserializeObject<Uri>(kvp.Value.ToString(), NestedUriParser));

            return uriDictionary;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var links = (Dictionary<string, Uri>) value;

            writer.WriteStartObject();
            foreach (var kvp in links)
            {
                writer.WritePropertyName(kvp.Key);
                NestedUriParser.WriteJson(writer, kvp.Value, serializer);
            }
            writer.WriteEndObject();
        }
    }
}