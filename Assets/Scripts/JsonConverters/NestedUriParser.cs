﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Meta.JsonConverters
{
    public class NestedUriParser : JsonConverter
    {
        private const string HREF = "href";
        private const string ABSOLUTE_HREF = "absoluteHref";
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Uri);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null
                || reader.TokenType == JsonToken.None)
                return null;

            var jsonObject = JObject.Load(reader);
            return GetUriFromObject(jsonObject, ABSOLUTE_HREF) ?? GetUriFromObject(jsonObject, HREF);
        } 

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var uri = (Uri) value;

            writer.WriteStartObject();
            writer.WritePropertyName(HREF);
            writer.WriteValue(uri.OriginalString);
            writer.WriteEndObject();
        }

        private static Uri GetUriFromObject(JObject jsonObject, string name)
        {
            var jProperty = jsonObject.Property(name);
            if (jProperty == null
                || jProperty.Type == JTokenType.None
                || jProperty.Type == JTokenType.Null
                || jProperty.Value.Value<string>() == null)
                return null;

            return new Uri(jProperty.Value.Value<string>());
        }
    }
}