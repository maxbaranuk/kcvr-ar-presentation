﻿using System;
using Newtonsoft.Json;
using UnityEngine;
using Utils.Unity.Colors;

namespace Meta.JsonConverters
{
    public class ColorNumberConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((Color) value).ToIntColor());
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (int) || objectType == typeof (Color);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return ((int)(long)reader.Value).ToColor();
        }
    }
}