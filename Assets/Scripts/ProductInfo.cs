﻿using TMPro;
using UniRx;
using UnityEngine;

public class ProductInfo : MonoBehaviour
{

	public string Text;

	private static GameObject _infoCanvasPrefab;
	private LineRenderer _line;
	private GameObject _infoCanvas;

	private void Start ()
	{
		if (_infoCanvasPrefab == null)
			_infoCanvasPrefab = Resources.Load<GameObject>("infoCanvas");

		_infoCanvas = Instantiate(_infoCanvasPrefab);
		_infoCanvas.transform.parent = transform;
		_infoCanvas.transform.localPosition = new Vector3(Random.Range(-0.1f, 0.1f), 0, 0.2f);
		_infoCanvas.transform.localScale = new Vector3(-0.1f, 0.1f, 0.1f);
		_line = _infoCanvas.GetComponent<LineRenderer>();

		_infoCanvas.GetComponentInChildren<TextMeshPro>().text = Text;
		UiController.InfoPresent.Subscribe(b => _infoCanvas.SetActive(b));
	}


	private void Update () {
		_line.SetPositions(new[] { transform.position, _infoCanvas.transform.position });
		_infoCanvas.transform.LookAt(new Vector3(Camera.main.transform.position.x, 
			_infoCanvas.transform.position.y, Camera.main.transform.position.z));

	}
}
