﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using Meta.DataClasses.Merchandising.Products;
using Meta.DataClasses.Models;
using Models;
using SamplesResources.SceneAssets.CloudReco.Scripts;
using Smooth.Foundations.Algebraics;
using Smooth.Slinq;
using UnityEngine;
using Utils.Algebraic;

public class ContentBuilder : MonoBehaviour {

	private static ContentBuilder _instance;
	public static ContentBuilder Instance => _instance;
	
	private GameObject _moveGizmoPrefab;
	private GameObject _rotateGizmoPrefab;
	
	public GameObject ItemsHolder;

	private void Awake () {
		if (_instance == null)
			_instance = GetComponent<ContentBuilder>();
		
		_moveGizmoPrefab = Resources.Load<GameObject>("Move");
		_rotateGizmoPrefab = Resources.Load<GameObject>("Rotate");
	}
	
	[UsedImplicitly]
	public void CreateResourcesModel(string modelPrefabName)
	{
		if (CloudRecoContentManager.Instance.ItemsHolder == null)
			CloudRecoContentManager.Instance.ItemsHolder = GameObject.Find("ModelsHolder");
		
		if (CloudRecoContentManager.Instance.CurrentModel != null)
			Destroy(CloudRecoContentManager.Instance.CurrentModel.gameObject);

		var modelPrefab = Resources.Load<GameObject>(modelPrefabName);
		var model = Instantiate(modelPrefab);
		CloudRecoContentManager.Instance.CurrentModel = model.GetComponent<SceneModel>();
		model.transform.position = new Vector3(Camera.main.transform.position.x,
			CloudRecoContentManager.Instance.ItemsHolder.transform.position.y, Camera.main.transform.position.z);
		var player = Camera.main;
		var dir = Vector3.ProjectOnPlane(player.transform.forward, Vector3.up).normalized * 2;
		model.transform.Translate(dir);
		model.transform.LookAt(new Vector3(player.transform.position.x, model.transform.position.y, player.transform.position.z));
		UiController.Instance.CloseList();
	}
	
	public static async Task CreateProductModel(Product product)
	{
		if (_instance.ItemsHolder == null)
			_instance.ItemsHolder = GameObject.Find("ModelsHolder");
            
		if (CloudRecoContentManager.Instance.CurrentModel != null)
			Destroy(CloudRecoContentManager.Instance.CurrentModel.gameObject);
            
		var model = await product.ModelInformation
			.ContinueWithAsync(ResolveProductModel);
		if (model.IsError)
			return;

		model.Value.AddComponent<BoxCollider>();
		var size = model.Value.GetComponent<BoxCollider>().size;
		var move = Instantiate(_instance._moveGizmoPrefab, new Vector3(0, -size.y / 2, 0), Quaternion.identity, model.Value.transform);
		move.transform.localScale = size;
		move.name = "Move";
		var rotate = Instantiate(_instance._rotateGizmoPrefab, new Vector3(0, -size.y / 2, 0), Quaternion.identity, model.Value.transform);
		rotate.transform.localScale = size;
		rotate.name = "Rotate";
            
		model.Value.AddComponent<SceneModel>();
            
		CloudRecoContentManager.Instance.CurrentModel = model.Value.GetComponent<SceneModel>();
		model.Value.transform.position = Camera.main.transform.position;
		var player = Camera.main;
		var dir = player.transform.forward;
		model.Value.transform.Translate(dir);
		UiController.Instance.CloseList();
	}
	
	private static Task<ValueOrError<GameObject>> ResolveProductModel(ModelInformation modelInformation)
	{
		var model = modelInformation.ModelSet.Slinq()
			.First(m => m.Type == ModelType.Cuboid | m.Type == ModelType.CuboidWithAtlas);
        
		return model == null 
			? Task.FromResult(ValueOrError<GameObject>.FromError("No compatible model")) 
			: LoadInTwoOrHalfD(modelInformation);
	}
    
	private static Task<ValueOrError<GameObject>> LoadInTwoOrHalfD(ModelInformation modelInformation)
	{
		var builder = CuboidModelBuilder.TryGetModel(modelInformation);
		return builder.ValueOr(() => Task.FromResult(ValueOrError<GameObject>.FromError("Cuboid model wasn't loaded")));
	}
}
