﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Meta.DataClasses.Merchandising.Products;
using Newtonsoft.Json;
using Smooth.Foundations.Algebraics;
using Utils.Algebraic;
using Utils.Collections;


public static class MetaRepository {
	
	private static class AwaitBuffer<T>
	{
		public static readonly Dictionary<Uri, Task<ValueOrError<T>>> Waiting =
			new Dictionary<Uri, Task<ValueOrError<T>>>();
	}

	public static Task<ValueOrError<Product>> GetProduct(Uri uri)
	{
		return Get<Product>(uri);
	}

	private static Task<ValueOrError<T>> Get<T>(Uri uri) where T : Metas.Meta
	{
		return AwaitBuffer<T>.Waiting.GetOrAdd(uri, Download<T>, uri);
	}
	
	private static async Task<ValueOrError<T>> Download<T>(Uri uri) where T : Metas.Meta
	{
		var rawResult = await NetworkProvider.GetText(uri)
			.ContinueWithAsync(JsonConvert.DeserializeObject<T>)
			.Where(r => r != null, _ => $"Can't deserialize {typeof(T).Name}, result is null.");
		return rawResult.IsError
			? ValueOrError<T>.FromError($"Unable to get meta of {typeof(T).Name} because of error: {rawResult.Error}; uri: {uri}")
			: rawResult;
	}
}
