﻿using Meta.DataClasses.Merchandising.Products;
using UnityEngine;
using UnityEngine.UI;

public class ProductDetailPanel : MonoBehaviour
{
	private Text _name;
	private Text _brand;
	private Text _category;
	private GameObject _saveButton;
	
	private void Awake()
	{
		_name = transform.Find("Name").GetComponent<Text>();
		_brand = transform.Find("Brand").GetComponent<Text>();
		_category = transform.Find("Category").GetComponent<Text>();
		_saveButton = transform.Find("Save").gameObject;
		gameObject.SetActive(false);
	}

	public void SetLoading()
	{
		_name.text = "Loading...";
		_brand.gameObject.SetActive(false);
		_category.gameObject.SetActive(false);
	}

	public void SetData(Product product)
	{
		_brand.gameObject.SetActive(true);
		_category.gameObject.SetActive(true);
		_name.text = product.DisplayName;
		_brand.text = product.Brand;
		_category.text = product.Category;
		_saveButton.SetActive(!AssetProvider.LibraryProducts.Contains(product));
	}
	
	public void SetError(string errorMessage)
	{
		_name.text = errorMessage;
	}
}
