﻿using UnityEngine;

public class PreviewItem : MonoBehaviour
{

	private MeshRenderer _renderrer;
	private Material _previewMaterial;
	private Material _originMaterial;

	void Awake ()
	{
		_renderrer = GetComponent<MeshRenderer>();
		_originMaterial = _renderrer.material;
		_previewMaterial = Resources.Load<Material>("PlaceHolder");
	}

	public void Show()
	{
		_renderrer.material = _previewMaterial;
	}
	
	public void Hide()
	{
		_renderrer.material = _originMaterial;
	}
}
