﻿using System;
using Meta.DataClasses.Merchandising.Products;
using UnityEngine;
using UnityEngine.UI;
using Utils.Algebraic;

public class ModelButton : MonoBehaviour
{
	
	public async void SetProduct(Product product)
	{
		var thumbLink = product.GetThumbnail();
		
		if (thumbLink.isNone)
			return;

		var sprite = await AssetProvider.GetTexture(new Uri(thumbLink.value))
			.ContinueWithAsync(tex =>
				Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f));

		if (sprite.IsError)
			return;
		
		transform.Find("Image").GetComponent<Image>().sprite = sprite.Value;
	}
}
