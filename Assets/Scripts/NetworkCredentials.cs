﻿using Newtonsoft.Json;

public class NetworkCredentials
{
	[JsonProperty("userName")]
	private string _login;
	[JsonProperty("password")]
	private string _password;

	public NetworkCredentials(string login, string password)
	{
		_login = login;
		_password = password;
	}
}
