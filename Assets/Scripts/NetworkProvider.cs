﻿using System;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Smooth.Foundations.Algebraics;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkProvider : MonoBehaviour
{

	public static string Token;

	public static async Task<ValueOrError<string>> Autorization(string login, string password)
	{
		var credentialsString = JsonConvert.SerializeObject(new NetworkCredentials(login, password));
		Debug.Log(credentialsString);

		using (var www = new UnityWebRequest("https://qa.vrinfinity.com/api/auth/login", UnityWebRequest.kHttpVerbPOST))
		{
			www.redirectLimit = 0;
			byte[] bytes= Encoding.UTF8.GetBytes(credentialsString);
			var uH = new UploadHandlerRaw(bytes) {contentType = "application/json"};
			www.uploadHandler= uH;
			www.downloadHandler = new DownloadHandlerBuffer();
			www.SendWebRequest();
			while (!www.isDone)
			{
				await Task.Delay(10);
			}

			return www.isNetworkError || www.isHttpError
				? ValueOrError<string>.FromError($"Login error {www.error}")
				: ValueOrError<string>.FromValue((string) JObject.Parse( www.downloadHandler.text)["token"]);
		}
	}
	
	public static async Task<ValueOrError<string>> GetText(Uri uri)
	{
		using (var www = new UnityWebRequest(uri.AbsoluteUri, UnityWebRequest.kHttpVerbGET))
		{
			www.SetRequestHeader("X-JWT-Authorization","Bearer "+ Token);
			www.downloadHandler = new DownloadHandlerBuffer();
			www.SendWebRequest();
			while (!www.isDone)
			{
				await Task.Delay(10);
			}
			
			if (www.isNetworkError || www.isHttpError)
				return ValueOrError<string>.FromError($"Download error {www.error}");
			
			AssetProvider.AddText(uri, www.downloadHandler.text);
			
			return ValueOrError<string>.FromValue(www.downloadHandler.text);
		}
	}
	
	public static async Task<ValueOrError<Texture2D>> GetTexture(Uri uri)
	{
		using (var www = new UnityWebRequest(uri.AbsoluteUri, UnityWebRequest.kHttpVerbGET))
		{
			www.SetRequestHeader("X-JWT-Authorization","Bearer "+ Token);
			www.downloadHandler = new DownloadHandlerBuffer();
			www.SendWebRequest();
			while (!www.isDone)
			{
				await Task.Delay(10);
			}

			if (www.isNetworkError || www.isHttpError)
				return ValueOrError<Texture2D>.FromError($"Download error {www.error}");
			
			var texure = new Texture2D(4, 4);
			texure.LoadImage(www.downloadHandler.data);
			AssetProvider.AddTexture(uri, texure);
			return ValueOrError<Texture2D>.FromValue(texure);
		}
	}
	
	public static ValueOrError<byte[]> GetBytes(Uri uri)
	{
		using (var www = new UnityWebRequest(uri.AbsoluteUri, UnityWebRequest.kHttpVerbGET))
		{
			www.SetRequestHeader("X-JWT-Authorization","Bearer "+ Token);
			www.downloadHandler = new DownloadHandlerBuffer();
			www.SendWebRequest();
			while (!www.isDone)
			{
				Task.Delay(10);
			}
			
			return www.isNetworkError || www.isHttpError
				? ValueOrError<byte[]>.FromError($"Download error {www.error}")
				: ValueOrError<byte[]>.FromValue(www.downloadHandler.data);
		}
	}
}
