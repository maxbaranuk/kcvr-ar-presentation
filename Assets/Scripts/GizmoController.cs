﻿using SamplesResources.SceneAssets.CloudReco.Scripts;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

public class GizmoController : MonoBehaviour {

	public enum GizmoType
	{
		MoveX, MoveY, MoveZ, RotateX, RotateY, RotateZ
	}
	
	private Vector3 _lastRayPosition = Vector3.zero;
	private Vector3 _lastTouchPosition = Vector3.zero;
	private bool _moving;
	private SceneModel _model;
	public GizmoType Type;

	private Vector3 _planePosition;
	// Use this for initialization
	void Start ()
	{
		_planePosition = CloudRecoContentManager.Instance.PlaneBeh.AnchorStage.transform.position;
		_model = transform.parent.parent.GetComponent<SceneModel>();
		var layerMask = 1 << 9;
		Observable.EveryUpdate()
			.TakeUntilDestroy(this)
			.Subscribe(_ =>
			{
#if UNITY_EDITOR
				if (Input.GetMouseButtonDown(0))
				{
					var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;
					if (Physics.Raycast(ray, out hit, 100, layerMask) && !EventSystem.current.IsPointerOverGameObject())
					{
						var gizmo = hit.collider.transform.parent.GetComponent<GizmoController>();
						if (gizmo != null && gizmo == this)
							_moving = true;
						_lastRayPosition = hit.point;
						_lastTouchPosition = Input.mousePosition;
					}
				}

				if (Input.GetMouseButton(0))
				{
					if (!_moving)
						return;
					
					var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;
					if (!Physics.Raycast(ray, out hit, 100, layerMask) || EventSystem.current.IsPointerOverGameObject()) return;

					Vector3 delta;
					Vector3 dir;
					switch (Type)
					{
						case GizmoType.MoveX:
							delta = _lastRayPosition - hit.point;
							dir = Vector3.Project(delta, _model.transform.right);
							_model.transform.position -= dir;
							break;
						case GizmoType.MoveY:
							delta = _lastRayPosition - hit.point;
							dir = Vector3.Project(delta, _model.transform.up);
							_model.transform.position -= dir;
							
							if (_model.transform.position.y < _planePosition.y)
								_model.transform.position = new Vector3(_model.transform.position.x, _planePosition.y, _model.transform.position.z);
							break;
						case GizmoType.MoveZ:
							delta = _lastRayPosition - hit.point;
							dir = Vector3.Project(delta, _model.transform.forward);
							_model.transform.position -= dir;
							break;
						case GizmoType.RotateX:
							break;
						case GizmoType.RotateY:
							delta = _lastTouchPosition - Input.mousePosition;
							_model.transform.Rotate(_model.transform.up, delta.x);
							break;
						case GizmoType.RotateZ:
							break;
					}
					_lastRayPosition = hit.point;
					_lastTouchPosition = Input.mousePosition;
				}

				if (Input.GetMouseButtonUp(0))
				{
					_moving = false;
					_lastRayPosition = Vector3.zero;
				}				
#else

		if (Input.touchCount < 1)
			return;
	
                if (Input.GetTouch(0).phase == TouchPhase.Began)
				{
					var ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
					RaycastHit hit;
					if (Physics.Raycast(ray, out hit, 100, layerMask) && !EventSystem.current.IsPointerOverGameObject())
					{
						var gizmo = hit.collider.transform.parent.GetComponent<GizmoController>();
						if (gizmo != null && gizmo == this)
							_moving = true;
						_lastRayPosition = hit.point;
						_lastTouchPosition = Input.GetTouch(0).position;
					}
				}

				if (Input.GetTouch(0).phase == TouchPhase.Moved)
				{
					if (!_moving)
						return;
					var ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
					RaycastHit hit;
					if (!Physics.Raycast(ray, out hit, 100, layerMask) || EventSystem.current.IsPointerOverGameObject()) return;

					Vector3 delta;
					Vector3 dir;
					switch (Type)
					{
						case GizmoType.MoveX:
							delta = _lastRayPosition - hit.point;
							dir = Vector3.Project(delta, _model.transform.right);
							_model.transform.position -= dir;
							break;
						case GizmoType.MoveY:
							delta = _lastRayPosition - hit.point;
							dir = Vector3.Project(delta, _model.transform.up);
							_model.transform.position -= dir;
							
							if (_model.transform.position.y < _planePosition.y)
								_model.transform.position = new Vector3(_model.transform.position.x, _planePosition.y, _model.transform.position.z);
							break;
						case GizmoType.MoveZ:
							delta = _lastRayPosition - hit.point;
							dir = Vector3.Project(delta, _model.transform.forward);
							_model.transform.position -= dir;
							break;
						case GizmoType.RotateX:
							break;
						case GizmoType.RotateY:
							delta = Input.GetTouch(0).deltaPosition;
							_model.transform.Rotate(_model.transform.up, delta.x);
							break;
						case GizmoType.RotateZ:
							break;
					}
					_lastRayPosition = hit.point;
					_lastTouchPosition = Input.GetTouch(0).position;
				}

				if (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled)
				{
                 	Debug.Log("Pointer up");
					_moving = false;
					_lastRayPosition = Vector3.zero;
				}
#endif
			});
	}
}
