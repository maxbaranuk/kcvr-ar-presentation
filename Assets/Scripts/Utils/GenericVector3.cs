﻿namespace Utils
{
    public struct GenericVector3<T>
    {
        public readonly T X;
        public readonly T Y;
        public readonly T Z;

        public GenericVector3(T x, T y, T z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public override string ToString()
        {
            return $"({X}; {Y}; {Z})";
        }
    }
}