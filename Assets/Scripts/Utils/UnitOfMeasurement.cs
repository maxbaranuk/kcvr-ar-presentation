﻿using Meta.DataClasses;
using Smooth.Foundations.PatternMatching.GeneralMatcher;
using UnityEngine;

namespace Start.Settings
{
    public enum UnitOfMeasurement
    {
        Centimeters,
        Inches
    }

    public static class UnitOfMeasurementMath
    {
        private const float CENTIMETERS_IN_INCH = 2.54f;
        private const float INCHES_IN_CENTIMETER = 1/CENTIMETERS_IN_INCH;
        private const float CENTIMETERS_IN_METER = 100f;
        private const float MILLIMETRES_IN_METER = 1000f;
        private const float INCHES_IN_METER = INCHES_IN_CENTIMETER*CENTIMETERS_IN_METER;

        private const float METRE_MULTIPLIER = 1f;
        private const float INCH_MULTIPLIER = CENTIMETERS_IN_INCH/CENTIMETERS_IN_METER;
        private const float CENTIMETRE_MULTIPLIER = 1/CENTIMETERS_IN_METER;
        private const float MILLIMETRE_MULTIPLIER = 1/MILLIMETRES_IN_METER;
        private const float FOOT_MULTIPLIER = 0.3048f;

        private const float PIXELS_MULTIPLIER = INCHES_IN_METER * 20;

        // TODO: try to remove 'UnitOfLength' and use 'UnitOfMeasurement' instead
        public static float GetMultiplierFromUnitOfLength(this UnitOfLength unitOfLength)
        {
            return unitOfLength.Match().To<float>()
                .With(UnitOfLength.METRE).Return(METRE_MULTIPLIER)
                .With(UnitOfLength.INCH).Return(INCH_MULTIPLIER)
                .With(UnitOfLength.CENTIMETRE).Return(CENTIMETRE_MULTIPLIER)
                .With(UnitOfLength.MILLIMETRE).Return(MILLIMETRE_MULTIPLIER)
                .With(UnitOfLength.FOOT).Return(FOOT_MULTIPLIER)
                .Result();
        }

        public static float ToCentimeters(float value, UnitOfMeasurement current)
        {
            return current == UnitOfMeasurement.Centimeters
                ? value
                : value*CENTIMETERS_IN_INCH;
        }

        public static Vector3 ToMetric(this Vector3 vector, UnitOfLength current)
        {
            return vector*current.GetMultiplierFromUnitOfLength();
        }

        public static Bounds ToMetric(this Bounds bounds, UnitOfLength current)
        {
            return new Bounds(bounds.center*current.GetMultiplierFromUnitOfLength(), bounds.size*current.GetMultiplierFromUnitOfLength());
        }

        public static float ToMeters(float value, UnitOfMeasurement current)
        {
            return current == UnitOfMeasurement.Centimeters
                ? value/CENTIMETERS_IN_METER
                : value/INCHES_IN_METER;
        }

        public static float FromMeters(float value, UnitOfMeasurement current)
        {
            return current == UnitOfMeasurement.Centimeters
                ? value*CENTIMETERS_IN_METER
                : value*INCHES_IN_METER;
        }

//        public static float ToCurrentUnitsOfMeasurement(float centimeters)
//        {
//            return GlobalSettings.Units == UnitOfMeasurement.Centimeters
//                ? centimeters
//                : centimeters*INCHES_IN_CENTIMETER;
//        }

        public static int ToPixels(float meters)
        {
            return (int) (meters * PIXELS_MULTIPLIER);
        }

        public static string AsString(this UnitOfMeasurement unit)
        {
            return unit == UnitOfMeasurement.Centimeters
                ? "cm"
                : "in";
        }
    }
}