﻿using System;
using Meta.JsonConverters;
using Newtonsoft.Json;
using UnityEngine;

namespace Utils
{
    [Serializable, JsonObject(MemberSerialization.OptIn), JsonConverter(typeof (UniqueIdJsonConverter))]
    public struct UniqueId : IEquatable<UniqueId>
    {
        [SerializeField] private string _id;

        public UniqueId(string id)
        {
            _id = id;
        }

        public static UniqueId Generate()
            => new UniqueId {_id = Guid.NewGuid().ToString()};

        public override bool Equals(object obj)
            => obj is UniqueId && Equals((UniqueId) obj);

        public bool Equals(UniqueId other)
            => _id == other._id;

        public override int GetHashCode() => _id.GetHashCode();

        public override string ToString() => _id;

        public static bool operator ==(UniqueId left, UniqueId right) => left.Equals(right);

        public static bool operator !=(UniqueId left, UniqueId right) => !(left == right);
    }
}