﻿using System.Reflection;

namespace Assets.Scripts.Utils
{
    public static class ReflectionExtensions
    {
        private const BindingFlags PROTECTED_PRIVATE_FLAGS = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;

        public static void SetPropertyValue(this object obj, string propertyName, object value)
            => GetPropertyInfo(obj, propertyName, BindingFlags.Public | BindingFlags.Instance)?.SetValue(obj, value, null);

        public static void SetPropertyValueClosed(this object obj, string propertyName, object value)
            => GetPropertyInfo(obj, propertyName, PROTECTED_PRIVATE_FLAGS)?.SetValue(obj, value, null);

        public static object GetPropertyValueClosed(this object obj, string propertyName)
            => GetPropertyInfo(obj, propertyName, PROTECTED_PRIVATE_FLAGS)?.GetValue(obj, null);

        public static object GetFieldValueClosed(this object obj, string propertyName)
            => GetFieldInfo(obj, propertyName, PROTECTED_PRIVATE_FLAGS)?.GetValue(obj);

        private static FieldInfo GetFieldInfo(object obj, string propertyName, BindingFlags bindingFlags)
            => obj.GetType().GetField(propertyName, bindingFlags);

        private static PropertyInfo GetPropertyInfo(object obj, string propertyName, BindingFlags bindingFlags)
            => obj.GetType().GetProperty(propertyName, bindingFlags);

        public static void InvokePrivateMethod(this object obj, string method, params object[] args)
            => obj.GetType().GetMethod(method, BindingFlags.NonPublic | BindingFlags.Instance).Invoke(obj, args);

        public static void InvokeBasePrivateMethod(this object obj, string method, params object[] args)
            => obj.GetType().BaseType.GetMethod(method, BindingFlags.NonPublic | BindingFlags.Instance).Invoke(obj, args);

        public static object GetPrivateMethod(this object obj, string method, params object[] args)
            => obj.GetType().GetMethod(method, BindingFlags.NonPublic | BindingFlags.Instance).Invoke(obj, args);   
        
        public static void SetFieldValue(this object obj, string fieldName, object value)
            => obj.GetType().GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance).SetValue(obj, value);
    }
}
