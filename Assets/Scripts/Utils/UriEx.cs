﻿using System;
using Smooth.Algebraics;

namespace Utils
{
    public static class UriEx
    {
        public static Option<Uri> CreateOrNone(string rawUri, UriKind uriKind = UriKind.Absolute)
        {
            Uri uri;
            return Uri.TryCreate(rawUri, uriKind, out uri) 
                ? uri.ToSome() 
                : Option<Uri>.None;
        }
    }
}
