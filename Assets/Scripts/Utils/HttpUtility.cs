﻿using System;
using System.Collections.Generic;
using System.Text;
using Smooth.Slinq;

namespace Utils
{
    /// <summary>
    /// The simplest query parsing implementation, without handling edge cases, special symbols, etc.
    /// </summary>
    public static class HttpUtility
    {
        /// <summary>
        /// Set parameter for uri. Replace if it exists. All other query parameters are left as is.
        /// </summary>
        public static Uri SetQueryParameter(this Uri uri, string key, string value)
        {
            var builder = new UriBuilder(uri);
            var queryParams = new QueryParams(builder.Query);
            queryParams[key] = value;
            builder.Query = queryParams.ToString();
            return builder.Uri;
        }

        private class QueryParams : Dictionary<string, string>
        {
            public QueryParams(string query)
            {
                if (string.IsNullOrEmpty(query))
                    return;
                if(query.StartsWith("?"))
                    query = query.Substring(1);
                query.Split('&')
                    .Slinq()
                    .Select(kvp => kvp.Split('='))
                    .Where(p => p.Length == 2)
                    .ForEach((p, dic) => dic.Add(p[0], p[1]), this);
            }

            public override string ToString()
            {
                return this.Slinq()
                    .Aggregate(new StringBuilder(),
                        (builder, kvp) =>
                        {
                            if (builder.Length != 0)
                                builder.Append('&');
                            return builder.Append($"{kvp.Key}={kvp.Value}");
                        }).ToString();
            }
        }
    }
}
