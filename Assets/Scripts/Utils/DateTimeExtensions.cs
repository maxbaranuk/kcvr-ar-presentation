﻿using System;

namespace Utils
{
   public static class DateTimeExtensions
    {
       public static string ToReplayerTimeFormat(float time)
       {
            var dt = new DateTime((long)(time * 10000000));
            return dt.ToString("mm:ss:fff");
        }

        public static string ToReplayerTimeFormatFromSeconds(float seconds)
        {
            var minutes = (int) seconds / 60;
            var secondsWithoutMinutes = (int) (seconds - minutes * 60);
            return $"{minutes}:{secondsWithoutMinutes,2:00}";
        }

        public static DateTime ToDateTimeFromUnixTimeStamp(this double unixTimeStamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(unixTimeStamp);
        }

        public static double ToUnixTimeStamp(this DateTime dateTime)
        {
            return dateTime.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}
