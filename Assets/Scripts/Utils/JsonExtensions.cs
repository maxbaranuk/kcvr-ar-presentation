﻿using Newtonsoft.Json.Linq;
using Smooth.Algebraics;
using Smooth.Slinq;

namespace Utils
{
    public static class JsonExtensions
    {
        public static JProperty GetOrAddProperty(this JProperty parent, string name)
        {
            var jObject = (JObject) parent.Value;
            return jObject.GetOrAddProperty(name);
        }

        public static JProperty GetOrAddProperty(this JObject jObject, string name)
        {
            var property = jObject.Children<JProperty>().Slinq()
            .FirstOrNone((p, n) => p.Name == n, name)
            .ValueOr(nameAndObject =>
            {
                var p = new JProperty(nameAndObject.Item1);
                p.Value = new JObject();
                nameAndObject.Item2.Add(p);
                return p;
            }, Smooth.Algebraics.Tuple.Create(name, jObject));

            return property;
        }

        public static Option<string> TryGetValue(this JObject jObject, string key)
        {
            JToken jToken;

            return jObject.TryGetValue(key, out jToken)
                ? jToken.ToString().ToSome() 
                : Option<string>.None;
        }
    }
}