﻿using System;
using System.Text.RegularExpressions;
using Meta.JsonConverters;
using Newtonsoft.Json;
using Smooth.Algebraics;
using Utils.String;

namespace Utils
{
    [JsonConverter(typeof (UuidJsonConverter))]
    public class Uuid
    {
        public const string UuidPattern = @"^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$";
        private readonly string _uuid;

        public static Option<Uuid> TryCreate(string uuid) => IsValid(uuid)
            ? new Uuid(uuid).ToSome()
            : Option<Uuid>.None;

        public static Uuid Create(string uuid)
        {
            if (!IsValid(uuid))
               throw new ArgumentException("Not a Uuid: " + uuid);           
            return new Uuid(uuid);
        }

        private Uuid(string uuid)
        {
            _uuid = uuid;
        }

        public static Uuid Random()
        {
            return new Uuid(Guid.NewGuid().ToString());
        }

        public static bool IsValid(string uuid)
        {
            return uuid != null && Regex.IsMatch(uuid, UuidPattern);
        }

        public static implicit operator Uuid(string s)
        {
            return new Uuid(s);
        }

        public static implicit operator string(Uuid u)
        {
            return u.ToString();
        }

        public override string ToString()
        {
            return _uuid;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            return _uuid.EqualsIgnoringCase(((Uuid) obj)._uuid);
        }

        public override int GetHashCode()
        {
            return _uuid.GetHashCode();
        }
    }
}
