﻿namespace Meta.DataClasses
{
    public enum UnitOfLength
    {
        CENTIMETRE,
        FOOT,
        INCH,
        METRE,
        MILLIMETRE
    }
}
