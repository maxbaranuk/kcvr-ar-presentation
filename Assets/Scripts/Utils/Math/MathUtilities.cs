﻿using UnityEngine;

namespace Utils.Math
{
    public static class MathUtilities
    {
        private const float Epsilon = 1e-5f;

        public static int CountFittingsToSpace(float availableSpace, float singleSize)
        {
            return Mathf.FloorToInt(availableSpace / singleSize + 1e-5f);
        }

        public static bool NearEquals(this float f1, float f2)
        {
            return Mathf.Abs(f1 - f2) < Epsilon;
        }

        public static bool NearEquals(this float f1, float f2, float epsilon)
            => Mathf.Abs(f1 - f2) < epsilon;
        
        public static bool NearEquals(this double f1, double f2)
        {
            return System.Math.Abs(f1 - f2) < Epsilon;
        }

        public static bool NearEquals(this double f1, double f2, double epsilon)
            => System.Math.Abs(f1 - f2) < epsilon;

        public static bool NearEquals(this Vector3 v1, Vector3 v2)
        {
            return v1.x.NearEquals(v2.x)
                && v1.y.NearEquals(v2.y)
                && v1.z.NearEquals(v2.z);
        }

        public static bool NearEquals(this Quaternion q1, Quaternion q2)
        {
            return Mathf.Abs(Quaternion.Dot(q1, q2)) >= 1 - Quaternion.kEpsilon;
        }

        public static Vector3 PlaneRayIntersection(Vector3 planePoint, Vector3 planeNormal, Ray ray)
        {
            float dotNumerator = Vector3.Dot((planePoint - ray.origin), planeNormal);
            float dotDenominator = Vector3.Dot(ray.direction, planeNormal);

            if (dotDenominator != 0.0f)
            {
                return ray.origin + ray.direction.normalized * (dotNumerator / dotDenominator);
            }
            else
            {
                return planePoint;
            }
        }

        public static Vector3 Clamp(this Vector3 v, Vector3 min, Vector3 max)
        {
            return new Vector3(Mathf.Clamp(v.x, min.x, max.x),
                Mathf.Clamp(v.y, min.y, max.y),
                Mathf.Clamp(v.z, min.z, max.z));
        }

        public static Vector3 Abs(this Vector3 v)
        {
            return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
        }

        public static Vector3 Min(this Vector3 v, Vector3 other)
        {
            return new Vector3(Mathf.Min(v.x, other.x),
                Mathf.Min(v.y, other.y),
                Mathf.Min(v.z, other.z));
        }

        public static Vector3 Max(this Vector3 v, Vector3 other)
        {
            return new Vector3(Mathf.Max(v.x, other.x),
                Mathf.Max(v.y, other.y),
                Mathf.Max(v.z, other.z));
        }

        public static float MaxAxis(this Vector3 v) => Mathf.Max(v.x, Mathf.Max(v.y, v.z));
        public static float MinAxis(this Vector3 v) => Mathf.Min(v.x, Mathf.Min(v.y, v.z));

        public static bool NearEquals(this Bounds bounds, Bounds other)
        {
            return bounds.center.NearEquals(other.center)
                   && bounds.extents.NearEquals(other.extents);
        }

        public static int NearestPow2(this int n)
        {
            n -= 1;
            n |= n >> 1;
            n |= n >> 2;
            n |= n >> 4;
            n |= n >> 8;
            n |= n >> 16;
            return n + 1;
        }
        public static float ToFloorDiscretValue(this float value, float step)
        {
            return Mathf.FloorToInt(value/step)*step;
        }

        public static float ToRoundDiscretValue(this float value, float step)
        {
            return Mathf.RoundToInt(value/step)*step;
        }

        public static float ToCeilDiscreteValue(this float value, float step)
        {
            return Mathf.CeilToInt(value/step)*step;
        }

        public static float ToNonZero(this float value) => value == 0 ? 0.001f : value;
    }
}
