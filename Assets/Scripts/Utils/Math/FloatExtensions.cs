﻿using System.Globalization;
using UnityEngine;

namespace Utils.Math
{
    public static class FloatExtensions
    {
        public static string ToStringInvariant(this float number)
        {
            return number.ToString(CultureInfo.InvariantCulture);
        }
        
        public static string ToStringInvariant(this int number)
        {
            return number.ToString(CultureInfo.InvariantCulture);
        }

        public static string ToStringInvariant(this float number, string format)
        {
            return number.ToString(format, CultureInfo.InvariantCulture);
        }
        
        public static string ToStringInvariant(this double number, string format)
        {
            return number.ToString(format, CultureInfo.InvariantCulture);
        }

        public static int FloorWithTolerance(this float number)
        {
            var round = Mathf.Round(number);
            return Mathf.Approximately(round, number) ? Mathf.RoundToInt(number) : (int) number;
        }
    }
}
