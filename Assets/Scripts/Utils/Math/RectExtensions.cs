﻿using UnityEngine;

namespace Utils.Math
{
    public static class RectExtensions
    {
        public static Rect Encapsulate(this Rect rect, Rect otherRect)
        {
            var xMin = Mathf.Min(rect.xMin, otherRect.xMin);
            var yMin = Mathf.Min(rect.yMin, otherRect.yMin);
            var xMax = Mathf.Max(rect.xMax, otherRect.xMax);
            var yMax = Mathf.Max(rect.yMax, otherRect.yMax);

            return new Rect(xMin, yMin, xMax - xMin, yMax - yMin);
        }
    }
}