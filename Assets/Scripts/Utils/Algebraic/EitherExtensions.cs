﻿using Smooth.Algebraics;
using Smooth.Delegates;

namespace Utils.Algebraic
{
    public static class EitherExtensions
    {
        public static Either<T3, T4> Select<T1, T2, T3, T4>(this Either<T1, T2> either, DelegateFunc<T1, T3> firstSelector, DelegateFunc<T2, T4> secondSelector)
        {
            return either.isLeft 
                ? Either<T3, T4>.Left(firstSelector(either.leftValue)) 
                : Either<T3, T4>.Right(secondSelector(either.rightValue));
        }
    }
}