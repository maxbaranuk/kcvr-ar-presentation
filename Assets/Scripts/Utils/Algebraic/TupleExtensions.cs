﻿using System;
using Smooth.Algebraics;
using Smooth.Delegates;
using UnityEngine.Assertions;

namespace Utils.Algebraic
{
    public static class TupleExtensions
    {
        public static Smooth.Algebraics.Tuple<T1, T2> PipeAndReturn<T1, T2>(this Smooth.Algebraics.Tuple<T1, T2> tuple, Action<T1, T2> action)
        {
            Assert.IsNotNull(action);
            action(tuple.Item1, tuple.Item2);
            return tuple;
        }

        public static Smooth.Algebraics.Tuple<T1, T2> Convert<T1, T2, T3, T4>(this Smooth.Algebraics.Tuple<T3, T4> tuple, 
            DelegateFunc<T3, T1> func1, DelegateFunc<T4, T2> func2)
        {
            return Smooth.Algebraics.Tuple.Create(func1(tuple.Item1), func2(tuple.Item2));
        }

        public static Option<Smooth.Algebraics.Tuple<T1, T2>> Strict<T1, T2>(this Smooth.Algebraics.Tuple<Option<T1>, Option<T2>> tuple)
        {
            if (tuple.Item1.isSome && tuple.Item2.isSome)
            {
                return Option.Some(Smooth.Algebraics.Tuple.Create(tuple.Item1.value, tuple.Item2.value));
            }
            return Option<Smooth.Algebraics.Tuple<T1, T2>>.None;
        }

        public static Option<Smooth.Algebraics.Tuple<T1, T2, T3>> Strict<T1, T2, T3>(this Smooth.Algebraics.Tuple<Option<T1>, Option<T2>, Option<T3>> tuple)
        {
            if (tuple.Item1.isSome && tuple.Item2.isSome && tuple.Item3.isSome)
            {
                return Option.Some(Smooth.Algebraics.Tuple.Create(tuple.Item1.value, tuple.Item2.value, tuple.Item3.value));
            }
            return Option<Smooth.Algebraics.Tuple<T1, T2, T3>>.None;
        }

        public static Option<Smooth.Algebraics.Tuple<T1, T2, T3, T4>> Strict<T1, T2, T3, T4>(this Smooth.Algebraics.Tuple<Option<T1>, Option<T2>, Option<T3>, Option<T4>> tuple)
        {
            if (tuple.Item1.isSome && tuple.Item2.isSome && tuple.Item3.isSome && tuple.Item4.isSome)
            {
                return Option.Some(Smooth.Algebraics.Tuple.Create(tuple.Item1.value, tuple.Item2.value, tuple.Item3.value, tuple.Item4.value));
            }
            return Option<Smooth.Algebraics.Tuple<T1, T2, T3, T4>>.None;
        }

        public static TResult Select<T1, T2, TParam, TResult>(this Smooth.Algebraics.Tuple<T1, T2> tuple,
            DelegateFunc<T1, T2,  TParam, TResult> func, TParam param)
        {
            return func(tuple.Item1, tuple.Item2, param);
        }

        public static TResult Select<T1, T2, T3, TParam, TResult>(this Smooth.Algebraics.Tuple<T1, T2, T3> tuple,
            DelegateFunc<T1, T2, T3, TParam, TResult> func, TParam param)
        {
            return func(tuple.Item1, tuple.Item2, tuple.Item3, param);
        }

        public static TResult Select<T1, T2, T3, T4, TParam, TResult>(this Smooth.Algebraics.Tuple<T1, T2, T3, T4> tuple,
            DelegateFunc<T1, T2, T3, T4, TParam, TResult> func, TParam param)
        {
            return func(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4, param);
        }

        public static TResult Select<T1, T2, TResult>(this Smooth.Algebraics.Tuple<T1, T2> tuple,
            DelegateFunc<T1, T2, TResult> func)
        {
            return func(tuple.Item1, tuple.Item2);
        }

        public static TResult Select<T1, T2, T3, TResult>(this Smooth.Algebraics.Tuple<T1, T2, T3> tuple,
            DelegateFunc<T1, T2, T3, TResult> func)
        {
            return func(tuple.Item1, tuple.Item2, tuple.Item3);
        }

        public static TResult Select<T1, T2, T3, T4, TResult>(this Smooth.Algebraics.Tuple<T1, T2, T3, T4> tuple,
            DelegateFunc<T1, T2, T3, T4, TResult> func)
        {
            return func(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4);
        }

        public static TResult Select<T1, T2, T3, T4, T5, TResult>(this Smooth.Algebraics.Tuple<T1, T2, T3, T4, T5> tuple,
       DelegateFunc<T1, T2, T3, T4, T5, TResult> func)
        {
            return func(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4, tuple.Item5);
        }

        public static void Call<T1, T2>(this Smooth.Algebraics.Tuple<T1, T2> tuple, DelegateAction<T1, T2> action)
        {
            action(tuple.Item1, tuple.Item2);
        }

        public static void Call<T1, T2, T3>(this Smooth.Algebraics.Tuple<T1, T2, T3> tuple, DelegateAction<T1, T2, T3> action)
        {
            action(tuple.Item1, tuple.Item2, tuple.Item3);
        }

        public static void Call<T1, T2, T3, T4>(this Smooth.Algebraics.Tuple<T1, T2, T3, T4> tuple, DelegateAction<T1, T2, T3, T4> action)
        {
            action(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4);
        }

        public static void Call<T1, T2, TParam>(this Smooth.Algebraics.Tuple<T1, T2> tuple, DelegateAction<T1, T2, TParam> action, TParam param)
        {
            action(tuple.Item1, tuple.Item2, param);
        }

        public static void Call<T1, T2, T3, TParam>(this Smooth.Algebraics.Tuple<T1, T2, T3> tuple, DelegateAction<T1, T2, T3, TParam> action, TParam param)
        {
            action(tuple.Item1, tuple.Item2, tuple.Item3, param);
        }

        public static void Call<T1, T2, T3, T4, TParam>(this Smooth.Algebraics.Tuple<T1, T2, T3, T4> tuple, DelegateAction<T1, T2, T3, T4, TParam> action, TParam param)
        {
            action(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4, param);
        }

        public static Smooth.Algebraics.Tuple<T1, T2, T3> Flatten<T1, T2, T3>(this Smooth.Algebraics.Tuple<Smooth.Algebraics.Tuple<T1, T2>, T3> tuple)
        {
            return Smooth.Algebraics.Tuple.Create(tuple.Item1.Item1, tuple.Item1.Item2, tuple.Item2);
        }

        public static Smooth.Algebraics.Tuple<T1, T2, T3, T4> Flatten<T1, T2, T3, T4>(this Smooth.Algebraics.Tuple<Smooth.Algebraics.Tuple<T1, T2>, Smooth.Algebraics.Tuple<T3, T4>> tuple)
        {
            return Smooth.Algebraics.Tuple.Create(tuple.Item1.Item1, tuple.Item1.Item2, tuple.Item2.Item1, tuple.Item2.Item2);
        }
    }
}
