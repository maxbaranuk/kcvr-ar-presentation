﻿using System.Globalization;
using System.Threading.Tasks;
using Smooth.Algebraics;
using Smooth.Delegates;
using Smooth.Foundations.Algebraics;

namespace Utils.Algebraic
{
    public static class OptionExtensions
    {
        public static Option<int> TryParseInt(this string number)
            => number.TryParseIntInternal(NumberStyles.Integer);

        public static Option<int> TryParseHex(this string number)
            => number.TryParseIntInternal(NumberStyles.HexNumber);

        public static Option<float> TryParseFloat(this string number)
            => number.TryParseFloatInternal(NumberStyles.Any);

        public static Option<double> TryParseDouble(this string number)
            => number.TryParseDoubleInternal(NumberStyles.Any);

        public static Option<DelegateAction> SelectDelegate<T>(this Option<T> option, DelegateAction<T> action)
        {
            if(option.isNone)
                return Option<DelegateAction>.None;

            DelegateAction result = () => action(option.value);
            return Option.Create(result);
        }

        public static async Task<Option<T>> ToAsync<T>(this Option<Task<T>> option)
        {
            if (option.isNone)
                return Option<T>.None;

            var res = await option.value;
            return res.ToOption();


            // TODO: check ContinueWith
            //return option.isNone
            //    ? Task.FromResult(Option<T>.None)
            //    : option.value.ContinueWith(t => t.IsFaulted || t.IsCanceled ? Option<T>.None : t.Result.ToSome());
        }

        public static Option<Smooth.Algebraics.Tuple<T1, T2>> Merge<T1, T2>(this Option<T1> opt, Option<T2> opt2)
        {
            if (opt.isSome && opt2.isSome)
            {
                return Smooth.Algebraics.Tuple.Create(opt.value, opt2.value).ToSome();
            }
            return Option<Smooth.Algebraics.Tuple<T1, T2>>.None;
        }

        public static Option<T> ToOption<T>(this T? value) where T : struct
            => value?.ToSome() ?? Option<T>.None;

        private static Option<int> TryParseIntInternal(this string number, NumberStyles numberStyle)
        {
            int value;
            return int.TryParse(number, numberStyle, CultureInfo.InvariantCulture, out value)
                ? value.ToSome()
                : Option<int>.None;
        }

        private static Option<float> TryParseFloatInternal(this string number, NumberStyles numberStyle)
        {
            float value;
            return float.TryParse(number, numberStyle, CultureInfo.InvariantCulture, out value)
                ? value.ToSome()
                : Option<float>.None;
        }

        private static Option<double> TryParseDoubleInternal(this string number, NumberStyles numberStyle)
        {
            double value;
            return double.TryParse(number, numberStyle, CultureInfo.InvariantCulture, out value)
                ? value.ToSome()
                : Option<double>.None;
        }

        public static ValueOrError<Option<T>> SwapWithValueOrError<T>(this Option<ValueOrError<T>> option)
        {
            return option.isNone ? ValueOrError<Option<T>>.FromValue(Option<T>.None) : option.value.ContinueWith(o => o.ToSome());
        }

        public static async Task<Option<TResult>> SelectAsync<T, TResult>(this Task<Option<T>> optionTask,
            DelegateFunc<T, Task<TResult>> func)
        {
            var option = await optionTask;
            if (option.isNone)
                return Option<TResult>.None;
            var res = await func(option.value);
            return res.ToSome();
        }

        public static async Task<Option<TResult>> SelectAsync<T, TResult>(this Task<Option<T>> optionTask,
            DelegateFunc<T, Task<Option<TResult>>> func)
        {
            var option = await optionTask;
            if (option.isNone)
                return Option<TResult>.None;
            return await func(option.value);
        }

        public static async Task<Option<TResult>> SelectAsync<T, TResult>(this Task<Option<T>> optionTask,
            DelegateFunc<T, TResult> func)
        {
            var option = await optionTask;
            if(option.isNone)
                return Option<TResult>.None;

            var res = func(option.value);
            return res.ToSome();
        }

        public static async Task<Option<T>> OrAsync<T>(this Task<Option<T>> optionTask, DelegateFunc<Option<T>> noneFunc)
        {
            var option = await optionTask;
            if (option.isNone)
                return noneFunc();

            return option;
        }

        public static async Task<Option<T>> OrAsync<T, P>(this Task<Option<T>> optionTask, DelegateFunc<P, Option<T>> noneFunc, P param)
        {
            var option = await optionTask;
            if (option.isNone)
                return noneFunc(param);

            return option;
        }

        public static async Task<Option<T>> OrAsync<T>(this Task<Option<T>> optionTask, Option<T> noneValue)
        {
            var option = await optionTask;
            return option.isNone ? noneValue : option;
        }

        public static async Task<Option<T>> OrAsync<T>(this Task<Option<T>> optionTask, DelegateFunc<Task<Option<T>>> noneFunc)
        {
            var option = await optionTask;
            if (option.isNone)
                return await noneFunc();

            return option;
        }

        public static async Task<Option<T>> OrAsync<T, P>(this Task<Option<T>> optionTask, DelegateFunc<P, Task<Option<T>>> noneFunc, P param)
        {
            var option = await optionTask;
            if (option.isNone)
                return await noneFunc(param);

            return option;
        }

        public static async Task<T> ValueOrAsync<T>(this Task<Option<T>> optionTask, T noneValue)
        {
            var option = await optionTask;
            if (option.isNone)
                return noneValue;

            return option.value;
        }

        public static async Task<T> ValueOrAsync<T>(this Task<Option<T>> optionTask, DelegateFunc<T> noneFunc)
        {
            var option = await optionTask;
            if (option.isNone)
                return noneFunc();
            return option.value;
        }

        public static async Task<T> ValueOrAsync<T>(this Task<Option<T>> optionTask, DelegateFunc<Task<T>> noneFunc)
        {
            var option = await optionTask;
            if (option.isNone)
                return await noneFunc();
            return option.value;
        }

        public static async Task<Option<TResult>> SelectManyAsync<T, TResult>(this Task<Option<T>> optionTask,
            DelegateFunc<T, Option<TResult>> func)
        {
            var option = await optionTask;
            return option.isNone ? Option<TResult>.None : func(option.value);
        }
    }
}
