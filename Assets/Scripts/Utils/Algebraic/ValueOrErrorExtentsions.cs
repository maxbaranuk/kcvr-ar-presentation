﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Smooth.Algebraics;
using Smooth.Delegates;
using Smooth.Foundations.Algebraics;
using Smooth.Slinq;

namespace Utils.Algebraic
{
    public static class ValueOrErrorExtentsions
    {
        public static ValueOrError<T> Or<T>(this ValueOrError<T> voe1, DelegateFunc<ValueOrError<T>> voe2)
            => voe1.IsError ? voe2() : voe1;

        public static async Task<ValueOrError<T>> OrAsync<T>(this Task<ValueOrError<T>> voeTask, ValueOrError<T> elseVoe)
        {
            var voe = await voeTask;
            return voe.IsError ? elseVoe : voe;
        }

        public static async Task<ValueOrError<T>> OrAsync<T>(this Task<ValueOrError<T>> voeTask, Task<ValueOrError<T>> elseVoe)
        {
            var voe = await voeTask;
            return voe.IsError ? await elseVoe : voe;
        }

        public static T ValueOr<T>(this ValueOrError<T> voe, T elseValue)
        {
            return voe.IsError ? elseValue : voe.Value;
        }

        public static T ValueOr<T>(this ValueOrError<T> voe, DelegateFunc<T> elseValue)
        {
            return voe.IsError ? elseValue() : voe.Value;
        }

        public static T ValueOr<T, P>(this ValueOrError<T> voe, DelegateFunc<P, T> elseValue, P param)
        {
            return voe.IsError ? elseValue(param) : voe.Value;
        }

        public static async Task<T> ValueOrAsync<T>(this Task<ValueOrError<T>> voeTask, T elseValue)
        {
            var voe = await voeTask;
            return voe.IsError ? elseValue : voe.Value;
        }

        public static async Task<T> ValueOrAsync<T>(this Task<ValueOrError<T>> voeTask, DelegateFunc<T> elseValue)
        {
            var voe = await voeTask;
            return voe.IsError ? elseValue() : voe.Value;
        }

        public static async Task<ValueOrError<TResult>> ContinueWithAsync<T, TResult>(this ValueOrError<T> valueOrError,
            DelegateFunc<T, Task<ValueOrError<TResult>>> func, string errorMessage = null)
        {
            if (valueOrError.IsError)
                return ValueOrError<TResult>.FromError(errorMessage == null? valueOrError.Error: $"{errorMessage}: {valueOrError.Error}");

            return await func(valueOrError.Value);
        }

        public static async Task<ValueOrError<TResult>> ContinueWithAsync<T, TResult>(this ValueOrError<T> valueOrError,
            DelegateFunc<T, Task<TResult>> func, string errorMessage = null)
        {
            if (valueOrError.IsError)
                return ValueOrError<TResult>.FromError(errorMessage == null? valueOrError.Error: $"{errorMessage}: {valueOrError.Error}");

            try
            {
                var funcResult = await func(valueOrError.Value);
                return funcResult.ToValue();
            }
            catch (Exception ex)
            {
                var additionalMessage = errorMessage == null ? "" : $"{errorMessage}: ";
                return ValueOrError<TResult>.FromError($"{additionalMessage}Exception: {ex.Message}; StackTrace:\n{ex.StackTrace}\n");
            }
        }

        public static async Task<ValueOrError<TResult>> ContinueWithAsync<T, P, TResult>(this ValueOrError<T> valueOrError,
            DelegateFunc<T, P, Task<ValueOrError<TResult>>> func, P param, string errorMessage = null)
        {
            if (valueOrError.IsError)
                return ValueOrError<TResult>.FromError(errorMessage == null ? valueOrError.Error : $"{errorMessage}: {valueOrError.Error}");

            return await func(valueOrError.Value, param);
        }

        public static Option<T> ToOptionSkipError<T>(this ValueOrError<T> voe) => ToOption(voe, false);
        public static Option<T> ToOption<T>(this ValueOrError<T> voe) => ToOption(voe, true);

        private static Option<T> ToOption<T>(ValueOrError<T> voe, bool logError)
        {
            if (!voe.IsError)
                return voe.Value.ToSome();

            return Option<T>.None;
        }

        public static ValueOrError<T> ToValueOrError<T>(this Option<T> o)
        {
            return o.isSome
                ? ValueOrError<T>.FromValue(o.value)
                : ValueOrError<T>.FromError("Value was missing");
        }

        public static ValueOrError<T> ToValueOrError<T>(this Option<T> o, string errorMessage)
        {
            return o.isSome
                ? ValueOrError<T>.FromValue(o.value)
                : ValueOrError<T>.FromError(errorMessage);
        }

        public static ValueOrError<T> ToValueOrError<T>(this Option<T> o, DelegateFunc<string> errorFunc)
        {
            return o.Cata((v, _) => v.ToValue(), Unit.Default, f => ValueOrError<T>.FromError(f()), errorFunc);
        }

        public static ValueOrError<T> ToValueOrError<T, P>(this Option<T> o, DelegateFunc<P, string> errorFunc, P errorParam)
        {
            return o.Cata((v, _) => v.ToValue(), Unit.Default,
                fancAndParam => ValueOrError<T>.FromError(fancAndParam.Item1(fancAndParam.Item2)), Smooth.Algebraics.Tuple.Create(errorFunc, errorParam));
        }

        public static async Task<ValueOrError<T>> ToValueOrErrorAsync<T>(this Task<Option<T>> optionTask)
        {
            var option = await optionTask;
            return option.isSome
                ? ValueOrError<T>.FromValue(option.value)
                : ValueOrError<T>.FromError("Value was missing");
        }

        public static async Task<ValueOrError<T>> ToValueOrErrorAsync<T>(this Task<Option<T>> optionTask, string errorMessage)
        {
            var option = await optionTask;
            return option.isSome
                ? ValueOrError<T>.FromValue(option.value)
                : ValueOrError<T>.FromError(errorMessage);
        }

        public static ValueOrError<T> ToValue<T>(this T v)
        {
            return ValueOrError<T>.FromValue(v);
        }

        public static ValueOrError<T> ToError<T>(this T _, string error)
        {
            return string.IsNullOrEmpty(error)
                ? ValueOrError<T>.FromError("Generic error")
                : ValueOrError<T>.FromError(error);
        }

        public static ValueOrError<T> IfError<T>(this ValueOrError<T> voe, Action<string> func)
        {
            if (!voe.IsError) return voe;

            try
            {
                func(voe.Error);
            }
            catch (Exception e)
            {
                return ValueOrError<T>.FromError(e.Message);
            }
            return voe;
        }

        public static async Task<ValueOrError<T>> IfErrorAsync<T>(this Task<ValueOrError<T>> voe, Action<string> func)
        {
            var result = await voe;

            if (!result.IsError) return result;

            try
            {
                func(result.Error);
            }
            catch (Exception e)
            {
                return ValueOrError<T>.FromError(e.Message);
            }
            return result;
        }

        public static ValueOrError<T> IfError<T, P>(this ValueOrError<T> voe, Action<string, P> func, P param)
        {
            if (!voe.IsError) return voe;
            try
            {
                func(voe.Error, param);
            }
            catch (Exception e)
            {
                return ValueOrError<T>.FromError(e.Message);
            }
            return voe;
        }

        public static ValueOrError<T> LogIfError<T>(this ValueOrError<T> voe)
        {
            if (!voe.IsError) return voe;
            return voe;
        }

        public static async Task<ValueOrError<T>> LogIfError<T>(this Task<ValueOrError<T>> voeTask)
        {
            var voe = await voeTask;
            if (!voe.IsError)
                return voe;

            return voe;
        }

        public static async Task<ValueOrError<TResult>> ContinueWithAsync<T, TResult>(this Task<ValueOrError<T>> valueOrErrorTask,
            DelegateFunc<T, Task<ValueOrError<TResult>>> func, string errorMessage = null)
        {
            var valueOrError = await valueOrErrorTask;
            if (valueOrError.IsError)
                return ValueOrError<TResult>.FromError(errorMessage == null ? valueOrError.Error : $"{errorMessage}: {valueOrError.Error}");

            return await func(valueOrError.Value);
        }

        public static async Task<ValueOrError<TResult>> ContinueWithAsync<T, TResult>(this Task<ValueOrError<T>> valueOrErrorTask,
            DelegateFunc<T, ValueOrError<TResult>> func, string errorMessage = null)
        {
            var valueOrError = await valueOrErrorTask;
            return valueOrError.ContinueWith((vor, f) => f(vor), func, errorMessage);
        }
        
        public static async Task<ValueOrError<TResult>> ContinueWithAsync<T, TResult, P>(this Task<ValueOrError<T>> valueOrErrorTask,
            DelegateFunc<T, P, ValueOrError<TResult>> func, P param, string errorMessage = null)
        {
            var valueOrError = await valueOrErrorTask;
            if (valueOrError.IsError)
            {
                return ValueOrError<TResult>.FromError(errorMessage == null ? valueOrError.Error : $"{errorMessage}: {valueOrError.Error}");
            }
            try
            {
                return func(valueOrError.Value, param);
            }
            catch (Exception ex)
            {
                var additionalMessage = errorMessage == null ? "" : $"{errorMessage}: ";
                return ValueOrError<TResult>.FromError($"{additionalMessage}Exception: {ex.Message}; StackTrace:\n{ex.StackTrace}\n");
            }
        }

        public static async Task<ValueOrError<TResult>> ContinueWithAsync<T, TResult>(this Task<ValueOrError<T>> valueOrErrorTask,
            DelegateFunc<T, TResult> func, string errorMessage = null)
        {
            var taskResult = await valueOrErrorTask;
            return taskResult.ContinueWith((v, f) => f(v), func, errorMessage);
        }

        public static async Task<ValueOrError<TResult>> ContinueWithAsync<T, TResult>(this Task<ValueOrError<T>> valueOrErrorTask,
            DelegateFunc<T, Task<TResult>> func, string errorMessage = null)
        {
            var valueOrError = await valueOrErrorTask;
            if (valueOrError.IsError)
                return ValueOrError<TResult>.FromError(errorMessage == null ? valueOrError.Error : $"{errorMessage}: {valueOrError.Error}");
            try
            {
                var result = await func(valueOrError.Value);
                return ValueOrError<TResult>.FromValue(result);
            }
            catch (Exception ex)
            {
                var additionalMessage = errorMessage == null ? "" : $"{errorMessage}: ";
                return ValueOrError<TResult>.FromError($"{additionalMessage}Exception: {ex.Message}; StackTrace:\n{ex.StackTrace}\n");
            }
        }

        public static async Task<ValueOrError<TResult>> ContinueWithAsync<T, TResult, P>(this Task<ValueOrError<T>> voeTask,
            DelegateFunc<T, P, TResult> func, P param, string errorMessage = null)
        {
            var valueOrError = await voeTask;
            if (valueOrError.IsError)
            {
                return ValueOrError<TResult>.FromError(errorMessage == null ? valueOrError.Error : $"{errorMessage}: {valueOrError.Error}");
            }
            try
            {
                var res = func(valueOrError.Value, param);
                return ValueOrError.FromValue(res);
            }
            catch (Exception ex)
            {
                var additionalMessage = errorMessage == null ? "" : $"{errorMessage}: ";
                return ValueOrError<TResult>.FromError($"{additionalMessage}Exception: {ex.Message}; StackTrace:\n{ex.StackTrace}\n");
            }
        }

        public static async Task<ValueOrError<TResult>> ContinueWithAsync<T, TResult, P>(this Task<ValueOrError<T>> voeTask,
            DelegateFunc<T, P, Task<TResult>> func, P param, string errorMessage = null)
        {
            var valueOrError = await voeTask;
            if (valueOrError.IsError)
            {
                return ValueOrError<TResult>.FromError(errorMessage == null ? valueOrError.Error : $"{errorMessage}: {valueOrError.Error}");
            }
            try
            {
                var res = await func(valueOrError.Value, param);
                return ValueOrError.FromValue(res);
            }
            catch (Exception ex)
            {
                var additionalMessage = errorMessage == null ? "" : $"{errorMessage}: ";
                return ValueOrError<TResult>.FromError($"{additionalMessage}Exception: {ex.Message}; StackTrace:\n{ex.StackTrace}\n");
            }
        }

        public static async Task<ValueOrError<TResult>> ContinueWithAsync<T, TResult, P>(this Task<ValueOrError<T>> voeTask,
            DelegateFunc<T, P, Task<ValueOrError<TResult>>> func, P param, string errorMessage = null)
        {
            var valueOrError = await voeTask;
            if (valueOrError.IsError)
                return ValueOrError<TResult>.FromError(errorMessage == null ? valueOrError.Error : $"{errorMessage}: {valueOrError.Error}");

            return await func(valueOrError.Value, param);
        }

        public static ValueOrError<TResult> ContinueWith<T, TResult, P>(this ValueOrError<T> valueOrError,
            DelegateFunc<T, P, ValueOrError<TResult>> func, P arg, string errorMessage = null)
        {
            if (valueOrError.IsError)
            {
                return ValueOrError<TResult>.FromError(errorMessage == null ? valueOrError.Error : $"{errorMessage}: {valueOrError.Error}");
            }
            try
            {
                return func(valueOrError.Value, arg);
            }
            catch (Exception ex)
            {
                var additionalMessage = errorMessage == null ? "" : $"{errorMessage}: ";
                return ValueOrError<TResult>.FromError($"{additionalMessage}Exception: {ex.Message}; StackTrace:\n{ex.StackTrace}\n");
            }
        }

        public static ValueOrError<TResult> ContinueWith<T, TResult, P>(this ValueOrError<T> valueOrError,
            DelegateFunc<T, P, TResult> func, P arg, string errorMessage = null)
        {
            if (valueOrError.IsError)
                return ValueOrError<TResult>.FromError(errorMessage == null ? valueOrError.Error : $"{errorMessage}: {valueOrError.Error}");
            try
            {
                return ValueOrError<TResult>.FromValue(func(valueOrError.Value, arg));
            }
            catch (Exception ex)
            {
                var additionalMessage = errorMessage == null ? "" : $"{errorMessage}: ";
                return ValueOrError<TResult>.FromError($"{additionalMessage}Exception: {ex.Message}; StackTrace:\n{ex.StackTrace}\n");
            }
        }

        public static Task<Option<T>> ToOptionSkipErrorAsync<T>(this Task<ValueOrError<T>> voeTask)
        {
            try
            {
                return voeTask.ContinueWith(t => ToOptionSkipError(t.Result));
            }
            catch (Exception e)
            {
                return Task.FromResult(Option<T>.None);
            }
        }

        public static Task<Option<T>> ToOptionAsync<T>(this Task<ValueOrError<T>> voeTask)
        {
            try
            {
                return voeTask.ContinueWith(t => ToOption(t.Result));
            }
            catch (Exception e)
            {
                return Task.FromResult(Option<T>.None);
            }
        }

        public static ValueOrError<T> Where<T>(this ValueOrError<T> voe, DelegateFunc<T, bool> predicate, string errorMessage)
        {
            if (voe.IsError)
            {
                return voe;
            }
            return predicate(voe.Value)
                ? ValueOrError<T>.FromValue(voe.Value)
                : ValueOrError<T>.FromError($"Value didn't satisfy condition: {errorMessage}");
        }

        public static ValueOrError<T> Where<T, P>(this ValueOrError<T> voe, DelegateFunc<T, P, bool> predicate, P param, string errorMessage)
        {
            if (voe.IsError)
            {
                return ValueOrError<T>.FromError(voe.Error);
            }
            return predicate(voe.Value, param)
                ? ValueOrError<T>.FromValue(voe.Value)
                : ValueOrError<T>.FromError($"Value didn't satisfy condition: {errorMessage}");
        }

        public static ValueOrError<T> Where<T>(this ValueOrError<T> voe, DelegateFunc<T, bool> predicate, DelegateFunc<T, string> errorMessageFunc)
        {
            return voe.IsError || predicate(voe.Value)
                ? voe
                : ValueOrError<T>.FromError(errorMessageFunc(voe.Value));
        }

        public static async Task<ValueOrError<T>> Where<T>(this Task<ValueOrError<T>> voeTask, DelegateFunc<T, bool> predicate, string errorMessage)
        {
            var voe = await voeTask;
            if (voe.IsError)
            {
                return ValueOrError<T>.FromError(voe.Error);
            }
            return predicate(voe.Value)
                ? ValueOrError<T>.FromValue(voe.Value)
                : ValueOrError<T>.FromError($"Value didn't satisfy condition: {errorMessage}");
        }

        public static async Task<ValueOrError<T>> Where<T>(this Task<ValueOrError<T>> voeTask, DelegateFunc<T, bool> predicate, DelegateFunc<T, string> errorMessageFunc)
        {
            var voe = await voeTask;
            if (voe.IsError)
            {
                return ValueOrError<T>.FromError(voe.Error);
            }
            return predicate(voe.Value)
                ? ValueOrError<T>.FromValue(voe.Value)
                : ValueOrError<T>.FromError($"Value didn't satisfy condition: {errorMessageFunc(voe.Value)}");
        }

        public static async Task<ValueOrError<T>> Where<T, P>(this Task<ValueOrError<T>> voeTask, DelegateFunc<T, P, bool> predicate, P param, string errorMessage)
        {
            var voe = await voeTask;
            if (voe.IsError)
            {
                return ValueOrError<T>.FromError(voe.Error);
            }
            return predicate(voe.Value, param)
                ? ValueOrError<T>.FromValue(voe.Value)
                : ValueOrError<T>.FromError($"Value didn't satisfy condition: {errorMessage}");
        }

        public static async Task<ValueOrError<T>> WhereKeepError<T>(this Task<ValueOrError<T>> voeTask, DelegateFunc<T, bool> predicate,
            DelegateFunc<T, string> errorMessageFunc)
        {
            var voe = await voeTask;
            return voe.IsError || predicate(voe.Value)
                ? voe
                : ValueOrError<T>.FromError(errorMessageFunc(voe.Value));
        }

        public static void ForEach<T>(this ValueOrError<T> voe, DelegateAction<T> action)
        {
            if (!voe.IsError)
                action(voe.Value);
        }
        
        public static void ForEach<T, P>(this ValueOrError<T> voe, DelegateAction<T, P> action, P param)
        {
            if (!voe.IsError)
                action(voe.Value, param);
        }

        public static async Task ForEach<T>(this Task<ValueOrError<T>> voeTask, DelegateAction<T> action)
        {
            var voe = await voeTask;
            if (!voe.IsError)
                action(voe.Value);
        }

        public static async Task ForEach<T, P>(this Task<ValueOrError<T>> voeTask, DelegateAction<T, P> action, P param)
        {
            var voe = await voeTask;
            if (!voe.IsError)
                action(voe.Value, param);
        }

        public static async Task ForEachOr<T, P>(this Task<ValueOrError<T>> voeTask, DelegateAction<T, P> valueAction, P param,
            DelegateAction<string> errorAction)
        {
            var voe = await voeTask;
            if (!voe.IsError)
                valueAction(voe.Value, param);
            else
                errorAction(voe.Error);
        }

        public static async Task ForEachOr<T, P1, P2>(this Task<ValueOrError<T>> voeTask, DelegateAction<T, P1> valueAction, P1 valueParam,
            DelegateAction<string, P2> errorAction, P2 errorParam)
        {
            var voe = await voeTask;
            if (!voe.IsError)
                valueAction(voe.Value, valueParam);
            else
                errorAction(voe.Error, errorParam);
        }

        public static ValueOrError<TResult> All<T, TResult>(ValueOrError<T>[] vs, DelegateFunc<IEnumerable<T>,
            TResult> func)
        {
            if (vs.Slinq().Any(v => v.IsError))
            {
                return ValueOrError<TResult>.FromError(vs.Slinq().First(v => v.IsError).Error);
            }
            return ValueOrError<TResult>.FromValue(func(vs.Select(v => v.Value)));
        }
    }
}