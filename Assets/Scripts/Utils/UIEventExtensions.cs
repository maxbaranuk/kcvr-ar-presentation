﻿using UnityEngine.UI;

namespace Utils
{
    public static class UIEventExtensions
    {
        private static readonly Slider.SliderEvent emptySliderEvent = new Slider.SliderEvent();
        private static readonly Toggle.ToggleEvent emptyToggleEvent = new Toggle.ToggleEvent();
        private static readonly InputField.OnChangeEvent emptyInputFieldEvent = new InputField.OnChangeEvent();
        private static readonly Dropdown.DropdownEvent emptyDropdownEvent = new Dropdown.DropdownEvent();

        public static void SetValue(this Slider instance, float value)
        {
            var originalEvent = instance.onValueChanged;
            instance.onValueChanged = emptySliderEvent;
            instance.value = value;
            instance.onValueChanged = originalEvent;
        }

        public static void SetValue(this Toggle instance, bool value)
        {
            var originalEvent = instance.onValueChanged;
            instance.onValueChanged = emptyToggleEvent;
            instance.isOn = value;
            instance.onValueChanged = originalEvent;
        }

        public static void SetValue(this InputField instance, string value)
        {
            var originalEvent = instance.onValueChanged;
            instance.onValueChanged = emptyInputFieldEvent;
            instance.text = value;
            instance.onValueChanged = originalEvent;
        }

        public static void SetValue(this Dropdown instance, int index)
        {
            var originalEvent = instance.onValueChanged;
            instance.onValueChanged = emptyDropdownEvent;
            instance.value = index;
            instance.onValueChanged = originalEvent;
        }
    }
}