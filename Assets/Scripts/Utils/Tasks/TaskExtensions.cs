﻿using System.Threading.Tasks;
using Smooth.Delegates;

namespace Utils.Tasks
{
    public static class TaskExtensions
    {
        public static async Task<TResult> Select<T, TResult>(this Task<T> task, DelegateFunc<T, TResult> func)
        {
            var result = await task;
            return func(result);
        }
        
        public static async Task<TResult> Select<T, TResult, P>(this Task<T> task, DelegateFunc<T, P, TResult> func, P param)
        {
            var result = await task;
            return func(result, param);
        }
    }
}