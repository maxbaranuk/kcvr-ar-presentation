using System.Collections.Generic;
using Meta.DataClasses.Models;
using UnityEngine;

namespace ContentTransformation
{

    //fixme add documentation and simple examples of how it works, and what it do
    //fixme fix "if" statement formating    
    public class AxesFixer
    {
        private float[,] matrix;
        
        private static Dictionary<string, float[,]> matrices
            = new Dictionary<string, float[,]>();

        public AxesFixer(AxesConvention_1 vecConventionType)
        {
            string tname = vecConventionType.ToString();
            
            if (matrices.ContainsKey(tname))
            {
                matrix = matrices[tname];
                return;
            }
            
            matrix = new float[3,3];

                 if (tname.Contains("XLeft"))  matrix[0,0] = -1;
            else if (tname.Contains("XRight")) matrix[0,0] =  1;
            else if (tname.Contains("YLeft"))  matrix[0,1] = -1;
            else if (tname.Contains("YRight")) matrix[0,1] =  1;
            else if (tname.Contains("ZLeft"))  matrix[0,2] = -1;
            else if (tname.Contains("ZRight")) matrix[0,2] =  1;
            
                 if (tname.Contains("XDown"))  matrix[1,0] = -1;
            else if (tname.Contains("XUp"))    matrix[1,0] =  1;
            else if (tname.Contains("YDown"))  matrix[1,1] = -1;
            else if (tname.Contains("YUp"))    matrix[1,1] =  1;
            else if (tname.Contains("ZDown"))  matrix[1,2] = -1;
            else if (tname.Contains("ZUp"))    matrix[1,2] =  1;
            
                 if (tname.Contains("XIn"))    matrix[2,0] =  1;
            else if (tname.Contains("XOut"))   matrix[2,0] = -1;
            else if (tname.Contains("YIn"))    matrix[2,1] =  1;
            else if (tname.Contains("YOut"))   matrix[2,1] = -1;
            else if (tname.Contains("ZIn"))    matrix[2,2] =  1;
            else if (tname.Contains("ZOut"))   matrix[2,2] = -1;
            
            matrices[tname] = matrix;
        }


        public Vector3 GetFlippedVector(Vector3 vec)
        {
            float x = matrix[0,0] * vec.x + matrix[0,1] * vec.y + matrix[0,2] * vec.z;
            float y = matrix[1,0] * vec.x + matrix[1,1] * vec.y + matrix[1,2] * vec.z;
            float z = matrix[2,0] * vec.x + matrix[2,1] * vec.y + matrix[2,2] * vec.z;
            return new Vector3(x, y, z);
        }

        public Quaternion GetQuaternion()
        {
            var trace = matrix[0, 0] + matrix[1, 1] + matrix[2, 2];
            float s = 0;
            float w = 0;
            float x = 0;
            float y = 0;
            float z = 0;

            if (trace > 0)
            {
                s = Mathf.Sqrt(1 + trace)*2;
                w = s/4f;
                x = (matrix[2, 1] - matrix[1, 2])/s;
                y = (matrix[0, 2] - matrix[2, 0])/s;
                z = (matrix[1, 0] - matrix[0, 1])/s;
            }
            else if (matrix[0, 0] > matrix[1, 1] && matrix[0, 0] > matrix[2, 2])
            {
                s = Mathf.Sqrt(1 + matrix[0, 0] - matrix[1, 1] - matrix[2, 2])*2;
                w = (matrix[2, 1] - matrix[1, 2])/s;
                x = s/4f;
                y = (matrix[0, 1] + matrix[1, 0])/s;
                z = (matrix[0, 2] + matrix[2, 0])/s;
            }
            else if (matrix[1, 1] > matrix[2, 2])
            {
                s = Mathf.Sqrt(1 + matrix[1, 1] - matrix[0, 0] - matrix[2, 2])*2;
                w = (matrix[0, 2] - matrix[2, 0])/s;
                x = (matrix[0, 1] + matrix[1, 0])/s;
                y = s/4f;
                z = (matrix[1, 2] + matrix[2, 1])/s;
            }
            else
            {
                s = Mathf.Sqrt(1 + matrix[2, 2] - matrix[0, 0] - matrix[1, 1])*2;
                w = (matrix[1, 0] - matrix[0, 1])/s;
                x = (matrix[0, 2] + matrix[2, 0])/s;
                y = (matrix[1, 2] + matrix[2, 1])/s;
                z = s/4f;
            }
            return new Quaternion(x, y, z, w);
        }

        public Vector3 GetMatchingScaling (Vector3 desired, Vector3 actual, Vector3 conversionScaling)
        {
            var convertedBBoxSize = GetFlippedVector(desired);
            float xmax = Mathf.Abs(convertedBBoxSize.x * conversionScaling.x);
            float ymax = Mathf.Abs(convertedBBoxSize.y * conversionScaling.y);
            float zmax = Mathf.Abs(convertedBBoxSize.z * conversionScaling.z);
            
            var matchScaling = new Vector3(
                    (xmax / actual.x) * conversionScaling.x,
                    (ymax / actual.y) * conversionScaling.y,
                    (zmax / actual.z) * conversionScaling.z);
            
            return matchScaling;
        }
    }
}