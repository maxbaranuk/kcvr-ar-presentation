﻿using UnityEngine;
using Utils.Math;

namespace Utils.Unity
{
    public static class VectorExtensions
    {
        public static Vector3 ToNonZero(this Vector3 vec)
        {
            for (int i = 0; i < 3; i++)
            {
                vec[i] = vec[i].ToNonZero();
            }
            return vec;
        }
    }
}
