﻿namespace Utils.Unity
{
    public static class Tags
    {
        public const string SpikeRoot = "SpikeRoot";
        public const string ProductViewerCanvas = "ProductViewerCanvas";
        public const string VisibleInProductViewer = "VisibleInProductViewer";
    }
}