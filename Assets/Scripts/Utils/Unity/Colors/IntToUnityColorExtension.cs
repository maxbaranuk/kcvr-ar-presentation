using UnityEngine;

namespace Utils.Unity.Colors
{
    internal static class IntToUnityColorExtension
    {
        /// <summary>
        /// Used for cuboids color
        /// </summary>
        public static Color ToColor(this int rgb)
        {
            return new Color((float)(rgb & 0xff) / (1 << 8),
                               (float)(rgb & 0xff00) / (1 << 16),                              
                               (float)(rgb & 0xff0000) / (1 << 24));
        }

        public static Color ToLegacySceneryColor(this int rgb)
        {
            return new Color((float)(rgb & 0xff0000) / (1 << 24),
                               (float)(rgb & 0xff00) / (1 << 16),
                               (float)(rgb & 0xff) / (1 << 8));
        }

        public static Color ToColorWithAlpha(this Color color, float alpha)
            => new Color(color.r, color.g, color.b, alpha);
        
        private static int ToComponent(this float cc, int offset)
        {
            return (((int) (cc * 255)) & 255) << offset;
        }

        public static int ToIntColor(this Color color)
        {
            return color.a.ToComponent(24) | 
                   color.r.ToComponent(16) | 
                   color.g.ToComponent(8) | 
                   color.b.ToComponent(0);
        }
    }
}