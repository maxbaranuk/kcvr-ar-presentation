﻿using System;
using System.Globalization;
using UnityEngine;

namespace Utils.Unity.Colors
{
    public static class ColorConverter
    {
        public static Color HsvToRgb(float h, float s, float v)
        {
            h = (h % 1 + 1) % 1;

            float i = Mathf.Floor(h * 6.0f),
                  f = h * 6 - i,
                  p = v * (1 - s),
                  q = v * (1 - s * f),
                  t = v * (1 - s * (1 - f));

            switch ((int)i)
            {
                case 0: return new Color(v, t, p);
                case 1: return new Color(q, v, p);
                case 2: return new Color(p, v, t);
                case 3: return new Color(p, q, v);
                case 4: return new Color(t, p, v);
                default: return new Color(v, p, q);
            }
        }

        public static Color HsvToRgb(float h, float s, float v, float a)
        {
            Color color = HsvToRgb(h, s, v);
            return new Color(color.r, color.g, color.b, a);
        }

        public static Color HashToRgb(int hash)
        {
            return HsvToRgb((hash & 255) / 255.0f, 1, 1);
        }

        public static Color HashToRgb(int hash, float a)
        {
            return HsvToRgb((hash & 255) / 255.0f, 1, 1, a);
        }

        public static string ColorToHex(Color color)
        {
            var intColor = ToComponent(color.r, 24) |
                   ToComponent(color.g, 16) |
                   ToComponent(color.b, 8) |
                   ToComponent(color.a, 0);
            return intColor.ToString("x8");
        }

        public static Color HexToColor(string hexString)
        {
            if (hexString.Length < 6 || hexString.Length > 8)
                throw new InvalidOperationException($"Wrong length of color hex string: {hexString}");

            if (hexString.Length != 8)
            {
                var colorNumber = int.Parse(hexString.Substring(0, 6), NumberStyles.HexNumber);
                return new Color((float)((colorNumber & 0xff0000) >> 16) / byte.MaxValue,
                               (float)((colorNumber & 0xff00) >> 8) / byte.MaxValue,
                               (float)(colorNumber & 0xff) / byte.MaxValue);
            }
            else
            {
                var colorNumber = int.Parse(hexString, NumberStyles.HexNumber);
                return new Color((float)((colorNumber & 0xff000000) >> 24) / byte.MaxValue,
                               (float)((colorNumber & 0xff0000) >> 16) / byte.MaxValue,
                               (float)((colorNumber & 0xff00) >> 8) / byte.MaxValue,
                               (float)(colorNumber & 0xff) / byte.MaxValue);
            }
        }

        private static int ToComponent(float cc, int offset)
        {
            return ((int)(cc * 255) & 255) << offset;
        }
    }
}
