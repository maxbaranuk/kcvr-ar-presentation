﻿using UnityEngine;
using UnityEngine.UI;

namespace Utils.Unity.Colors
{
    public static class ColorBlockExtensions
    {
        public static ColorBlock WithNormalColor(this ColorBlock colorBlock, Color normalColor)
        {
            colorBlock.normalColor = normalColor;
            return colorBlock;
        }
    }
}