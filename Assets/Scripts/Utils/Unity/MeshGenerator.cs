﻿using System;
using UnityEngine;

namespace Utils.Unity
{
    public static class MeshGenerator
    {

        public static Mesh CreateGizmoArrowMesh()
        {
            return CreateClosedSolidOfRevolution(
                new[]
                {
                    new Vector2(0, 1),
                    new Vector2(0.07f, 0.7f),
                    new Vector2(0.03f, 0.7f),
                    new Vector2(0.03f, 0),
                    new Vector2(0, 0)
                },
                10);
        }

        /// <summary>
        /// Creates closed solid of revolution mesh
        /// </summary>
        /// <param name="curve">Curve specified by points. First and last points should have X = 0</param>
        /// <param name="sfc">Side facets count</param>
        /// <returns>Solid of revolution mesh</returns>
        public static Mesh CreateClosedSolidOfRevolution(Vector2[] curve, int sfc)
        {
            if (sfc < 3)
                throw new ArgumentException("Number of lateral facets cannot be less then 3");

            Mesh mesh = new Mesh();

            Vector3[] vertices = new Vector3[(curve.Length - 2) * sfc + 2];
            int[] triangles = new int[curve.Length * sfc * 3 * 2];

            vertices[0] = new Vector3(0, curve[0].y, 0);
            vertices[(curve.Length - 2) * sfc + 1] = new Vector3(0, curve[curve.Length - 1].y, 0);

            for (int i = 0; i != curve.Length - 2; i++)
            {
                Quaternion stepRotation = Quaternion.AngleAxis(360f / sfc, Vector3.up);
                Vector3 point = Vector3.right * curve[i + 1].x;

                for (int j = 0; j != sfc; j++)
                {
                    vertices[i * sfc + j + 1] = new Vector3(point.x, curve[i + 1].y, point.z);
                    point = stepRotation * point;
                }
            }

            for (int i = 0; i != sfc; i++)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = (i + 1) % sfc + 1;

                triangles[(curve.Length - 1) * sfc * 6 + i * 3] = (curve.Length - 2) * sfc + 1;
                triangles[(curve.Length - 1) * sfc * 6 + i * 3 + 2] = (curve.Length - 3) * sfc + i + 1;
                triangles[(curve.Length - 1) * sfc * 6 + i * 3 + 1] = (curve.Length - 3) * sfc + (i + 1) % sfc + 1;
            }

            for (int i = 0; i != curve.Length - 3; i++)
            {
                for (int j = 0; j != sfc; j++)
                {
                    triangles[(i * 2 + 1) * sfc * 3 + j * 6] = i * sfc + j + 1;
                    triangles[(i * 2 + 1) * sfc * 3 + j * 6 + 1] = (i + 1) * sfc + j + 1;
                    triangles[(i * 2 + 1) * sfc * 3 + j * 6 + 2] = (i + 1) * sfc + (j + 1) % sfc + 1;

                    triangles[(i * 2 + 1) * sfc * 3 + j * 6 + 3] = (i + 1) * sfc + (j + 1) % sfc + 1;
                    triangles[(i * 2 + 1) * sfc * 3 + j * 6 + 4] = i * sfc + (j + 1) % sfc + 1;
                    triangles[(i * 2 + 1) * sfc * 3 + j * 6 + 5] = i * sfc + j + 1;
                }
            }

            mesh.vertices = vertices;
            mesh.triangles = triangles;

            mesh.RecalculateNormals();

            return mesh;
        }
    }
}
