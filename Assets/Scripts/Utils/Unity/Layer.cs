using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Utils.Unity
{
    public enum Layer
    {
        UI = 5,
        Layout = 8,
        ViewProduct = 9,
        GUI = 10,
        ProductGroup = 12,
        Fixture = 13,
        DragReceiver = 14,
        Planogram = 15,
        FixtureEditorCollider = 16,
        Gizmos = 17,
        Joysticks = 18, // TODO: remove, legacy code
        WallpaperCollider = 18,
        BackWall = 19,
        Player = 20,
        Colorization = 21,
        TempColliders = 31
    }

    public static class LayerExtensions
    {
        public static readonly int All = unchecked((int)0xFFFFFFFF);

        public static string LayerToName(this Layer layer)
        {
            return layer.ToString();
        }

        public static LayerMask ToLayerMask(this Layer layer)
        {
            return 1 << (int)layer;
        }

        public static LayerMask ToLayerNumber(this Layer layer)
        {
            return (int)layer;
        }

        public static LayerMask ToLayerMask(this IEnumerable<Layer> layers)
        {
            return layers.Aggregate(0, (mask, layer) => mask | layer.ToLayerMask());
        }

        public static LayerMask ToExcludeLayerMask(this Layer layer)
        {
            return ~(1 << (int)layer);
        }

		public static void SetLayerRecursively(this UnityEngine.GameObject gameObject, int layer)
		{
			gameObject.layer = layer;
			
			foreach (Transform child in gameObject.transform) 
			{
				child.gameObject.SetLayerRecursively(layer);
			}
		}
    }
}