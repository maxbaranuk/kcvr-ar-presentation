﻿using Smooth.Slinq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Utils.Unity
{
    public static class UIExtensions
    {
        private static readonly Toggle.ToggleEvent EmptyEvent = new Toggle.ToggleEvent();

        public static void SetValueWithoutEvent(this Toggle toggle, bool value)
        {
            var onValueChanged = toggle.onValueChanged;
            toggle.onValueChanged = EmptyEvent;
            toggle.isOn = value;
            toggle.onValueChanged = onValueChanged;
        }

        public static Rect ToScreenRect(this RectTransform rect)
        {
            var worldCorners = new Vector3[4];
            rect.GetWorldCorners(worldCorners);
            var result = new Rect(
                          worldCorners[0].x,
                          worldCorners[0].y,
                          worldCorners[2].x - worldCorners[0].x,
                          worldCorners[2].y - worldCorners[0].y);
            return result;
        }

        public static bool IsPointerLowerThanBottomRectSide(Vector2 point, RectTransform rectTransform)
        {
            return point.y < rectTransform.ToScreenRect().yMin;
        }

        public static bool IsPointerLowerThanRectCenter(Vector2 point, RectTransform rectTransform)
        {
            var screenRect = rectTransform.ToScreenRect();
            return point.y < screenRect.yMin + screenRect.height / 2;
        }

        public static bool IsPointerHigherThanTopRectSide(Vector2 point, RectTransform rectTransform)
        {
            return point.y > rectTransform.ToScreenRect().yMax;
        }

        public static bool IsPointerHigherThanRectCenter(Vector2 point, RectTransform rectTransform)
        {
            var screenRect = rectTransform.ToScreenRect();
            return point.y > screenRect.yMin + screenRect.height / 2;
        }

        public static bool IsPointerOnItemHeight(Vector2 point, RectTransform rectTransform)
        {
            return !IsPointerHigherThanTopRectSide(point, rectTransform)
                   && !IsPointerLowerThanBottomRectSide(point, rectTransform);
        }

        public static void SetAlphaInChildren(this Image image, float alpha)
        {
            Assert.IsTrue(alpha >= 0 && alpha <= 1);
            image.GetComponentsInChildren<Graphic>()
                .Slinq()
                .ForEach((g, a) => g.SetSelfAlpha(a), alpha);
        }

        public static void SetSelfAlpha(this Graphic graphics, float alpha)
        {
            Assert.IsTrue(alpha >= 0 && alpha <= 1);
            var color = graphics.color;
            color.a = alpha;
            graphics.color = color;
        }
    }
}