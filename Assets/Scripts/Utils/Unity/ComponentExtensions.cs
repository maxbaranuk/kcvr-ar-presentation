﻿using Smooth.Algebraics;
using UnityEngine;

namespace Utils.Unity
{
    public static class ComponentExtensions
    {
        public static Option<T> TryGetComponent<T>(this Component c)
        {
            var component = c.GetComponent<T>();
            var exists = !typeof(T).IsInterface ? component as Object : component != null;
            var result = !exists ? Option<T>.None : component.ToSome();
            return result;
        }

        public static Option<T> TryGetComponent<T>(this UnityEngine.GameObject obj)
        {
            var component = obj.GetComponent<T>();
            var exists = !typeof(T).IsInterface ? component as Object : component != null;
            var result = !exists ? Option<T>.None : component.ToSome();
            return result;
        }

        public static Option<T> TryGetComponentInChildren<T>(this Component c)
        {
            var component = c.GetComponentInChildren<T>();
            var exists = !typeof(T).IsInterface ? component as Object : component != null;
            var result = !exists ? Option<T>.None : component.ToSome();
            return result;
        }

        public static Option<T> TryGetComponentInChildren<T>(this UnityEngine.GameObject obj)
        {
            var component = obj.GetComponentInChildren<T>();
            var exists = !typeof(T).IsInterface ? component as Object : component != null;
            var result = !exists ? Option<T>.None : component.ToSome();
            return result;
        }

        public static Option<T> TryGetComponentInParent<T>(this Component obj)
        {
            var component = obj.GetComponentInParent<T>();
            var exists = !typeof(T).IsInterface ? component as Object : component != null;
            var result = !exists ? Option<T>.None : component.ToSome();
            return result;
        }

        public static Option<T> TryGetComponentInParent<T>(this UnityEngine.GameObject obj)
        {
            var component = obj.GetComponentInParent<T>();
            var exists = !typeof(T).IsInterface ? component as Object : component != null;
            var result = !exists ? Option<T>.None : component.ToSome();
            return result;
        }

        public static T RealNull<T>(this T o) where T : Object
        {
            return o ? o : null;
        }

        public static T GetComponentInParentIncludeInactive<T>(this Component component) where T : MonoBehaviour
        {
            var parent = component.transform.parent;
            if (parent == null)
                return null;

            var targetComponent = parent.GetComponent<T>();
            return targetComponent != null ? targetComponent : GetComponentInParentIncludeInactive<T>(parent);
        }
    }
}