﻿using System.Collections.Generic;
using UnityEngine;
using Utils.Math;

namespace Utils.Unity
{
    public static class BoundsExtensions
    {
        private static readonly Vector3 _additionalOffset = Vector3.one*0.001f;

        public static Bounds Scale(this Bounds bounds, Vector3 scale)
        {
            return new Bounds(Vector3.Scale(bounds.center, scale), Vector3.Scale(bounds.size, scale));
        }

        public static List<Vector3> VectorLineSegmentPoints(this Bounds bounds)
        {
            var near = bounds.center - bounds.extents  - _additionalOffset;
            var far = bounds.center + bounds.extents + _additionalOffset;
            return new List<Vector3>
            {
                new Vector3(near.x, near.y, near.z), new Vector3(near.x, far.y, near.z),
                new Vector3(far.x, near.y, near.z), new Vector3(far.x, far.y, near.z),
                new Vector3(near.x, near.y, far.z), new Vector3(near.x, far.y, far.z),
                new Vector3(far.x, near.y, far.z), new Vector3(far.x, far.y, far.z),

                new Vector3(near.x, near.y, near.z), new Vector3(far.x, near.y, near.z),
                new Vector3(far.x, near.y, near.z), new Vector3(far.x, near.y, far.z),
                new Vector3(far.x, near.y, far.z), new Vector3(near.x, near.y, far.z),
                new Vector3(near.x, near.y, near.z), new Vector3(near.x, near.y, far.z),

                new Vector3(near.x, far.y, near.z), new Vector3(far.x, far.y, near.z),
                new Vector3(far.x, far.y, near.z), new Vector3(far.x, far.y, far.z),
                new Vector3(far.x, far.y, far.z), new Vector3(near.x, far.y, far.z),
                new Vector3(near.x, far.y, near.z), new Vector3(near.x, far.y, far.z)
            };
        }

        public static Bounds RelativeBounds(this Bounds bounds, Transform current, Transform relative)
        {
            var center = relative.InverseTransformPoint(current.TransformPoint(bounds.center));
            var size = relative.InverseTransformVector(current.TransformVector(bounds.size)).Abs();
            return new Bounds(center, size);
        }

        public static Bounds EncapsulateAndReturn(this Bounds bounds, Bounds other)
        {
            bounds.Encapsulate(other);
            return bounds;
        }
    }
}
