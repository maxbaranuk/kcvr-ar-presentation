﻿using System.Collections.Generic;
using UnityEngine;
using Utils.Collections;

namespace Utils.Unity.GameObject
{
    public static class GetComponentsHelper
    {
        public static Slice<T> GetComponentsInChildrenNonAlloc<T>(this UnityEngine.GameObject go, bool includeInactive)
        {
            var buffer = Container<T>.Buffer;
            go.GetComponentsInChildren(includeInactive, buffer);
            return new Slice<T>(buffer);
        }

        public static Slice<T> GetComponentsInChildrenNonAlloc<T>(this Component c, bool includeInactive)
        {
            var buffer = Container<T>.Buffer;
            c.gameObject.GetComponentsInChildren(includeInactive, buffer);
            return new Slice<T>(buffer);
        }

        private class Container<T>
        {
             public static readonly List<T> Buffer = new List<T>(50); 
        }
    }
}