﻿using Smooth.Algebraics;
using Smooth.Pools;
using Smooth.Slinq;
using UnityEngine;

namespace Utils.Unity.GameObject
{
    public static class GameObjectUtils
    {
        public static void DontDestroyOnLoad<T>(this T gameObject) 
            where T : Object
        {
            if (Application.isEditor && !Application.isPlaying)
                return;
            Object.DontDestroyOnLoad(gameObject);
        }

        public static T GetOrAddComponent<T>(this UnityEngine.GameObject gameObject)
            where T : Component
        {
            return gameObject.GetComponent<T>().RealNull() ?? gameObject.AddComponent<T>();
        }

        public static Bounds GetTotalObjectSpaceBounds(this UnityEngine.GameObject gameObject) => GetTotalObjectSpaceBounds(gameObject, ~0);

        public static Bounds GetTotalObjectSpaceBounds(this UnityEngine.GameObject gameObject, LayerMask layerMask)
        {
            var transform = gameObject.transform;
            var rotation = transform.rotation;
            //align
            transform.rotation = transform.localRotation;
            var bounds = gameObject.GetTotalBounds(layerMask, false, true);

            bounds.center = bounds.center - transform.position;
            var parentScale = transform.parent.RealNull()?.lossyScale.ToNonZero() ?? Vector3.one;
            var scale = Vector3.Scale(parentScale, transform.localScale);
            var invertedScale = new Vector3(1/scale.x, 1/scale.y, 1/scale.z);
            //restore
            transform.rotation = rotation;
            var scaled = bounds.Scale(invertedScale);
            return scaled;
        }
        
        public static Bounds GetTotalBounds(this UnityEngine.GameObject gameObject, bool recalculateBounds = false, bool onlyActiveObjects = false)
            => GetTotalBounds(gameObject, ~0, recalculateBounds, onlyActiveObjects);

        private static Bounds GetTotalBounds(this UnityEngine.GameObject gameObject, LayerMask layerMask, bool recalculateBounds, bool onlyActiveObjects)
        {

            if (recalculateBounds)
            {
                gameObject.GetComponentsInChildrenNonAlloc<MeshFilter>(true)
                    .Slinq()
                    .Select(filter => filter.mesh)
                    .ForEach(mesh => mesh.RecalculateBounds());
            }
            using (var allRenderers = ListPool<Renderer>.Instance.BorrowDisposable())
            {
                 gameObject.GetComponentsInChildrenNonAlloc<Renderer>(!onlyActiveObjects).Slinq()
                    .Select(renderer => Smooth.Algebraics.Tuple.Create(renderer, ((Layer)renderer.gameObject.layer).ToLayerMask()))
                    .Where((rendererAndMask, mask) => (rendererAndMask.Item2 & mask) == rendererAndMask.Item2, layerMask)
                    .Select(rendererAndMask => rendererAndMask.Item1)
                    .AddTo(allRenderers);

                switch (allRenderers.value.Count)
                {
                    case 0:
                        return new Bounds(gameObject.transform.position, Vector3.zero);
                    case 1:
                        return allRenderers.value[0].bounds;
                    default:
                        return allRenderers.value.Slinq().Select(r => r.bounds).Aggregate((b1, b2) => b1.EncapsulateAndReturn(b2));
                }
            }
        }
    }
}
