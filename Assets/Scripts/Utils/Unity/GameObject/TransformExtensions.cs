﻿using System.Collections.Generic;
using System.Linq;
using Smooth.Algebraics;
using Smooth.Dispose;
using Smooth.Pools;
using UnityEngine;
using Utils.Collections;

namespace Utils.Unity.GameObject
{
    public static class TransformExtensions
    {
        public static IEnumerable<Transform> GetAllChildren(this Transform transform)
        {
            return from Transform child in transform select child;
        }

        public static Slice<Transform> GetAllChildrenNonAlloc(this Transform transform)
        {
            Container<Transform>.Buffer.Clear();
            var count = transform.childCount;
            for (var i = 0; i < count; i++)
            {
                Container<Transform>.Buffer.Add(transform.GetChild(i));
            }
            return new Slice<Transform>(Container<Transform>.Buffer, 0, count);
        }

        public static Slice<T> GetAllChildrenNonAlloc<T>(this Transform transform)
        {
            Container<T>.Buffer.Clear();
            var count = transform.childCount;
            for (var i = 0; i < count; i++)
            {
                transform.GetChild(i).TryGetComponent<T>().ForEach(Container<T>.Buffer.Add);
            }
            return new Slice<T>(Container<T>.Buffer);
        }

        public static Slice<T> GetAllChildrenUpToDepthNonAlloc<T>(this Transform transform, int depth)
        {
            var buffer = Container<T>.Buffer;
            buffer.Clear();
            PopulateWithChildrenUpToDepth(transform, depth, buffer);
            return new Slice<T>(Container<T>.Buffer);
        }

        public static Disposable<List<T>> GetAllChildrenUpToDepthNonAllocDisposable<T>(this Transform transform, int depth)
        {
            var buffer = ListPool<T>.Instance.BorrowDisposable();
            PopulateWithChildrenUpToDepth(transform, depth, buffer.value);
            return buffer;
        }

        private static void PopulateWithChildrenUpToDepth<T>(Transform transform, int depth, List<T> listToPopulate)
        {
            var count = transform.childCount;
            for (var i = 0; i < count; i++)
            {
                var child = transform.GetChild(i);
                child.TryGetComponent<T>()
                    .ForEach((c, l) => l.Add(c), listToPopulate);
                if (depth > 0)
                {
                    PopulateWithChildrenUpToDepth(child, depth - 1, listToPopulate);
                }
            }
        }

        public static Option<Transform> FindChildNonAllocRecursive(this Transform transform, string name)
        {
            return transform.GetComponentsInChildrenNonAlloc<Transform>(true)
                .Slinq()
                .FirstOrNone((t, searchName) => t.name == searchName, name);
        }

        private static class Container<T>
        {
            public static List<T> Buffer = new List<T>();
        }
    }
}