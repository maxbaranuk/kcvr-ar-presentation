using UnityEngine;

namespace Utils.Unity
{
    public static class VertexArrayUtilities
    {
        public static void ForceBottomCenterPivot(this Vector3[] vertices)
        {
            Vector3 min = Vector3.one * float.MaxValue;
            Vector3 max = Vector3.one * float.MinValue;

            foreach (Vector3 vertex in vertices)
            {
                min = Vector3.Min(vertex, min);
                max = Vector3.Max(vertex, max);
            }

            for (int index = 0; index < vertices.Length; index++)
            {
                vertices[index] += -min.y * Vector3.up;
                vertices[index] += -min.x * Vector3.right;
                vertices[index] += -min.z * Vector3.forward;
                vertices[index] -= (max.x - min.x) * Vector3.right / 2;
                vertices[index] -= (max.z - min.z) * Vector3.forward / 2;
            }
        }
    }
}