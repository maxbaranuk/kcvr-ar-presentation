﻿namespace Utils.Unity
{
    public static class UnityAssetBundleVersion
    {
        public const string Current = "2017.2";
        public const string CurrentCloud = "2017.2";
    }
}