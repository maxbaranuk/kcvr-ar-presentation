﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Utils
{
    public class HashComputer
    {
        public static string ComputeMD5Hash(string filePath)
        {
            using (var executableStream = File.OpenRead(filePath))
            {
                using (var md5 = new MD5CryptoServiceProvider())
                {
                    var md5Bytes = md5.ComputeHash(executableStream);
                    return BitConverter.ToString(md5Bytes).Replace("-", "");
                }
            }
        }
    }
}
