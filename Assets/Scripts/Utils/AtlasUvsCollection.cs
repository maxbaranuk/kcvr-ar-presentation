﻿using System.Collections.Generic;
using Meta.DataClasses.Geometry;
using Meta.JsonConverters;
using Newtonsoft.Json;
using UnityEngine;

namespace Meta.DataClasses.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class AtlasUvsCollection
    {
        [JsonProperty("BACK"), JsonConverter(typeof (RectToUvJsonConverter))]
        private Rect Back { get; set; }

        [JsonProperty("FRONT"), JsonConverter(typeof(RectToUvJsonConverter))]
        private Rect Front { get; set; }

        [JsonProperty("LEFT"), JsonConverter(typeof(RectToUvJsonConverter))]
        private Rect Left { get; set; }

        [JsonProperty("RIGHT"), JsonConverter(typeof(RectToUvJsonConverter))]
        private Rect Right { get; set; }

        [JsonProperty("BOTTOM"), JsonConverter(typeof(RectToUvJsonConverter))]
        private Rect Bottom { get; set; }

        [JsonProperty("TOP"), JsonConverter(typeof(RectToUvJsonConverter))]
        private Rect Top { get; set; }

        public Dictionary<QuadFace, Rect> FaceToRectDictionary
        {
            get
            {
                return new Dictionary<QuadFace, Rect>
                {
                    [QuadFace.BACK] = Back,
                    [QuadFace.FRONT] = Front,
                    [QuadFace.LEFT] = Left,
                    [QuadFace.RIGHT] = Right,
                    [QuadFace.BOTTOM] = Bottom,
                    [QuadFace.TOP] = Top
                };
            }
            set
            {
                Back = value[QuadFace.BACK];
                Front = value[QuadFace.FRONT];
                Left = value[QuadFace.LEFT];
                Right = value[QuadFace.RIGHT];
                Bottom = value[QuadFace.BOTTOM];
                Top = value[QuadFace.TOP];
            }
        }
    }
}