﻿namespace Utils.String
{    
    public static class Strings
    {
        public static class App
        {
            public const string CATALOGUE_COLLECTION_THUMBNAIL_PATH = "CatalogueThumbnails/Collection_IC_menucatalog";
            public const string CATALOGUE_PLANOGRAM_THUMBNAIL_PATH = "CatalogueThumbnails/Planogram_IC_menucatalog";
            public const string CATALOGUE_ASSORTMENT_THUMBNAIL_PATH = "CatalogueThumbnails/Assortment_IC_menucatalog";
            public const string CATALOGUE_ASSORTMENT_PLANOGRAM_THUMBNAIL_PATH = "CatalogueThumbnails/AssortmentPlanogram_IC_menucatalog";
            public const string CATALOGUE_VARIANT_THUMBNAIL_PATH = "CatalogueThumbnails/PlanogramVariant_IC_menucatalog";
            public const string CATALOGUE_FIXTURE_THUMBNAIL_PATH = "CatalogueThumbnails/Fixture_IC_menucatalog";
            public const string CATALOGUE_FOLDER_THUMBNAIL_PATH = "CatalogueThumbnails/Folder_IC_menucatalog";
            public const string CATALOGUE_PRODUCT_THUMBNAIL_PATH = "CatalogueThumbnails/Product_IC_menucatalog";

            public const string ShaderStandardUnlit = "Standard Unlit";
            public const string ShaderLinesColoredBlended = "Lines/Colored Blended";
            public const string CatalogueSearchFolderName = "Search Results";

            // TODO: Use this format for shopping mode reports also
            public const string DateFormat = "dd MMM yyyy hh-mm-ss tt";
            public const string DatavizFormat = "#,0.##";

            public const string EmptyJsonObjectPattern = @"{\s*}";
        }

        public static class Text
        {
            public const string EmptyVariantNameText = "Empty Planogram Variant name";
            public const string EmptyPlanogramProjectNameText = "Empty Planogram Project name";

            public const string EditShelfModeText = "Edit shelf mode";
            public const string EditShelf2DModeText = "Edit shelf 2D mode";
            public const string ProxyFallingBackText = "Proxy was set, but failed to proceed. Falling back to 'Direct Connection'.";

            public static string SaveReportMessage(string folderName) => $"Report has been saved to {folderName} folder";
        }
    }
}
