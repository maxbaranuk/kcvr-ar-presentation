﻿namespace Utils.String
{
    public static class RichTextHelper
    {
        public const string URL_LINK = "url=";
        private const string TAG_PATTERN_WITH_QUOTES = "<{0}=\"{1}\">{2}</{0}>";
        private const string TAG_PATTERN = "<{0}={1}>{2}</{0}>";
        private const string LINK_TAG = "link";
        private const string COLOR_TAG = "color";

        public static string AddLinkTag(this string text, string link)
        {
            return string.Format(TAG_PATTERN_WITH_QUOTES, LINK_TAG, URL_LINK + link, text);
        }

        public static string AddColorTag(this string text, string hexColor)
        {
            return string.Format(TAG_PATTERN, COLOR_TAG, hexColor, text);
        }
    }
}