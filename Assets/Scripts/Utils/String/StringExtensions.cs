﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Smooth.Algebraics;
using Smooth.Foundations.Algebraics;
using Utils.Algebraic;

namespace Utils.String
{
    public static class StringExtensions
    {
        public static bool Includes(this string original, string compareWith)
            => original.IndexOf(compareWith, StringComparison.InvariantCultureIgnoreCase) >= 0;

        public static bool EqualsIgnoringCase(this string original, string compareWith)
            => string.Equals(original, compareWith, StringComparison.InvariantCultureIgnoreCase);

        public static float ParseToInvariantCulture(this string number)
            => float.Parse(number, CultureInfo.InvariantCulture);

        public static float TryParseToInvariantCulture(this string number)
        {
            float result;
            float.TryParse(number, NumberStyles.Float | NumberStyles.AllowThousands, CultureInfo.InvariantCulture, out result);
            return result;
        }
            
        public static Option<bool> TryParseBoolean(this string value)
        {
            bool result;
            return bool.TryParse(value, out result) 
                ? result.ToSome()
                : Option<bool>.None;
        }
    }

    public class StringOptionIgnoreCaseEqualityComparer : IEqualityComparer<Option<string>>
    {
        public static readonly StringOptionIgnoreCaseEqualityComparer Comparer = new StringOptionIgnoreCaseEqualityComparer();
        private StringOptionIgnoreCaseEqualityComparer()
        {
        }

        public bool Equals(Option<string> x, Option<string> y)
        {
            return x.isSome && y.isSome
                ? x.value.EqualsIgnoringCase(y.value)
                : x.Equals(y);
        }

        public int GetHashCode(Option<string> option)
        {
            return option.isSome ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(option.value) : 0;
        }
    }

    public class StringOptionIgnoreCaseComparer : IComparer<Option<string>>
    {
        public static readonly StringOptionIgnoreCaseComparer Comparer = new StringOptionIgnoreCaseComparer();
        private StringOptionIgnoreCaseComparer()
        {
        }
        public int Compare(Option<string> x, Option<string> y)
        {
            return x.isSome
                ? (y.isSome ? string.Compare(x.value, y.value, StringComparison.InvariantCultureIgnoreCase) : 1)
                : (y.isSome ? -1 : 0);
        }
    }
}
