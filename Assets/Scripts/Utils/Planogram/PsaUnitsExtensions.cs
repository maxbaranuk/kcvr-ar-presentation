﻿using ProSpace;
using UnityEngine;

namespace Utils.Planogram
{
    public static class PsaUnitsExtensions
    {
        private const float InchToMeter = 0.0254f;
        private const float CmToMeter = 0.01f;
        private const float FootToMeter = 0.3048f;
        private const float FootToInch = 12;
        private const float CmInMeter = 100;

        public static Vector3 ToMeters(this Vector3 source, eUnitOfMeasurement sourceUnits)
        {
            return (sourceUnits != eUnitOfMeasurement.Inches) ? source * CmToMeter : source * InchToMeter;
        }

        public static float ToMeters(this float source, eUnitOfMeasurement sourceUnits)
        {
            return (sourceUnits != eUnitOfMeasurement.Inches) ? source * CmToMeter : source * InchToMeter;
        }

        public static Vector3 FromMeters(this Vector3 meters, eUnitOfMeasurement targetUnits)
        {
            return (targetUnits != eUnitOfMeasurement.Inches) ? meters / CmToMeter : meters / InchToMeter;
        }

        public static float FromMeters(this float meters, eUnitOfMeasurement targetUnits)
        {
            return (targetUnits != eUnitOfMeasurement.Inches) ? meters / CmToMeter : meters / InchToMeter;
        }

        public static Vector3 MetersToCm(this Vector3 v) => v * CmInMeter;
    }
}