using ProSpace;
using Utils.String;

namespace Utils.Planogram
{
    public enum ePositionExtraContent
    {
        Peg_ID = 42,
        XCapTraysAndCases = 164,
        YCapTraysAndCases = 165,
        ZCapTraysAndCases = 166,
    }

    public static class PsaStringExtensions
    {
        public static string ToIntegerString(this bool @bool)
        {
            return @bool ? "1" : "0";
        }

        public static T ToEnum<T>(this string @string)
        {
            int result = 0;
            int.TryParse(@string, out result);
            return (T)(object)result;
        }

        public static int ToInt32(this string @string)
        {
            int result = 0;
            int.TryParse(@string, out result);
            return result;
        }

        public static float ToSingle(this string @string)
            => @string.TryParseToInvariantCulture();

        public static bool ToBool(this string @string)
        {
            if (@string.ToInt32() == 1) return true;
            bool result = false;
            bool.TryParse(@string, out result);
            return result;
        }
    }
}
