﻿using ProSpace;
using Utils.Planogram;

namespace Assets.Scripts.Utils.Planogram
{
    public static class PsaProductExtensions
    {
        public static float ToSingle(this PsaProduct psaProduct, eProductContent productContent, eUnitOfMeasurement unitOfMeasurement)
            => psaProduct.GetValue(productContent).ToSingle().ToMeters(unitOfMeasurement);

        public static int ToInt32(this PsaProduct psaProduct, eProductContent productContent)
            => psaProduct.GetValue(productContent).ToInt32();
    }
}
