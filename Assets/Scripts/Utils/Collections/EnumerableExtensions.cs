﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smooth.Algebraics;
using Smooth.Delegates;
using Smooth.Pools;
using Smooth.Slinq;
using Utils.Algebraic;

namespace Utils.Collections
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<R> Zip<T1, T2, R>(this IEnumerable<T1> first, IEnumerable<T2> second,
            Func<T1, T2, R> resultSelector)
        {
            if (first == null) throw new NullReferenceException(nameof(first));
            if (second == null) throw new NullReferenceException(nameof(second));
            if (resultSelector == null) throw new NullReferenceException(nameof(second));

            using (var e1 = first.GetEnumerator())
            using (var e2 = second.GetEnumerator())
                while (e1.MoveNext() && e2.MoveNext())
                    yield return resultSelector(e1.Current, e2.Current);
        }

        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> enumerable)
        {
            return new HashSet<T>(enumerable);
        }

        public static T FirstOr<T>(this IEnumerable<T> enumerable, DelegateFunc<T, bool> predicate, DelegateFunc<T> generator)
        {
            var firstOrDefault = enumerable.Slinq().FirstOrDefault(predicate);
            return (System.Collections.Generic.EqualityComparer<T>.Default.Equals(firstOrDefault, default(T)))
                ? generator()
                : firstOrDefault;
        }

        public static string AsString<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null ? "NULL" : enumerable.Aggregate("[", (s, o) => s + (o+ "\n")) + "]";
        }

        public static List<T> OuterJoin<T, TKey>(this IEnumerable<T> leftCollection, IEnumerable<T> rightCollection,
            DelegateFunc<T, TKey> keySelector, DelegateFunc<T, T, T> joinSelector)
        {
            return OuterJoin(leftCollection, rightCollection, keySelector, keySelector, joinSelector, left => left, right => right);
        }

        public static List<TResult> OuterJoin<TResult, TLeft, TRight, TKey>(this IEnumerable<TLeft> leftCollection, IEnumerable<TRight> rightCollection, 
            DelegateFunc<TLeft,TKey> leftKeySelector, DelegateFunc<TRight, TKey> rightKeySelector,
            DelegateFunc<TLeft, TRight, TResult> joinSelector, DelegateFunc<TLeft, TResult> leftSelector, DelegateFunc<TRight, TResult> rightSelector)
        {
            var dictionary = DictionaryPool<TKey, Smooth.Algebraics.Tuple<Option<TLeft>, Option<TRight>>>.Instance.Borrow();
            var list = ListPool<TResult>.Instance.Borrow();

            foreach (var left in leftCollection)
            {
                dictionary.Add(leftKeySelector(left), Smooth.Algebraics.Tuple.Create(left.ToSome(), Option<TRight>.None));
            }

            foreach (var right in rightCollection)
            {
                var key = rightKeySelector(right);
                dictionary[key] = dictionary.TryGet(key)
                    .Cata((leftAndRight, r) => Smooth.Algebraics.Tuple.Create(leftAndRight.Item1, r.ToSome()), right,
                        r => Smooth.Algebraics.Tuple.Create(Option<TLeft>.None, r.ToSome()), right);
            }

            dictionary.Slinq()
                .Select(pair => pair.Value)
                .Select((leftAndRight, selectors) => selectors.Select(SelectOuterJoinValue, leftAndRight),
                    Smooth.Algebraics.Tuple.Create(joinSelector, leftSelector, rightSelector))
                .AddTo(list);

            DictionaryPool<TKey, Smooth.Algebraics.Tuple<Option<TLeft>, Option<TRight>>>.Instance.Release(dictionary);

            return list;
        }

        private static TResult SelectOuterJoinValue<TResult, TLeft, TRight>(
            DelegateFunc<TLeft, TRight, TResult> joinSelector, DelegateFunc<TLeft, TResult> leftSelector,
            DelegateFunc<TRight, TResult> rightSelector,
            Smooth.Algebraics.Tuple<Option<TLeft>, Option<TRight>> leftAndRight)
            => leftAndRight.Strict()
                .Select((lAndR, selector) => selector(lAndR.Item1, lAndR.Item2), joinSelector)
                .Or(leftAndSelector => leftAndSelector.Item1.Select((left, selector) => selector(left), leftAndSelector.Item2),
                    Smooth.Algebraics.Tuple.Create(leftAndRight.Item1, leftSelector))
                .ValueOr(rightAndSelector => rightAndSelector.Item2(rightAndSelector.Item1.value),
                    Smooth.Algebraics.Tuple.Create(leftAndRight.Item2, rightSelector));
    }
}