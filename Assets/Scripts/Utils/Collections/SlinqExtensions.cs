﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Smooth.Algebraics;
using Smooth.Delegates;
using Smooth.Dispose;
using Smooth.Pools;
using Smooth.Slinq;
using Smooth.Slinq.Collections;
using Smooth.Slinq.Context;

namespace Utils.Collections
{
    public static class SlinqExtensions
    {
        public static Slinq<Smooth.Algebraics.Tuple<T, int>, IListContext<T>> SlinqWithIndex<T>(this Disposable<List<T>> disposableList, int startIndex = 0)
            => disposableList.value.SlinqWithIndex(startIndex);

        public static Slinq<Smooth.Algebraics.Tuple<T, int>, IListContext<T>> SlinqWithIndex<T, TList>(this Disposable<TList> disposableList, int startIndex = 0) 
            where TList : IList<T>
            => disposableList.value.SlinqWithIndex(startIndex);

        public static T[] ToArray<T, C>(this Slinq<T, C> slinq)
        {
            using (var list = ListPool<T>.Instance.BorrowDisposable())
            {
                slinq.AddTo(list);
                return list.value.ToArray();
            }
        }



        public static async Task<Slinq<T, IListContext<T>>> ToAsync<T, C>(this Slinq<Task<T>, C> slinq)
        {
            var list = slinq.ToList(); //ToList borrows from pool.
            var results = await Task.WhenAll(list);
            ListPool<Task<T>>.Instance.Release(list);
            return results.Slinq();
        }

        public static async Task<List<T>> ToListAsync<T, C>(this Task<Slinq<T, C>> slinq)
        {
            return (await slinq).ToList(); //ToList borrows from pool.
        }

        public static async Task<LinkedHeadTail<T>> ToLinkedAsync<T, C>(this Task<Slinq<T, C>> slinq)
        {
            return (await slinq).ToLinked(); 
        }

        public static async Task<Slinq<TResult, SelectContext<TResult, T, C>>> SelectAsync<T, C, TResult>(this Task<Slinq<T, C>> slinqTask, DelegateFunc<T, TResult> selector)
        {
            var slinq = await slinqTask;
            return slinq.Select(selector);
        }

        public static async Task<Slinq<TResult, IListContext<TResult>>> SelectAsync<T, C, TResult>(this Task<Slinq<T, C>> slinqTask, DelegateFunc<T, Task<TResult>> selector)
        {
            var slinq = await slinqTask;
            return await slinq.Select(selector).ToAsync();
        }

        public static async Task<Slinq<TResult, SelectSlinqContext<TResult, C2, T, C>>> SelectManyAsync<T, C, C2, TResult>(this Task<Slinq<T, C>> slinqTask, DelegateFunc<T, Slinq<TResult, C2>> selector)
        {
            var slinq = await slinqTask;
            return slinq.SelectMany(selector);
        }

        public static async Task<Slinq<T, PredicateContext<T, C>>> WhereAsync<T, C>(this Task<Slinq<T, C>> slinqTask, DelegateFunc<T, bool> predicate)
        {
            var slinq = await slinqTask;
            return slinq.Where(predicate);
        }

        public static Dictionary<K, V> ToDictionary<K, V, C>(this Slinq<Smooth.Algebraics.Tuple<K, V>, C> slinq)
        {
            return slinq.AddTo(DictionaryPool<K, V>.Instance.Borrow(), tuple => new KeyValuePair<K, V>(tuple.Item1, tuple.Item2));
        }

#if UNITY_EDITOR
        public static Slinq<T, IListContext<T>> Debug<T, C>(this Slinq<T, C> slinq, DelegateFunc<T, string> toString = null)
        {
            var list = slinq.ToList();
            toString = toString ?? (t => t.ToString());
            var debugString = list.Slinq().Aggregate(new StringBuilder().Append($"########## Slinq of {list.Count} items"),
                (s, t) => s.Append($"\n {toString(t)}"));
            debugString.Append("\n ########## end of Slinq \n\n");
            UnityEngine.Debug.Log(debugString.ToString());
            return list.Slinq();
        }
#endif


#if !UNITY_EDITOR
        public static Slinq<T, C> Debug<T, C>(this Slinq<T, C> slinq, DelegateFunc<T, string> toString = null)
        {
            return slinq;
        }
#endif

    }

    public static class SlinqableEx
    {


        public static Slinq<T, ConcatContext<OptionContext<T>, T, OptionContext<T>>> Two<T>(this T item, T item2)
        {
            return item.ToSome().Slinq().Concat(item2.ToOption().Slinq());
        }

    }

}
