using System.IO;

namespace Utils
{
	public static class StreamExtensions
	{
		/// <summary>
		/// Reads the bytes from the current stream and writes them to another stream.
        /// For .NET 3.5 only (4.0 and above have this)
        /// </summary>
        public static long CopyTo(this Stream source, Stream dest) 
        {
            byte[] buffer = new byte[2048];
            int bytesRead;
            long totalBytes = 0;

            while((bytesRead = source.Read(buffer, 0, buffer.Length)) > 0) 
            {
                dest.Write(buffer, 0, bytesRead);
                totalBytes += bytesRead;
            }

            return totalBytes;
        }
    }
	
}
