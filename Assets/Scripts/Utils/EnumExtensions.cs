﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smooth.Algebraics;
using UnityEngine.Assertions;

namespace Utils
{
    public static class EnumExtensions
    {
        public static Option<T> GetEnumOrNone<T>(this string rawValue) where T : struct, IConvertible
        {
            if (string.IsNullOrEmpty(rawValue))
            {
                return Option<T>.None;
            }
            try
            {
                var enumValue = (T) Enum.Parse(typeof (T), rawValue);
                return new Option<T>(enumValue);
            }
            catch (ArgumentException)
            {
                return Option<T>.None;
            }
        }

        public static T GetEnum<T>(this string rawValue) where T : struct, IConvertible
        {
            return (T) Enum.Parse(typeof (T), rawValue);
        }

        public static IEnumerable<T> GetValues<T>() where T : struct, IConvertible
        {
            return Enum.GetValues(typeof (T)).Cast<T>();
        }

        public static bool HasFlag<T>(this T e, T value) where T : struct, IConvertible
        {
            if (typeof(T).IsEnum)
            {
                return (Convert.ToUInt32(e) & Convert.ToUInt32(value)) == Convert.ToUInt32(value);
            }
            throw new ArgumentException($"Expected enum, got {value.GetType()}");
        }
    }
}