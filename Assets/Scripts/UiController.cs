﻿using System.Diagnostics.CodeAnalysis;
using SamplesResources.SceneAssets.CloudReco.Scripts;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

public class UiController : MonoBehaviour
{

	[SerializeField]
	private Animator _modelsListPanel;

	[SerializeField] 
	private GameObject _editModelPanel;

	private SceneModel _currentEditableModel;
	public static ReactiveProperty<bool> InfoPresent = new BoolReactiveProperty();

	public static UiController Instance { get; private set; }

	private void Awake()
	{
		if (Instance == null)
			Instance = GetComponent<UiController>();
	}

	private void Update()
	{
#if UNITY_EDITOR
		var input = Input.GetMouseButtonUp(0);
		if (input)
			TouchScreen(Input.mousePosition);
#else
		if (Input.touchCount < 1)
			return;
		var input = Input.GetTouch(0);
		if (input.phase == TouchPhase.Ended)
			TouchScreen(input.position);
#endif
	}

	private static void TouchScreen(Vector2 position)
	{
		var ray = Camera.main.ScreenPointToRay(position);
		RaycastHit hit;
		SceneModel model;

		if (!Physics.Raycast(ray, out hit, 100) || EventSystem.current.IsPointerOverGameObject()) return;
		if (hit.collider != null 
		    && (model = hit.transform.GetComponent<SceneModel>()) != null
		    && !Instance._editModelPanel.activeInHierarchy)
			model.Edit();
	}

	public void OpenList()
	{
		_modelsListPanel.SetBool("Open", true);
	}
	
	[SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
	public void CloseList()
	{
		_modelsListPanel.SetBool("Open", false);		
	}

	public void ApplyModel()
	{
		if (CloudRecoContentManager.Instance.CurrentModel.IsNew)
			CloudRecoContentManager.Instance.CurrentModel.Place();
		FinishEditModel();
		CloseList();
	}

	public void DeleteModel()
	{
		Destroy(_currentEditableModel.gameObject);
		FinishEditModel();
	}

	public void StartEditModel(SceneModel model)
	{
		_editModelPanel.SetActive(true);
		_currentEditableModel = model;
		model.ShowMoveGizmo();
	}
	
	public void FinishEditModel()
	{
		_currentEditableModel.CloseGizmos();
		_editModelPanel.SetActive(false);	
		CloudRecoContentManager.Instance.CurrentModel = null;
	}
	
	public void ShowMoveGizmo()
	{
		_currentEditableModel.ShowMoveGizmo();
	}
	
	public void ShowRotateGizmo()
	{
		_currentEditableModel.ShowRotateGizmo();
	}

	public void OnInfoClick()
	{
		InfoPresent.Value = !InfoPresent.Value;
	}
}
