﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContentTransformation.ShadersWorkers.Replacers;
using Scene.SceneBuilding.Models;
using Smooth.Algebraics;
using Smooth.Slinq;
using UnityEngine;
using Utils.Collections;

namespace ContentTransformation.ShadersWorkers
{
    public static class ShadersFixer
    {
        private static readonly Shader _defaultShader = Shader.Find("Standard");
        private static readonly Dictionary<string, Shader> _shaders = new Dictionary<string, Shader>();
        private static readonly Dictionary<string, IShaderReplacer> _replacers;
        private static readonly HashSet<string> _missingShaders = new HashSet<string>();

        private static readonly Dictionary<string, int> _raplacedShadersCounter = new Dictionary<string, int>();
        private static readonly Dictionary<string, int> _notReplacedShadersCounter = new Dictionary<string, int>();

        static ShadersFixer()
        {
            _replacers = typeof(ShadersFixer).Assembly.GetTypes()
                .Where(t => typeof(IShaderReplacer).IsAssignableFrom(t) && !t.IsInterface && !t.IsAbstract)
                .Where(t =>
                {
                    var result = t.GetConstructor(Type.EmptyTypes) != null;
//                    if (!result)
//                        typeof(ShadersFixer).LogError($"Can't instantiate class {t.Name} because it doesn't have parameterless constructor.");
                    return result;
                })
                .Select(t => Activator.CreateInstance(t) as IShaderReplacer)
                .ToDictionary(replacer => replacer.ShaderName, replacer => replacer);
        }

        public static void FixGameObjectShaders(GameObject gameObject)
        {
            // TODO: don't remove, needed to check future shaders replacement.
            //gameObject.GetComponentsInChildren<MeshRenderer>(true).Slinq().ForEach(ShaderReverter.AttachTo);

            gameObject.GetComponentsInChildren<MeshRenderer>(true)
                .Slinq()
                .SelectMany(renderer => renderer.sharedMaterials.Slinq())
                .Where(material => material != null)
                .Select(material => Smooth.Algebraics.Tuple.Create(material, _replacers.TryGet(material.shader.name)))
                .Select(materialReplacer => 
                {
                    AddShaderResolutionForLogging(materialReplacer.Item1.shader.name, materialReplacer.Item2);
                    return materialReplacer;
                })
                .ForEach(materialAndReplacer => materialAndReplacer.Item2.ForEachOr(
                    (replacer, material) => replacer.Replace(material), materialAndReplacer.Item1,
                    material => SetShader(material, GetShader(material.shader.name)), materialAndReplacer.Item1));
        }

        public static void LogShaders()
        {
            var fixedLogString = "Replaced shaders:\n" + _raplacedShadersCounter.OrderByDescending(pair => pair.Key)
                .Aggregate(new StringBuilder(), (b, pair) => b.Append($"{pair.Key}: {pair.Value}\n"));
            var notFixedLogString = "Not replaced shaders:\n" +  _notReplacedShadersCounter.OrderByDescending(pair => pair.Key)
                .Aggregate(new StringBuilder(), (b, pair) => b.Append($"{pair.Key}: {pair.Value}\n"));

//            typeof(ShadersFixer).LogInfo(fixedLogString, true);
//            typeof(ShadersFixer).LogInfo(notFixedLogString, true);
        }

        private static void AddShaderResolutionForLogging(string shaderName, Option<IShaderReplacer> replacer)
        {
            var dictionary = replacer.Cata(_ => _raplacedShadersCounter, _notReplacedShadersCounter);
            if (dictionary.ContainsKey(shaderName)) dictionary[shaderName]++;
            else dictionary.Add(shaderName, 1);
        }

        // TODO: temporary changed to public to check shader replacement
        public static void SetShader(Material material, Shader shader)
        {
            var queue = material.renderQueue;
            material.shader = shader;
            material.renderQueue = queue;
        }

        // TODO: temporary changed to public to check shader replacement
        public static Shader GetShader(string name)
        {
            if (_shaders.ContainsKey(name))
                return _shaders[name];

            if (_missingShaders.Contains(name))
                return _defaultShader;

            var shader = Shader.Find(name);
            if (shader != null)
            {
                _shaders.Add(name, shader);
                return shader;
            }

//            typeof(AssetBundleBuilder).LogError($"Can't find shader with name \"{name}\"", true);
            _missingShaders.Add(name);
            return _defaultShader;
        }
    }
}