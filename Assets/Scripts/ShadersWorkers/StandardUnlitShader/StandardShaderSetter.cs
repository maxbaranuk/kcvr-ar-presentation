﻿using System;
using ShadersWorkers.StandardUnlitShader;
using UnityEngine;

// ReSharper disable UseNameofExpression

namespace ContentTransformation.ShadersWorkers.StandardUnlitShader
{
    public static class StandardShaderSetter
    {
        private static readonly Shader _standardShader = Shader.Find(StandardShaderConstants.SHADER_NAME);

        // Properties
        private static readonly int _mainTexturePropertyId = Shader.PropertyToID(StandardShaderConstants.MAIN_TEXTURE_PROPERTY_NAME);
        private static readonly int _lightmapTexturePropertyId = Shader.PropertyToID(StandardShaderConstants.LIGHTMAP_TEXTURE_PROPERTY_NAME);
        private static readonly int _normalTexturePropertyId = Shader.PropertyToID(StandardShaderConstants.NORMAL_TEXTURE_PROPERTY_NAME);
        private static readonly int _specularTexturePropertyId = Shader.PropertyToID(StandardShaderConstants.SPECULAR_TEXTURE_PROPERTY_NAME);
        private static readonly int _reflectionCubePropertyId = Shader.PropertyToID(StandardShaderConstants.REFLECTION_CUBE_PROPERTY_NAME);
        private static readonly int _mainColorPropertyId = Shader.PropertyToID(StandardShaderConstants.MAIN_COLOR_PROPERTY_NAME);
        private static readonly int _specularColorPropertyId = Shader.PropertyToID(StandardShaderConstants.SPECULAR_COLOR_PROPERTY_NAME);
        private static readonly int _cutoffPropertyId = Shader.PropertyToID(StandardShaderConstants.CUTOFF_PROPERTY_NAME);
        private static readonly int _hasReflectionPropertyId = Shader.PropertyToID(StandardShaderConstants.HAS_REFLECTION_PROPERTY_NAME);
        private static readonly int _blendModePropertyId = Shader.PropertyToID(StandardShaderConstants.BLEND_MODE_PROPERTY_NAME);
        private static readonly int _srcBlendPropertyId = Shader.PropertyToID(StandardShaderConstants.SRC_BLEND_PROPERTY_NAME);
        private static readonly int _dstBlendPropertyId = Shader.PropertyToID(StandardShaderConstants.DST_BLEND_PROPERTY_NAME);
        private static readonly int _zWritePropertyId = Shader.PropertyToID(StandardShaderConstants.ZWRITE_PROPERTY_NAME);

        public static Material CreateMaterial(StandardShaderProperties properties)
        {
            var material = new Material(_standardShader);
            ReplaceMaterialShader(material, properties);
            return material;
        }

        public static void ReplaceMaterialShader(Material material, StandardShaderProperties properties)
        {
            material.shader = _standardShader;
            material.SetTexture(_mainTexturePropertyId, properties.MainTexture);
            material.SetTexture(_lightmapTexturePropertyId, properties.LightmapTexture);
            material.SetTexture(_normalTexturePropertyId, properties.NormalmapTexture);
            material.SetTexture(_specularTexturePropertyId, properties.SpecularTexture);
            material.SetTexture(_reflectionCubePropertyId, properties.ReflectionCubemap);
            material.SetColor(_mainColorPropertyId, properties.MainColor);
            material.SetColor(_specularColorPropertyId, properties.SpecularColor);
            material.SetFloat(_cutoffPropertyId, properties.CutoffValue);
            material.SetFloat(_hasReflectionPropertyId, properties.ReflectionType == StandardShaderReflectionType.NoReflection ? 0 : 1);
            material.SetFloat(_blendModePropertyId, (float)properties.ObjectBlendMode);
            ChangeRenderingMode(material, properties.ObjectBlendMode);

            var isCutout = properties.ObjectBlendMode == StandardShaderBlendMode.Cutout;
            var hasLightmap = properties.LightmapTexture != null;
            var hasNormal = properties.NormalmapTexture != null;
            ChangeShaderKeywords(material, isCutout, hasLightmap, hasNormal, properties.ReflectionType);
        }

        public static void ChangeRenderingMode(Material material, StandardShaderBlendMode blendMode)
        {
            switch (blendMode)
            {
                case StandardShaderBlendMode.Opaque:
                    material.SetOverrideTag("RenderType", "Opaque");
                    material.SetInt(_srcBlendPropertyId, (int) UnityEngine.Rendering.BlendMode.One);
                    material.SetInt(_dstBlendPropertyId, (int) UnityEngine.Rendering.BlendMode.Zero);
                    material.SetInt(_zWritePropertyId, 1);
                    material.DisableKeyword(StandardShaderConstants.HAS_CUTOFF_KEYWORD);
                    material.renderQueue = 1;
                    break;
                case StandardShaderBlendMode.Cutout:
                    material.SetOverrideTag("RenderType", "TransparentCutout");
                    material.SetInt(_srcBlendPropertyId, (int) UnityEngine.Rendering.BlendMode.One);
                    material.SetInt(_dstBlendPropertyId, (int) UnityEngine.Rendering.BlendMode.Zero);
                    material.SetInt(_zWritePropertyId, 1);
                    material.EnableKeyword(StandardShaderConstants.HAS_CUTOFF_KEYWORD);
                    material.renderQueue = 2450;
                    break;
                case StandardShaderBlendMode.Transparent:
                    material.SetOverrideTag("RenderType", "Transparent");
                    material.SetInt(_srcBlendPropertyId, (int) UnityEngine.Rendering.BlendMode.SrcAlpha);
                    material.SetInt(_dstBlendPropertyId, (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    material.SetInt(_zWritePropertyId, 0);
                    material.DisableKeyword(StandardShaderConstants.HAS_CUTOFF_KEYWORD);
                    material.renderQueue = 3000;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("blendMode", blendMode, null);
            }
        }

        public static void ChangeShaderKeywords(Material material, bool isCutout, bool hasLightmap, bool hasNormal,
            StandardShaderReflectionType reflection)
        {
            ChangeKeyword(material, StandardShaderConstants.HAS_LIGHTMAP_KEYWORD, hasLightmap);
            ChangeKeyword(material, StandardShaderConstants.HAS_NORMALMAP_KEYWORD, hasNormal);
            ChangeKeyword(material, StandardShaderConstants.HAS_CUTOFF_KEYWORD, isCutout);
            ChangeKeyword(material, StandardShaderConstants.NO_REFLECTION_KEYWORD, reflection == StandardShaderReflectionType.NoReflection);
            ChangeKeyword(material, StandardShaderConstants.REFLECTION_FROM_COLOR_KEYWORD, reflection == StandardShaderReflectionType.ReflectionFromSpecularColor);
            ChangeKeyword(material, StandardShaderConstants.REFLECTION_FROM_MAP_KEYWORD, reflection == StandardShaderReflectionType.ReflectionFromSpecularTexture);
        }

        private static void ChangeKeyword(Material material, string keyword, bool newValue)
        {
            if (newValue)
            {
                material.EnableKeyword(keyword);
            }
            else
            {
                material.DisableKeyword(keyword);
            }
        }
    }
}