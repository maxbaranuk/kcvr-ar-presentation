﻿namespace ContentTransformation.ShadersWorkers.StandardUnlitShader
{
    public enum StandardShaderReflectionType
    {
        NoReflection,
        ReflectionFromSpecularColor,
        ReflectionFromSpecularTexture
    }
}