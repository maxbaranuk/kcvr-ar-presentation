﻿namespace ContentTransformation.ShadersWorkers.StandardUnlitShader
{
    public static class StandardShaderConstants
    {
        public const string SHADER_NAME = "Unlit/Transparent";

        // Properties
        public const string MAIN_TEXTURE_PROPERTY_NAME = "_MainTex";
        public const string LIGHTMAP_TEXTURE_PROPERTY_NAME = "_LightmapTex";
        public const string NORMAL_TEXTURE_PROPERTY_NAME = "_NormalmapTex";
        public const string SPECULAR_TEXTURE_PROPERTY_NAME = "_SpecularmapTex";
        public const string REFLECTION_CUBE_PROPERTY_NAME = "_Cube";
        public const string MAIN_COLOR_PROPERTY_NAME = "_Color";
        public const string SPECULAR_COLOR_PROPERTY_NAME = "_SpecularColor";
        public const string CUTOFF_PROPERTY_NAME = "_Cutoff";
        public const string HAS_REFLECTION_PROPERTY_NAME = "_HasReflection";

        // Hidden properties
        public const string BLEND_MODE_PROPERTY_NAME = "_Mode";
        public const string SRC_BLEND_PROPERTY_NAME = "_SrcBlend";
        public const string DST_BLEND_PROPERTY_NAME = "_DstBlend";
        public const string ZWRITE_PROPERTY_NAME = "_ZWrite";

        // Keywords
        public const string HAS_LIGHTMAP_KEYWORD = "_HAS_LIGHTMAP";
        public const string HAS_CUTOFF_KEYWORD = "_HAS_CUTOFF";
        public const string HAS_NORMALMAP_KEYWORD = "_HAS_NORMALMAP";
        
        public const string NO_REFLECTION_KEYWORD = "_NO_REFLECTION";
        public const string REFLECTION_FROM_COLOR_KEYWORD = "_REFLECTION_FROM_COLOR";
        public const string REFLECTION_FROM_MAP_KEYWORD = "_REFLECTION_FROM_MAP";
    }
}