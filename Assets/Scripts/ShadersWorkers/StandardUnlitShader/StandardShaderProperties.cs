﻿using ContentTransformation.ShadersWorkers.StandardUnlitShader;
using Smooth.Algebraics;
using UnityEngine;

namespace ShadersWorkers.StandardUnlitShader
{
    public sealed class StandardShaderProperties
    {
        public Texture MainTexture { get; private set; }
        public Texture LightmapTexture { get; private set; }
        public Texture NormalmapTexture { get; private set; }
        public Option<Color> LightMapColor { get; private set; }
        public Texture SpecularTexture { get; private set; }
        public Texture ReflectionCubemap { get; private set; }
        public Color MainColor { get; private set; }
        public Color SpecularColor { get; private set; }
        public float CutoffValue { get; private set; }
        public StandardShaderReflectionType ReflectionType { get; private set; }
        public StandardShaderBlendMode ObjectBlendMode { get; private set; }

        private StandardShaderProperties()
        {
            MainColor = Color.white;
            ReflectionType = StandardShaderReflectionType.NoReflection;
            ObjectBlendMode = StandardShaderBlendMode.Opaque;
        }

        public static StandardShaderProperties CreateDefault()
        {
            return new StandardShaderProperties();
        }

        public StandardShaderProperties SetAsOpaque()
        {
            ObjectBlendMode = StandardShaderBlendMode.Opaque;
            return this;
        }

        public StandardShaderProperties SetAsCutout(float cutoffValue)
        {
            CutoffValue = Mathf.Clamp01(cutoffValue);
            ObjectBlendMode = StandardShaderBlendMode.Cutout;
            return this;
        }

        public StandardShaderProperties SetAsTransparent()
        {
            ObjectBlendMode = StandardShaderBlendMode.Transparent;
            return this;
        }

        public StandardShaderProperties SetMainTexture(Texture texture)
        {
            MainTexture = texture;
            return this;
        }

        public StandardShaderProperties EnableLightMapColorSettings()
        {
            LightMapColor = Color.white.ToSome();
            return this;
        }
        
        public StandardShaderProperties SetLightmapTexture(Texture texture)
        {
            LightmapTexture = texture;
            return this;
        }

        public StandardShaderProperties SetNormalTexture(Texture texture)
        {
            NormalmapTexture = texture;
            return this;
        }

        public StandardShaderProperties SetSpecular(Texture specularTexture, Texture reflectionCubemap)
        {
            ReflectionType = StandardShaderReflectionType.ReflectionFromSpecularTexture;
            SpecularTexture = specularTexture;
            ReflectionCubemap = reflectionCubemap;
            return this;
        }

        public StandardShaderProperties SetSpecular(Color specularColor, Texture reflectionCubemap)
        {
            ReflectionType = StandardShaderReflectionType.ReflectionFromSpecularColor;
            SpecularColor = specularColor;
            ReflectionCubemap = reflectionCubemap;
            return this;
        }

        public StandardShaderProperties SetMainColor(Color color)
        {
            MainColor = color;
            return this;
        }
    }
}