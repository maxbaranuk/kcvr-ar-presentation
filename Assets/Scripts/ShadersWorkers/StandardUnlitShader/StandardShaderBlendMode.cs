﻿namespace ContentTransformation.ShadersWorkers.StandardUnlitShader
{
    public enum StandardShaderBlendMode
    {
        Opaque = 0,
        Cutout = 1,
        Transparent = 2
    }
}