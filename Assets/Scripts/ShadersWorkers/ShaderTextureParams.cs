﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Smooth.Algebraics;
using Smooth.Slinq;
using UnityEngine;

namespace ContentTransformation.ShadersWorkers
{
    [UsedImplicitly]
    public class ShaderTextureParams : ScriptableObject
    {
        private const string RESOURCE_PATH = "Shaders/shaderTextureParams";

        private static ShaderTextureParams _shaderParams;

        [Serializable]
        private class StringToStringKvp : InspectorKeyValuePair<string, string> { }

        public static ShaderTextureParams Load() => _shaderParams ?? (_shaderParams = Resources.Load<ShaderTextureParams>(RESOURCE_PATH));

        public Dictionary<string, List<string>> ShaderNameToTextureParamsMapping
        {
            get
            {
                return _shaderNameToTextureParamsMapping ??
                    (_shaderNameToTextureParamsMapping = _backingList.GroupBy(kvp => kvp.key)
                        .ToDictionary(ig => ig.Key, val => val.Select(kvp => kvp.value).ToList()));
            }
        }

        private Dictionary<string, List<string>> _shaderNameToTextureParamsMapping;

        [SerializeField]
        private List<StringToStringKvp> _backingList = new List<StringToStringKvp>();

        public void Set(List<Smooth.Algebraics.Tuple<string, List<string>>> shaderParams)
        {
            _backingList.Clear();
            shaderParams.Slinq()
                .SelectMany(t => t.Item2.Slinq().Select((val, key) =>
                    new StringToStringKvp() { key = key, value = val }, t.Item1))
                .AddTo(_backingList);
            _shaderNameToTextureParamsMapping = null;
        }
    }
}