﻿using System;
using System.Linq;
using JetBrains.Annotations;
using Smooth.Slinq;
using UnityEngine;

namespace ContentTransformation.ShadersWorkers
{
    // TODO: don't remove, needed to check future shaders replacement.
    public sealed class ShaderReverter : MonoBehaviour
    {
        private MeshRenderer _meshRenderer;
        private Material[] _savedMaterials;
        
        public static void AttachTo(MeshRenderer meshRenderer)
        {
            var reverter = meshRenderer.gameObject.AddComponent<ShaderReverter>();
            reverter._meshRenderer = meshRenderer;
            reverter._savedMaterials = meshRenderer.materials
                .Select(material => new Material(material)).ToArray();
            try
            {
                reverter._savedMaterials.Slinq()
                    .Where(material => material != null)
                    .ForEach(material => ShadersFixer.SetShader(material, ShadersFixer.GetShader(material.shader.name)));
            }
            catch (Exception ex)
            {
//                typeof(ShaderReverter).LogException(ex);
            }
        }

        [UsedImplicitly]
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F7))
                Revert();
        }

        private void Revert()
        {
            var tempMaterials = _meshRenderer.materials;
            _meshRenderer.materials = _savedMaterials;
            _savedMaterials = tempMaterials;
        }
    }
}