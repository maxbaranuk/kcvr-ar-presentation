﻿using UnityEngine;

namespace ContentTransformation.ShadersWorkers.Replacers
{
    public static class ReplacersHelper
    {
        public static Texture2D GetTextureFromMaterial(Texture texture, Material material)
        {
            var width = texture.width;
            var height = texture.height;

            var renderTexture = RenderTexture.GetTemporary(width, height, 0, RenderTextureFormat.ARGB32);
            Graphics.Blit(texture, renderTexture, material, 0);

            var originalRenderTexture = RenderTexture.active;
            RenderTexture.active = renderTexture;
            var specularTexture = new Texture2D(width, texture.height, TextureFormat.ARGB32, true);
            specularTexture.ReadPixels(new Rect(0, 0, width, height), 0, 0, true);
            specularTexture.Compress(false);
            specularTexture.Apply(true, true);
            RenderTexture.active = originalRenderTexture;

            RenderTexture.ReleaseTemporary(renderTexture);

            return specularTexture;
        }
    }
}