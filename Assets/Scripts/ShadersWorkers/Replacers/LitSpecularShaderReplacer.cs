﻿using ContentTransformation.ShadersWorkers.StandardUnlitShader;
using JetBrains.Annotations;
using ShadersWorkers.StandardUnlitShader;
using UnityEngine;

namespace ContentTransformation.ShadersWorkers.Replacers
{
    [UsedImplicitly]
    public class LitSpecularShaderReplacer : IShaderReplacer
    {
        private static readonly Material _material = new Material(Shader.Find("Hidden/LitSpecularReplacerShader"));

        public string ShaderName => "RDSS/LitSpecular";

        public void Replace(Material material)
        {
            var specularColor = material.GetColor("_ReflectColor");
            var mainColor = material.GetColor("_Color")*1.264f + specularColor;
            var mainTexture = material.mainTexture;
            var specularTexture = CreateSpecularTexture(specularColor, mainTexture);
            var reflectionCubeMap = material.GetTexture("_Cube");

            var materialProperties = StandardShaderProperties.CreateDefault()
                .SetMainTexture(mainTexture)
                .SetMainColor(mainColor)
                .SetSpecular(specularTexture, reflectionCubeMap as Cubemap);

            StandardShaderSetter.ReplaceMaterialShader(material, materialProperties);
        }

        private Texture CreateSpecularTexture(Color specularColor, Texture mainTexture)
        {
            _material.SetColor("_SpecularColor", specularColor);
            return ReplacersHelper.GetTextureFromMaterial(mainTexture, _material);
        }
    }
}