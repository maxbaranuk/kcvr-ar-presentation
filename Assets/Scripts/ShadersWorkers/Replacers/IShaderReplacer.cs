﻿using UnityEngine;

namespace ContentTransformation.ShadersWorkers.Replacers
{
    public interface IShaderReplacer
    {
        string ShaderName { get; }
        void Replace(Material material);
    }
}