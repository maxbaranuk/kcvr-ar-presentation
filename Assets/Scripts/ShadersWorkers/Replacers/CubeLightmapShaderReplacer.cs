﻿using ContentTransformation.ShadersWorkers.StandardUnlitShader;
using ShadersWorkers.StandardUnlitShader;
using UnityEngine;

namespace ContentTransformation.ShadersWorkers.Replacers
{
    public class CubeLightmapShaderReplacer : IShaderReplacer
    {
        public string ShaderName => "RDSS/CubeLightmap";

        public void Replace(Material material)
        {
            var specularColor = material.GetColor("_ReflectColor");
            var mainColor = (Color.white + specularColor)*1.244f;
            var reflectionCubeMap = material.GetTexture("_Cube");
            var lightmap = material.GetTexture("_LightMap");
            var materialProperties = StandardShaderProperties.CreateDefault()
                .SetMainTexture(material.mainTexture)
                .SetMainColor(mainColor)
                .SetSpecular(specularColor, reflectionCubeMap as Cubemap)
                .SetLightmapTexture(lightmap);
            StandardShaderSetter.ReplaceMaterialShader(material, materialProperties);
        }
    }
}