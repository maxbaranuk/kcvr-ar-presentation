﻿using ContentTransformation.ShadersWorkers.StandardUnlitShader;
using JetBrains.Annotations;
using ShadersWorkers.StandardUnlitShader;
using UnityEngine;

namespace ContentTransformation.ShadersWorkers.Replacers
{
    [UsedImplicitly]
    public class UnlitCutoutShaderReplacer : IShaderReplacer
    {
        public string ShaderName => "Unlit/Transparent Cutout";

        public void Replace(Material material)
        {
            var cutoff = material.GetFloat("_Cutoff");
            var materialProperties = StandardShaderProperties.CreateDefault()
                .SetMainTexture(material.mainTexture)
                .SetAsCutout(cutoff);
            StandardShaderSetter.ReplaceMaterialShader(material, materialProperties);
        }
    }
}