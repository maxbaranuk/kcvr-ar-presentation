﻿using ContentTransformation.ShadersWorkers.StandardUnlitShader;
using JetBrains.Annotations;
using ShadersWorkers.StandardUnlitShader;
using UnityEngine;

namespace ContentTransformation.ShadersWorkers.Replacers
{
    [UsedImplicitly]
    public class LightMappedTransparentShaderReplacer : IShaderReplacer
    {
        public string ShaderName => "RDSS/LightMapped-Transparent";

        public void Replace(Material material)
        {
            var lightmapTexture = material.GetTexture("_LightMap");
            var materialProperties = StandardShaderProperties.CreateDefault()
                .SetAsTransparent()
                .SetMainTexture(material.mainTexture)
                .SetLightmapTexture(lightmapTexture);
            StandardShaderSetter.ReplaceMaterialShader(material, materialProperties);
            material.SetInt(StandardShaderConstants.ZWRITE_PROPERTY_NAME, 1); // TODO: check if it could be opaque
            material.renderQueue = 2999; // TODO: check if it could be opaque
        }
    }
}