﻿using ContentTransformation.ShadersWorkers.StandardUnlitShader;
using JetBrains.Annotations;
using ShadersWorkers.StandardUnlitShader;
using UnityEngine;

namespace ContentTransformation.ShadersWorkers.Replacers
{
    [UsedImplicitly]
    public class UnlitTransparentShaderReplacer : IShaderReplacer
    {
        public string ShaderName => "Unlit/Transparent";

        public void Replace(Material material)
        {
            var materialProperties = StandardShaderProperties.CreateDefault()
                .SetMainTexture(material.mainTexture)
                .SetAsTransparent();
            StandardShaderSetter.ReplaceMaterialShader(material, materialProperties);
        }
    }
}