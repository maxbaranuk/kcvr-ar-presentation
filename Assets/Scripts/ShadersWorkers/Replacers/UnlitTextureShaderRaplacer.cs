﻿using ContentTransformation.ShadersWorkers.StandardUnlitShader;
using JetBrains.Annotations;
using ShadersWorkers.StandardUnlitShader;
using UnityEngine;

namespace ContentTransformation.ShadersWorkers.Replacers
{
    [UsedImplicitly]
    public sealed class UnlitTextureShaderRaplacer : IShaderReplacer
    {
        public string ShaderName => "Unlit/Texture";

        public void Replace(Material material)
        {
            var materialProperties = StandardShaderProperties.CreateDefault().SetMainTexture(material.mainTexture);
            StandardShaderSetter.ReplaceMaterialShader(material, materialProperties);
        }
    }
}