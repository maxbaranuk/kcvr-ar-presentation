﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using Meta.DataClasses.Merchandising.Products;
using SamplesResources.SceneAssets.CloudReco.Scripts;
using Smooth.Foundations.Algebraics;
using Smooth.Slinq;
using UnityEngine;
using Utils.Algebraic;

public static class AssetProvider
{
	private static readonly Dictionary<string, Texture2D> TextureCache = new Dictionary<string, Texture2D>();
	private static Dictionary<Uri, string> _textCache = new Dictionary<Uri, string>();
	public static List<Product> LibraryProducts = new List<Product>();
	private static readonly string TexturesPath = Application.persistentDataPath + "/textures";
	
	public static void AddTexture(Uri key, Texture2D texture)
	{
		var uuid = key.Segments.Slinq().Last();
		if(!TextureCache.ContainsKey(uuid))
			TextureCache.Add(uuid, texture);
	}

	public static void AddText(Uri key, string text)
	{

		if (!_textCache.ContainsKey(key))
			_textCache.Add(key, text);
	}

	public static async Task<ValueOrError<Texture2D>> GetTexture(Uri key)
	{
		var uuid = key.Segments.Slinq().Last();

		return TextureCache.ContainsKey(uuid)
			? ValueOrError<Texture2D>.FromValue(TextureCache[uuid])
			: await NetworkProvider.GetTexture(key)
			.ContinueWithAsync((texture, filename) => {
			{
				if (!Directory.Exists(TexturesPath))
					Directory.CreateDirectory(TexturesPath);
				
				var bytes = texture.EncodeToPNG();
				File.WriteAllBytes($"{TexturesPath}/{filename}.png", bytes);
				return texture;
			}}, key.Segments.Slinq().Last());		
	}
	
	public static async Task<ValueOrError<string>> GetTextAsset(Uri data)
	{
		return _textCache.ContainsKey(data)
		    ? ValueOrError<string>.FromValue(_textCache[data])
			: await NetworkProvider.GetText(data);			
	}
	
	public static void Save() {
		var bf = new BinaryFormatter();	
		var textFile = File.Create(Application.persistentDataPath + "/TextCache.data");
		bf.Serialize(textFile, _textCache);
		textFile.Close();
		var libraryFile = File.Create(Application.persistentDataPath + "/library.data");
		bf.Serialize(libraryFile, LibraryProducts);
		libraryFile.Close();
	}
    
	public static void Load()
	{
		var bf = new BinaryFormatter();
		
		if (!Directory.Exists(TexturesPath))
			Directory.CreateDirectory(TexturesPath);
		
		var files = Directory.GetFiles(TexturesPath);
		
		foreach (var fileName in files)
		{
			var filePath = $"{TexturesPath}/{fileName}.png";
			if (!File.Exists(filePath)) continue;
			
			var fileData = File.ReadAllBytes(filePath);
			var tex = new Texture2D(2, 2);
			tex.LoadImage(fileData);
			TextureCache.Add(fileName, tex);
		}
	
		if (File.Exists(Application.persistentDataPath + "/TextCache.data"))
		{
			var textFile = File.Open(Application.persistentDataPath + "/TextCache.data", FileMode.Open);
			_textCache = (Dictionary<Uri, string>) bf.Deserialize(textFile);
			textFile.Close();
		}
		
		if (File.Exists(Application.persistentDataPath + "/library.data"))
		{
			var textFile = File.Open(Application.persistentDataPath + "/library.data", FileMode.Open);
			LibraryProducts = (List<Product>) bf.Deserialize(textFile);
			textFile.Close();
		}

		LibraryProducts.Slinq()
			.ForEach(CloudRecoContentManager.AddProductToLibrary);
	}
}
