﻿using Meta.DataClasses.Merchandising.Products;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class LibraryList : MonoBehaviour
{

	private GameObject _itemPrefab;
	
	private void Start()
	{
		_itemPrefab = Resources.Load<GameObject>("GalleryItemPrefab");
	}

	public void AddItem(Product product)
	{
		var item = Instantiate(_itemPrefab, transform.Find("Content"));
		var comp = item.AddComponent<ModelButton>();
		comp.SetProduct(product);
		item.GetComponent<Button>().OnClickAsObservable().Subscribe( async _ =>
		{
			await ContentBuilder.CreateProductModel(product);
		});
		item.SetActive(true);
	}

}
