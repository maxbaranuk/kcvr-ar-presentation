﻿using System.Collections.Generic;
using SamplesResources.SceneAssets.CloudReco.Scripts;
using UnityEngine;

public class SceneModel : MonoBehaviour
{
	public bool IsNew = true;
	[SerializeField] 
	private GameObject _moveGizmo;
	
	[SerializeField] 
	private GameObject _rotateGizmo;
	
	private GameObject _placeHolder;
	
	private Material _previewMaterial;


	private Dictionary<Renderer, Material[]> _previewItems;

	private void Awake()
	{
		_moveGizmo = transform.Find("Move").gameObject;
		_rotateGizmo = transform.Find("Rotate").gameObject;
		_previewMaterial = Resources.Load<Material>("PlaceHolder");
		
		_previewItems = new Dictionary<Renderer, Material[]>();

		var renders = transform.GetChild(0).GetComponentsInChildren<MeshRenderer>();
				
		foreach (var meshRenderer in renders)
		{			
			if (!meshRenderer.transform.name.Equals("Move") && !meshRenderer.transform.name.Equals("Rotate"))
				_previewItems.Add(meshRenderer, meshRenderer.materials);
		}
		
		_rotateGizmo.SetActive(false);
	}

	private void Start () {
		
		foreach (var previewItem in _previewItems.Keys)
		{
			var list = new List<Material>();
			for (var i = 0; i < previewItem.materials.Length; i++)
			{
				list.Add(_previewMaterial);
			}

			previewItem.materials = list.ToArray();

		}
		UiController.Instance.StartEditModel(this);
	}
	
	public void Place() {
		foreach (var previewItem in _previewItems.Keys)
		{
			previewItem.materials = _previewItems[previewItem];
		}
		UiController.Instance.FinishEditModel();
		IsNew = false;
	}
	
	public void Edit()
	{
		if (CloudRecoContentManager.Instance.CurrentModel)
			CloudRecoContentManager.Instance.CurrentModel.CloseGizmos();
		CloudRecoContentManager.Instance.CurrentModel = this;
		UiController.Instance.StartEditModel(this);
	}
	
	public void ShowMoveGizmo()
	{
		_moveGizmo.SetActive(true);
		_rotateGizmo.SetActive(false);
	}
	
	public void ShowRotateGizmo()
	{
		_rotateGizmo.SetActive(true);
		_moveGizmo.SetActive(false);
	}
	
	public void CloseGizmos()
	{
		_moveGizmo.SetActive(false);
		_rotateGizmo.SetActive(false);
	}
}
