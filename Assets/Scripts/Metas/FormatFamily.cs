namespace Meta.DataClasses
{
    public enum FormatFamily
    {
        UNDEFINED,

        #region DataViz

        COLORIZATION_CONFIG_DATA_VIZ_FORMAT,
        CSV_SOURCE_DATA_VIZ_FORMAT,
        DATACARD_CONFIG_DATA_VIZ_FORMAT,
        PSA_CONFIG_DATA_VIZ_FORMAT,
        RICH_MIX_DATA_VIZ_FORMAT,
        SELLDOWN_CONFIG_DATA_VIZ_FORMAT,

        #endregion

        #region Images

        GIF,
        JPEG,
        PNG,
        TGA,
        TIFF,

        #endregion

        #region Videos

        MPEG_4,

        #endregion
        
        JDA_SPACE_PLANNING_FORMAT,

        UNITY_ASSET_BUNDLE,
        OBJ,

        JSON
    }
}