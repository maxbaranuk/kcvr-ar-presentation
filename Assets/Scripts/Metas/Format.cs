using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Meta.DataClasses
{
    public class Format
    {
        [JsonProperty("@type")]
        public string Type => "Format";

        [JsonProperty("formatFamily")]
        [JsonConverter(typeof(StringEnumConverter))]
        public FormatFamily FormatFamily { get; set; }

        [JsonProperty("formatVersion")]
        public string FormatVersion { get; set; }

        public override string ToString()
            => $"{typeof(FormatFamily)}.{FormatFamily}";
    }
}