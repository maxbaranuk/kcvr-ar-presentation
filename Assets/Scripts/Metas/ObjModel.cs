﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Smooth.Foundations.Algebraics;

namespace Meta.DataClasses.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ObjModel : Model
    {
        [JsonProperty("materialSet")]
        public List<MaterialInformation> Materials { get; set; }

        public Task<ValueOrError<string>> ObjTextData => AssetProvider.GetTextAsset(Data);
    }
}
