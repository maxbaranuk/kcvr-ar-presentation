﻿using Meta.DataClasses.Models;
using Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Meta.DataClasses.Geometry
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Quad
    {
        [JsonProperty("@type")]
        public string Type => "Quad";

        [JsonProperty("face")]
        [JsonConverter(typeof(StringEnumConverter))]
        public QuadFace QuadFace { get; set; }

        [JsonProperty("texture")]
        public ModelTexture ModelTexture{ get; set; }

        public override string ToString()
        {
            return $"quad face={QuadFace}, {ModelTexture}";
        }
    }
}
