﻿using System;
using System.Collections.Generic;
using Meta.JsonConverters;
using Newtonsoft.Json;
using Utils.Collections;

namespace Metas
{
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable]
    public abstract class Meta : IEquatable<Meta>
    {
        protected const string SELF_URI_NAME = "self";

        [JsonProperty("_links")]
        [JsonConverter(typeof(LinkDictionaryConverter))]
        protected  Dictionary<string, Uri> Links { get; set; } = new Dictionary<string, Uri>();

        public Uri SelfUri => Links.TryGet(SELF_URI_NAME).ValueOr((Uri)null);

        public void SetSelfUri(Uri uri) => Links[SELF_URI_NAME] = uri;

        public bool Equals(Meta other)
        {
            return other != null
                && GetType() == other.GetType()
                && (SelfUri != null)
                && SelfUri == other.SelfUri;
        }
    }
}
