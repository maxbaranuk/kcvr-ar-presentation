﻿using System;
using System.Threading.Tasks;
using Meta.JsonConverters;
using Models;
using Newtonsoft.Json;
using Smooth.Algebraics;
using Smooth.Foundations.Algebraics;
using Utils.Algebraic;
using Utils.Collections;

[Serializable]
public class AssetMeta : Metas.Meta
{
    [JsonProperty("displayName")]
    public string DisplayName { get; set; }

    [JsonProperty("thumb128")]
    [JsonConverter(typeof(UriJsonConverter))]
    protected Uri Thumb128 { get; set; }

    [JsonProperty("thumb256")]
    [JsonConverter(typeof(UriJsonConverter))]
    protected Uri Thumb256 { get; set; }

    [JsonProperty("thumb512")]
    [JsonConverter(typeof(UriJsonConverter))]
    protected Uri Thumb512 { get; set; }

    public Task<ValueOrError<ModelInformation>> ModelInformation
    {
        get
        {
            var uri = Links.TryGet("modelInformation");
            if (uri.isNone)
                return Task.FromResult(ValueOrError<ModelInformation>.FromError("No uri in modelInformation"));
            
            return AssetProvider.GetTextAsset(uri.value)
                .ContinueWithAsync(s =>
                {
                    return JsonConvert.DeserializeObject<ModelInformation>(s);
                });
        }
    }

    public Option<string> GetThumbnail()
    {
        if (Thumb128 != null)
            return new Option<string>(Thumb128.AbsoluteUri);
        if (Thumb256 != null)
            return new Option<string>(Thumb256.AbsoluteUri);
        return Thumb512 != null ? new Option<string>(Thumb512.AbsoluteUri) : Option<string>.None;
    }
}