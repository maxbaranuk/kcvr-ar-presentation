﻿
namespace Meta.DataClasses.Models
{

    /// <summary>
    /// Axes convention named individuals.
    /// </summary>
    public enum AxesConvention_1
    {
        AxesUndefined,
        AxesXDownYInZLeft,
        AxesXDownYInZRight,
        AxesXDownYLeftZIn,
        AxesXDownYLeftZOut,
        AxesXDownYOutZLeft,
        AxesXDownYOutZRight,
        AxesXDownYRightZIn,
        AxesXDownYRightZOut,
        AxesXInYDownZLeft,
        AxesXInYDownZRight,
        AxesXInYLeftZDown,
        AxesXInYLeftZUp,
        AxesXInYRightZDown,
        AxesXInYRightZUp,
        AxesXInYUpZLeft,
        AxesXInYUpZRight,
        AxesXLeftYDownZIn,
        AxesXLeftYDownZOut,
        AxesXLeftYInZDown,
        AxesXLeftYInZUp,
        AxesXLeftYOutZDown,
        AxesXLeftYOutZUp,
        AxesXLeftYUpZIn,
        AxesXLeftYUpZOut,
        AxesXOutYDownZLeft,
        AxesXOutYDownZRight,
        AxesXOutYLeftZDown,
        AxesXOutYLeftZUp,
        AxesXOutYRightZDown,
        AxesXOutYRightZUp,
        AxesXOutYUpZLeft,
        AxesXOutYUpZRight,
        AxesXRightYDownZIn,
        AxesXRightYDownZOut,
        AxesXRightYInZDown,
        AxesXRightYInZUp,
        AxesXRightYOutZDown,
        AxesXRightYOutZUp,
        AxesXRightYUpZIn,
        AxesXRightYUpZOut,
        AxesXUpYInZLeft,
        AxesXUpYInZRight,
        AxesXUpYLeftZIn,
        AxesXUpYLeftZOut,
        AxesXUpYOutZLeft,
        AxesXUpYOutZRight,
        AxesXUpYRightZIn,
        AxesXUpYRightZOut

    }
}