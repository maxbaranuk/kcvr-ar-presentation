namespace Meta.DataClasses.Geometry
{
    public enum QuadFace
    {
        BOTTOM = 0,
        FRONT = 1,
        RIGHT = 2,
        BACK = 3,
        TOP = 4,
        LEFT = 5
    }
}