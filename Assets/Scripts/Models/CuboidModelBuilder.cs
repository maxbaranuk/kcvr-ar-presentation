﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Meta.DataClasses.Geometry;
using Meta.DataClasses.Models;
using Scene.SceneBuilding.Models;
using Smooth.Algebraics;
using Smooth.Dispose;
using Smooth.Foundations.Algebraics;
using Smooth.Pools;
using Smooth.Slinq;
using UnityEngine;
using Utils.Algebraic;
using Utils.Tasks;
using ModelMetaType = Meta.DataClasses.Models.ModelType;

namespace Models
{
    public static class CuboidModelBuilder
    {
        private const string DEFAULT_TEXTURE_FACE_NAME = "dummy";

        private static readonly CuboidAtlasProvider _cuboidAtlasProvider = new CuboidAtlasProvider(2, 1024);
        private static readonly Dictionary<QuadFace, Texture2D> DefaultTextures = new Dictionary<QuadFace, Texture2D>();
        
        public static Option<Task<ValueOrError<GameObject>>> TryGetModel(ModelInformation modelInformation)
        {
            return GetModelMeta<CuboidModelWithAtlas>(modelInformation, ModelMetaType.CuboidWithAtlas)
                .Select(BuildFromAtlas, modelInformation)
                .Or(mi => GetModelMeta<CuboidModel>(mi, ModelMetaType.Cuboid)
                    .Select(BuildCuboidAndUploadAtlas, mi), modelInformation);
        }

        private static async Task<ValueOrError<GameObject>> BuildCuboidAndUploadAtlas(CuboidModel cuboidMeta,
            ModelInformation modelInformation)
        {
            using (var texturesResult = await GetDisposableTextures(cuboidMeta))
            using (var textureDictionary = CreateTemporaryTextureDictionary(texturesResult.Textures))
            {
                var atlasResult = _cuboidAtlasProvider.GetAtlas(textureDictionary.value);

//                if (texturesResult.AllTexturesAreLoaded)
//                    UploadAtlas(atlasResult.Texture, atlasResult.Uvs, cuboidMeta.BoundsMetric, modelInformation);

                CompressAtlas(atlasResult.Texture);

                var material = _cuboidAtlasProvider.GetMaterialForAtlas(atlasResult.Texture);
                var mesh = new CuboidBuilder(cuboidMeta.BoundsMetric.size, atlasResult.Uvs).Build();

                return ValueOrError<GameObject>.FromValue(CreateGameObject(material, mesh));
            }
        }

        private static async Task<ValueOrError<GameObject>> BuildFromAtlas(CuboidModelWithAtlas atlasModel, ModelInformation modelInformation)
        {
            var cuboidMesh = new CuboidBuilder(atlasModel.BoundsMetric.size, atlasModel.Uvs.FaceToRectDictionary).Build();
            var result = await atlasModel.Atlas
                .ContinueWithAsync(atlas => _cuboidAtlasProvider.GetMaterialForAtlas(atlas))
                .ContinueWithAsync(CreateGameObject, cuboidMesh);
            if (!result.IsError)
                return result;

//            typeof(CuboidModelBuilder).LogWarning($"Can't load cuboid from atlas for {nameof(ModelInformation)} {modelInformation.SelfUri}. Error: {result.Error}", true);
            var cuboidModel = GetModelMeta<CuboidModel>(modelInformation, ModelMetaType.Cuboid);
            return await (cuboidModel.isSome
                ? ValueOrError.FromValue(cuboidModel.value)
                : ValueOrError<CuboidModel>.FromError($"Can't find CuboidModel for model with atlas (uri: {modelInformation.SelfUri})"))
                    .ContinueWithAsync(BuildCuboidAndUploadAtlas, modelInformation);
        }

        private static Option<T> GetModelMeta<T>(ModelInformation modelInformation, ModelMetaType modelType) where T : Model
            => modelInformation.ModelSet.Slinq()
                .FirstOrNone((model, type) => model.Type == type, modelType)
                .Select(model => model as T);

        private static GameObject CreateGameObject(Material material, Mesh mesh)
        {            
            var gameObject = new GameObject("Cuboid");
            gameObject.AddComponent<MeshRenderer>().sharedMaterial = material;
            gameObject.AddComponent<MeshFilter>().sharedMesh = mesh;
//            ModelTypeMarker.AttachTo(gameObject, ModelType.Cuboid);
            return gameObject;
        }

        private static async Task<TexturesLoadingResult> GetDisposableTextures(CuboidModel cuboidModel)
        {
            using (var tasks = GetTexturesFromCuboid(cuboidModel))
            {
                var texturesResult = await Task.WhenAll(tasks.value);
                var allTexturesAreLoaded = texturesResult.Slinq()
                    .SelectMany(quadAndTexture => quadAndTexture.Item2)
                    .All(error => !error.IsError);

                var dict = DictionaryPool<QuadFace, Disposable<Texture2D>>.Instance.BorrowDisposable();
                texturesResult.Slinq()
                    .Select(quadAndTexture => Smooth.Algebraics.Tuple.Create(quadAndTexture.Item1, GetTextureOrDefault(quadAndTexture.Item2, quadAndTexture.Item1)))
                    .Select(quadAndTexture => new KeyValuePair<QuadFace, Disposable<Texture2D>>(quadAndTexture.Item1, quadAndTexture.Item2))
                    .AddTo(dict);

                return new TexturesLoadingResult(dict, allTexturesAreLoaded);
            }
        }

        private static Disposable<List<Task<Smooth.Algebraics.Tuple<QuadFace, Option<ValueOrError<Disposable<Texture2D>>>>>>> GetTexturesFromCuboid(CuboidModel cuboidModel)
        {
            var tasks = ListPool<Task<Smooth.Algebraics.Tuple<QuadFace, Option<ValueOrError<Disposable<Texture2D>>>>>>.Instance.BorrowDisposable();
            ((QuadFace[])Enum.GetValues(typeof(QuadFace))).Slinq()
                .Select((quadFace, model) => Smooth.Algebraics.Tuple.Create(quadFace, model.Quads.Slinq().FirstOrNone((q, face) => q.QuadFace == face, quadFace)), cuboidModel)
                .Select(quadAndOption => GetCuboidTexture(quadAndOption.Item1, quadAndOption.Item2))
                .AddTo(tasks);
            return tasks;
        }

        private static Task<Smooth.Algebraics.Tuple<QuadFace, Option<ValueOrError<Disposable<Texture2D>>>>> GetCuboidTexture(QuadFace quadFace, Option<Quad> quad)
            => quad.Select(q => q.ModelTexture.ReadableTexture).ToAsync().Select((option, q) => Smooth.Algebraics.Tuple.Create(q, option), quadFace);

        private static Disposable<Texture2D> GetTextureOrDefault(Option<ValueOrError<Disposable<Texture2D>>> texture, QuadFace quadFace)
            => texture.SelectMany(voe => voe.ToOption()).ValueOr(GetDefaultTexture, quadFace);

        private static Disposable<Texture2D> GetDefaultTexture(QuadFace quadFace)
        {
            if (DefaultTextures.ContainsKey(quadFace))
            {
                return Disposable<Texture2D>.Borrow(DefaultTextures[quadFace], _ => { });
            }

            var texturePath = DEFAULT_TEXTURE_FACE_NAME + quadFace.ToString().ToLower();

            var texture = DefaultTextures[quadFace] = (Texture2D)Resources.Load(texturePath, typeof(Texture2D));
            return Disposable<Texture2D>.Borrow(texture, _ => { });
        }

        private static void CompressAtlas(Texture2D texture)
        {
            if (texture.format == TextureFormat.DXT1 || texture.format == TextureFormat.DXT5)
                return;
            
            //Compressing to DXT and making nonreadable. 
            texture.Compress(false);
            texture.Apply(true, true);
        }

//        private static async void UploadAtlas(Texture2D atlas, Dictionary<QuadFace, Rect> uvs, Bounds bounds, ModelInformation modelInformation)
//        {
//            if (!modelInformation.IsEditable)
//                return;
//
//            await TextureUploader.UploadReadableArgbTexture(atlas)
//                .ContinueWithAsync((uri, uvsAndbounds) => new CuboidModelWithAtlas(uri, uvsAndbounds.Item1, uvsAndbounds.Item2), Smooth.Algebraics.Tuple.Create(uvs, bounds))
//                .ContinueWithAsync(MetaRepository.UpdateModelInformationWithAtlas, modelInformation,
//                    $"Can't upload {nameof(ModelInformation)} with atlas").ConfigureAwait(false);
//        }

        private static Disposable<Dictionary<QuadFace, Texture2D>> CreateTemporaryTextureDictionary(Dictionary<QuadFace, Disposable<Texture2D>> textures)
        {
            var dictionary = DictionaryPool<QuadFace, Texture2D>.Instance.BorrowDisposable();
            textures.Slinq()
                .Select(pair => new KeyValuePair<QuadFace, Texture2D>(pair.Key, pair.Value.value))
                .AddTo(dictionary);
            return dictionary;
        }

        private struct TexturesLoadingResult : IDisposable
        {
            private readonly Disposable<Dictionary<QuadFace, Disposable<Texture2D>>> _textures;
            public readonly bool AllTexturesAreLoaded;

            public Dictionary<QuadFace, Disposable<Texture2D>> Textures => _textures.value;

            public TexturesLoadingResult(Disposable<Dictionary<QuadFace, Disposable<Texture2D>>> textures, bool allTexturesAreLoaded)
            {
                _textures = textures;
                AllTexturesAreLoaded = allTexturesAreLoaded;
            }

            public void Dispose()
            {
                _textures.value.Values.Slinq().ForEach(disposable => disposable.Dispose());
                _textures.Dispose();
            }
        }
    }
}