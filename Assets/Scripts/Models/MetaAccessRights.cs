﻿using System;

namespace Start.RightsAndAccess
{
    [Flags]
    public enum MetaAccessRights
    {
        None = 0,
        Read = 1 << 0,
        Edit = 1 << 1,
        Share = 1 << 2,
        Manage = 1 << 3,
        All = 1 << 4
    }
}