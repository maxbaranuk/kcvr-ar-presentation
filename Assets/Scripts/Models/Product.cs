﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Start.Settings;
using UnityEngine;
using Utils.Collections;
using Utils.Math;

namespace Meta.DataClasses.Merchandising.Products
{
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public class Product : AssetMeta
    {
        /* TODO: 
         *  Users write some custom identifier into UPC field in JDA, but it's
         *  not in the standard UPC format. In the app, we identify 'Identifier' as UPC
         *  from JDA, ie NOT the standardised formats (UPC-A, UPC-E).
         *  
         *  Which formats do we need to support?
         */

        /// <summary>
        /// Product Identifier or UPC
        /// </summary>
        [JsonProperty("identifier", Required = Required.Always)]
        public string Identifier { get; set; }

        [JsonProperty("width")]
        public float Width { get; set; }

        [JsonProperty("height")]
        public float Height { get; set; }

        [JsonProperty("depth")]
        public float Depth { get; set; }

        [JsonProperty("unitOfLength")]
        [JsonConverter(typeof(StringEnumConverter))]
        private UnitOfLength UnitOfLength { get; set; } = UnitOfLength.METRE;

        [JsonProperty("supplier")]
        public string Supplier { get; set; } = string.Empty;

        [JsonProperty("brand")]
        public string Brand { get; set; } = string.Empty;

        [JsonProperty("category")]
        public string Category { get; set; } = string.Empty;

        [JsonProperty("subCategory")]
        public string SubCategory { get; set; } = string.Empty;

        public Vector3 Size
        {
            get
            {
                if (Width.NearEquals(0) && Height.NearEquals(0) && Depth.NearEquals(0))
                    throw new Exception($"No info about '{nameof(Width)}', '{nameof(Height)}' and '{nameof(Depth)}' in meta. " +
                                        $"Display name: '{DisplayName}', link: '{SelfUri}'");
                return new Vector3(Width, Height, Depth) * UnitOfLengthInMetres();
            }
        }

        public float UnitOfLengthInMetres()
        {
            return UnitOfMeasurementMath.GetMultiplierFromUnitOfLength(UnitOfLength);
        }

        public override string ToString()
        {
            return $"Product[name={DisplayName},\n" +
                   $"identifier={Identifier},\n" +
                   $"thumb128={Thumb128},\n" +
                   $"thumb256={Thumb256},\n" +
                   $"thumb512={Thumb512},\n" +
                   $"links={Links.AsString()}]";
        }
    }
}
