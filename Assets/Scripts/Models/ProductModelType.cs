﻿namespace Merchandising.Products.Pool
{
    public enum ProductModelType
    {
        Model3D,
        ModelCuboid,
        ModelDummy
    }
}