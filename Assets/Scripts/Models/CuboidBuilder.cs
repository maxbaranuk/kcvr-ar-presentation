﻿using System;
using System.Collections.Generic;
using System.Linq;
using Meta.DataClasses.Geometry;
using UnityEngine;

namespace Scene.SceneBuilding.Models
{
    public sealed class CuboidBuilder
    {
        private readonly List<Vector3> _vertices = new List<Vector3>();
        private readonly List<Vector2> _uvs = new List<Vector2>();
        private readonly List<Vector3> _normals = new List<Vector3>();
        private readonly List<int> _triangleIndices = new List<int>();

        private static readonly Rect DefaultUvRect = new Rect(0, 0, 1, 1);

        private static QuadFace[] _allFaces;
        private static QuadFace[] AllFaces => _allFaces ?? (_allFaces = Enum.GetValues(typeof(QuadFace)).Cast<QuadFace>().ToArray());

        private static Dictionary<QuadFace, Rect> _stubUvs;
        private static Dictionary<QuadFace, Rect> _stubUvsWithoutTop;

        private static readonly Dictionary<QuadFace, Rect> StubUvs =
            _stubUvs ?? (_stubUvs = AllFaces.ToDictionary(side => side, _ => DefaultUvRect));

        private static readonly Dictionary<QuadFace, Rect> StubUvsWithoutTop =
            _stubUvsWithoutTop ?? (_stubUvsWithoutTop = AllFaces.Where(side => side != QuadFace.TOP).ToDictionary(side => side, _ => DefaultUvRect));

        public static CuboidBuilder BuildWithoutTop(Vector3 size)
            => new CuboidBuilder().AddCuboid(new Bounds(Vector3.zero, size), StubUvsWithoutTop);

        public CuboidBuilder() { }

        public CuboidBuilder(Vector3 size)
        {
            AddCuboid(size, StubUvs);
        }

        public CuboidBuilder(Bounds bounds, Dictionary<QuadFace, Rect> uvs)
        {
            var checkedUvs = new Dictionary<QuadFace, Rect>();

            foreach (var face in AllFaces)
            {
                checkedUvs[face] = uvs.ContainsKey(face) ? uvs[face] : StubUvs[face];
            }
            AddCuboid(bounds, checkedUvs);
        }

        public CuboidBuilder(Vector3 size, Dictionary<QuadFace, Rect> uvs) : this(new Bounds(Vector3.zero, size), uvs)
        {
        }

        public CuboidBuilder AddCuboid(Bounds bounds)
        {
            return AddCuboid(bounds, StubUvs);
        }

        private CuboidBuilder AddCuboid(Vector3 size, Dictionary<QuadFace, Rect> uvs)
        {
            return AddCuboid(new Bounds(Vector3.zero, size), uvs);
        }

        private CuboidBuilder AddCuboid(Bounds bounds, Dictionary<QuadFace, Rect> uvs)
        {
            var size = bounds.size;
            var offset = bounds.center;

            var right = Vector3.right * size.x;
            var up = Vector3.up * size.y;
            var forward = Vector3.forward * size.z;
            var extents = bounds.extents;

            foreach (var uv in uvs)
            {
                if (uv.Key == QuadFace.FRONT)
                    CreateQuad(new Vector3(-extents.x, -extents.y, -extents.z) + offset, right, up, uv.Value);

                if (uv.Key == QuadFace.RIGHT)
                    CreateQuad(new Vector3(extents.x, -extents.y, -extents.z) + offset, forward, up, uv.Value);

                if (uv.Key == QuadFace.BACK)
                    CreateQuad(new Vector3(extents.x, -extents.y, extents.z) + offset, -right, up, uv.Value);

                if (uv.Key == QuadFace.LEFT)
                    CreateQuad(new Vector3(-extents.x, -extents.y, extents.z) + offset, -forward, up, uv.Value);

                if (uv.Key == QuadFace.BOTTOM)
                    CreateQuad(new Vector3(-extents.x, -extents.y, extents.z) + offset, right, -forward, uv.Value);

                if (uv.Key == QuadFace.TOP)
                    CreateQuad(new Vector3(-extents.x, extents.y, -extents.z) + offset, right, forward, uv.Value);
            }

            return this;
        }

        public CuboidBuilder AddQuad(Vector3 origin, Vector3 width, Vector3 height)
        {
            CreateQuad(origin, width, height, DefaultUvRect);
            return this;
        }

        private void CreateQuad(Vector3 origin, Vector3 width, Vector3 height, Rect uvRect)
        {
            var normal = Vector3.Cross(width, height).normalized;

            var leftBottomIndex = AddVertex(origin, new Vector2(uvRect.xMin, uvRect.yMin), normal);
            var leftTopIndex = AddVertex(origin + height, new Vector2(uvRect.xMin, uvRect.yMax), normal);
            var rightTopIndex = AddVertex(origin + height + width, new Vector2(uvRect.xMax, uvRect.yMax), normal);
            var rightBottomIndex = AddVertex(origin + width, new Vector2(uvRect.xMax, uvRect.yMin), normal);

            AddTriangle(leftBottomIndex, leftTopIndex, rightTopIndex);
            AddTriangle(leftBottomIndex, rightTopIndex, rightBottomIndex);
        }

        public Mesh Build()
        {
            var mesh = new Mesh
            {
                vertices = _vertices.ToArray(),
                normals = _normals.ToArray(),
                uv = _uvs.ToArray(),
                triangles = _triangleIndices.ToArray()
            };
            mesh.RecalculateBounds();

            return mesh;
        }

        /// <returns>Returns vertex's id</returns>
        private int AddVertex(Vector3 position, Vector2 uv, Vector3 normal)
        {
            _vertices.Add(position);
            _uvs.Add(uv);
            _normals.Add(normal);

            return _vertices.Count - 1;
        }

        private void AddTriangle(int index1, int index2, int index3)
        {
            _triangleIndices.AddRange(new[] { index1, index2, index3 });
        }
    }
}
