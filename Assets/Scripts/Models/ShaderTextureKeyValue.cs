﻿using Newtonsoft.Json;

namespace Meta.DataClasses.Models
{
    public class ShaderTextureKeyValue
    {
        [JsonProperty("shaderTextureKey")]
        public string Key { get; private set; }

        [JsonProperty("shaderTextureValue")]
        public string Value { get; private set; }
    }
}