﻿using System;

namespace Start.RightsAndAccess 
{
    [Flags]
    public enum AccessRights
    {
        None = 0,
        View = 1 << 0,
        Edit = 1 << 1,
        Admin = 1 << 2 | Edit
    }

    public static class AccessRightsExtensions
    {
        public static bool HasAccessRights(this AccessRights accessRights, AccessRights neededAccessRights)
            => (accessRights & neededAccessRights) == neededAccessRights;

        public static bool HasAccessRights(this MetaAccessRights accessRights, MetaAccessRights neededAccessRights)
            => (accessRights & neededAccessRights) == neededAccessRights;
    }
}
