﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ContentTransformation.ShadersWorkers.StandardUnlitShader;
using Meta.DataClasses.Models;
using Models;
using ShadersWorkers.StandardUnlitShader;
using Smooth.Algebraics;
using Smooth.Foundations.PatternMatching.GeneralMatcher;
using Smooth.Slinq;
using UnityEngine;
using Utils.Algebraic;
using Utils.Collections;

namespace Scene.SceneBuilding.Models
{
    using BuilderDelegate = Smooth.Delegates.DelegateFunc<MaterialInformation, Dictionary<string, ModelTexture>, Task<Material>>;

    public static class MaterialBuilder
    {
        private const string LEGACY_REFLECTIVE_COLOR = "_ReflectColor"; // From "Legacy Shaders/Reflective/Diffuse"

        private static readonly Shader ColoredTransparentShader = Shader.Find("Unlit/Transparent Colored");

        private static readonly Dictionary<PredefinedShader, BuilderDelegate> _materialsBuilders = new Dictionary<PredefinedShader, BuilderDelegate>
        {
            {PredefinedShader.UNKNOWN, (information, textures) => CreateMaterial(information, textures, StandardShaderBlendMode.Opaque)},
            {PredefinedShader.DIFFUSE_SHADER, (information, textures) => CreateMaterial(information, textures, StandardShaderBlendMode.Opaque)},
            {PredefinedShader.TRANSPARENT_SHADER, (information, textures) => CreateMaterial(information, textures, StandardShaderBlendMode.Transparent)},
            {PredefinedShader.REFLECTIVE_SHADER, (information, textures) => CreateMaterial(information, textures, StandardShaderBlendMode.Opaque)},
            {PredefinedShader.UNLIT_SHADER, CreateColoredTransparentMaterial},
            {PredefinedShader.UNLIT_TRANSPARENT_SHADER, CreateColoredTransparentMaterial},
            {PredefinedShader.COLORED_TRANSPARENT_SHADER, CreateColoredTransparentMaterial}
        };

        private static Texture2D _missingTexture;

        private static Texture2D MissingTexture
        {
            get
            {
                if (_missingTexture == null)
                {
                    _missingTexture = new Texture2D(1, 1);
                    _missingTexture.SetPixel(0, 0, Color.magenta);
                    _missingTexture.Apply(true);
                }
                return _missingTexture;
            }
        }

        public static async Task<Material> Build(MaterialInformation materialInformation, Dictionary<string, ModelTexture> textureDictionary)
        {
            var materialBuilder = _materialsBuilders.GetOr(materialInformation.PredefinedShader, _materialsBuilders[PredefinedShader.UNKNOWN]);
            return await materialBuilder(materialInformation, textureDictionary);
        }

        // TODO: Should be replaced with Standard Unlit shader with Cutout rendering mode
        private static async Task<Material> CreateColoredTransparentMaterial(MaterialInformation materialInformation, Dictionary<string, ModelTexture> textureDictionary)
        {
            var mainTexture = await FindTexture(materialInformation, textureDictionary, StandardShaderConstants.MAIN_TEXTURE_PROPERTY_NAME);
            var material = new Material(ColoredTransparentShader) {mainTexture = mainTexture.ValueOr((Texture2D) null), color = Color.white};
            return material;
        }

        public static Material CreateEditableTextureMaterial(Texture texture, Color color)
        {
            var standardMaterialParams = StandardShaderProperties.CreateDefault()
                .SetAsTransparent()
                .SetMainColor(color)
                .SetMainTexture(texture);
            return StandardShaderSetter.CreateMaterial(standardMaterialParams);
        }

        private static async Task<Material> CreateMaterial(
            MaterialInformation materialInformation,
            Dictionary<string, ModelTexture> textureDictionary,
            StandardShaderBlendMode blendMode)
        {
            var mainTextureTask = FindTexture(materialInformation, textureDictionary, StandardShaderConstants.MAIN_TEXTURE_PROPERTY_NAME);
            var cubemapTextureTask = FindTexture(materialInformation, textureDictionary, StandardShaderConstants.REFLECTION_CUBE_PROPERTY_NAME);
            var mainTexture = await mainTextureTask;
            var cubemapTexture = await cubemapTextureTask;
            var mainColor = FindColor(materialInformation, StandardShaderConstants.MAIN_COLOR_PROPERTY_NAME);
            var specularColor = FindColor(materialInformation, LEGACY_REFLECTIVE_COLOR);

            var standardMaterialParams = StandardShaderProperties.CreateDefault();
            blendMode.Match()
                .With(StandardShaderBlendMode.Opaque).Do(_ => standardMaterialParams.SetAsOpaque())
                .With(StandardShaderBlendMode.Cutout).Do(_ => standardMaterialParams.SetAsCutout(0.5f)) // TODO: get from material after implementation on Cloud
                .With(StandardShaderBlendMode.Transparent).Do(_ => standardMaterialParams.SetAsTransparent())
                .Exec();

            mainTexture.ForEach((t, p) => p.SetMainTexture(t), standardMaterialParams);
            mainColor.ForEach((c, p) => p.SetMainColor(c), standardMaterialParams);
            Smooth.Algebraics.Tuple.Create(cubemapTexture, specularColor).Strict()
                .ForEach((cubemapAndColor, p) => p.SetSpecular(cubemapAndColor.Item2, cubemapAndColor.Item1), standardMaterialParams);

            return StandardShaderSetter.CreateMaterial(standardMaterialParams);
        }

        private static Task<Option<Texture2D>> FindTexture(MaterialInformation materialInformation, Dictionary<string, ModelTexture> textureDictionary, string textureKey)
        {
            return materialInformation.ShaderTextures.Slinq()
                .FirstOrNone((pair, key) => pair.Key == key, textureKey)
                .Select((t, tDic) => GetTextureOrDefault(tDic, t.Value), textureDictionary)
                .Cata(t => t.ContinueWith(task => task.Result.ToSome()), Task.FromResult(Option<Texture2D>.None));
        }

        private static Option<Color> FindColor(MaterialInformation materialInformation, string colorKey)
        {
            return materialInformation.ShaderColors.Slinq()
                .FirstOrNone((pair, key) => pair.Key == key, colorKey)
                .Select(c => c.Value);
        } 

        private static async Task<Texture2D> GetTextureOrDefault(Dictionary<string, ModelTexture> textures, string textureKey)
        {
            var textureResult = await textures.GetValueOrError(textureKey).ContinueWithAsync(t => t.Texture);
            if (textureResult.IsError)
            {
//                typeof(MaterialBuilder).LogError($"Error while loading texture in {nameof(MaterialBuilder)}: {textureResult.Error}");
                return MissingTexture;
            }
            return textureResult.Value;
        }
    }
}