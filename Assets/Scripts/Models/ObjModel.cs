using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Utils.String;

namespace Scene.SceneBuilding.Models
{
    internal class ObjModel
    {
        private class SubMesh
        {
            public SubMesh(string materialName, int startFace, int endFace)
            {
                MaterialName = materialName;
                StartFace = startFace;
                EndFace = endFace;
            }

            public int StartFace { get; set; }
            public int EndFace { get; set; }
            public string MaterialName { get; set; }
        }

        private const string Vertices = "v";
        private const string TextureVertices = "vt";
        private const string VertexNormals = "vn";
        private const string Faces = "f";
        private const string NewMaterial = "usemtl";

        protected float[][] vertices;
        protected float[][] vertexnormals;
        protected float[][] vertextextures;

        protected int[][] faces;

        private List<SubMesh> SubMeshes;

        /// <summary>
        /// The OrderOfMaterials property stores parsed materials from raw OBJ in right order
        /// </summary>
        /// <value>The OrderOfMaterials property gets the order of materials from raw OBJ</value>
        public Dictionary<int, string> OrderOfMaterials { get; private set; }

        public ObjModel ()
        {
            OrderOfMaterials = new Dictionary<int, string>();
        }

        public ObjModel(string source) : this ()
        {
            parse(source);
        }

        public float[][] getVertices()
        {
            return vertices;
        }

        public float[][] getNormals()
        {
            return vertexnormals;
        }

        public float[][] getUVs()
        {
            return vertextextures;
        }

        public int[][] getFaces()
        {
            return faces;
        }

        public int NumSubMeshes()
        {
            return SubMeshes.Count;
        }

        public int[][] getFaces(int SubMeshID)
        {
            SubMesh submesh = SubMeshes[SubMeshID];
            int numSubMeshFaces = submesh.EndFace - submesh.StartFace;

            int[][] submeshFaces = new int[numSubMeshFaces][];
            int index = 0;
            for (int i = submesh.StartFace; i < submesh.EndFace; i++)
            {
                submeshFaces[index] = faces[i];
                index++;
            }

            return submeshFaces;
        }

        public void parse(string objContents)
        {
            SubMeshes = new List<SubMesh>();

            ArrayList vertArray = new ArrayList();
            ArrayList vertnormalArray = new ArrayList();
            ArrayList verttextureArray = new ArrayList();

            ArrayList vertexfaceArray = new ArrayList();
            ArrayList vertexnormalfaceArray = new ArrayList();
            ArrayList vertextexturefaceArray = new ArrayList();

            List<string> objLines = new List<string>();
            objContents.SplitToLines(objLines);

            foreach (string objLine in objLines)
            {
                string trimmedObjLine = objLine.Trim();

                if (string.IsNullOrEmpty(trimmedObjLine) == true)
                {
                    // Empty line
                    continue;
                }

                if (trimmedObjLine.StartsWith("#") == true)
                {
                    // Comment line
                    continue;
                }

                string keyword = string.Empty;
                List<string> keywordParams = new List<string>();
                SplitLine(trimmedObjLine, out keyword, ref keywordParams);

                string[] pieces = trimmedObjLine.Split(new char[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries);

                switch (pieces[0])
                {
                    case Vertices: // Vertices
                        vertArray.Add(new [] { pieces[1].ParseToInvariantCulture(), pieces[2].ParseToInvariantCulture(), pieces[3].ParseToInvariantCulture() });
                        break;
                    case TextureVertices: // Texture Vertices
                        verttextureArray.Add(new [] { pieces[1].ParseToInvariantCulture(), pieces[2].ParseToInvariantCulture() });
                        break;
                    case VertexNormals: // Vertex Normals
                        vertnormalArray.Add(new [] { pieces[1].ParseToInvariantCulture(), pieces[2].ParseToInvariantCulture(), pieces[3].ParseToInvariantCulture() });
                        break;
                    case Faces: // Faces
                        // Faces have 3 or more vertexes.  Since Unity wants triangles, we assume that the face is convex
                        // and split it into triangles.
                        for (int faceIndex = 3; faceIndex < pieces.Length; faceIndex++)
                        {
                            string[][] face = new string[3][];
                            face[0] = pieces[1].Split('/');
                            face[1] = pieces[faceIndex - 1].Split('/');
                            face[2] = pieces[faceIndex].Split('/');

                            vertexfaceArray.Add(new int[3] { int.Parse(face[0][0]), int.Parse(face[1][0]), int.Parse(face[2][0]) });
                            if ((face[0].Length > 1) && (face[0][1] != ""))
                            {
                                vertextexturefaceArray.Add(new int[3] { int.Parse(face[0][1]), int.Parse(face[1][1]), int.Parse(face[2][1]) });
                            }
                            if ((face[0].Length > 2) && (face[0][2] != ""))
                            {
                                vertexnormalfaceArray.Add(new int[3] { int.Parse(face[0][2]), int.Parse(face[1][2]), int.Parse(face[2][2]) });
                            }
                        }
                        break;
                    case NewMaterial:
                        // Set the face count for the last submesh
                        int StartFace = vertexfaceArray.Count;
                        if (SubMeshes.Count > 0)
                        {
                            SubMeshes[SubMeshes.Count - 1].EndFace = StartFace;
                        }

                        // Begin a submesh
                        string materialName = (pieces.Length > 1) ? pieces[1] : "default";
                        OrderOfMaterials.Add(OrderOfMaterials.Count, materialName);
                        SubMesh submesh = new SubMesh(materialName, StartFace, 0);
                        SubMeshes.Add(submesh);
                        break;
                    default:
                        //All unsopported and commented lines (s, g, etc)
                        break;
                }
            }

            // Set the last face of the last submesh
            if (SubMeshes.Count > 0)
            {
                SubMeshes[SubMeshes.Count - 1].EndFace = vertexfaceArray.Count;
            }

            vertices = new float[vertexfaceArray.Count * 3][];
            if (vertexnormalfaceArray.Count > 0)
            {
                vertexnormals = new float[vertexfaceArray.Count * 3][];
            }
            else
            {
                vertexnormals = null;
            }
            if (vertextexturefaceArray.Count > 0)
            {
                vertextextures = new float[vertexfaceArray.Count * 3][];
            }
            else
            {
                vertextextures = null;
            }
            faces = new int[vertexfaceArray.Count][];
            for (int index = 0; index < vertexfaceArray.Count; index++)
            {
                faces[index] = new int[3] { 0, 0, 0 };
                int[] facevertex = (int[])vertexfaceArray[index];

                vertices[(index * 3) + 0] = (float[])vertArray[facevertex[0] - 1];
                vertices[(index * 3) + 1] = (float[])vertArray[facevertex[1] - 1];
                vertices[(index * 3) + 2] = (float[])vertArray[facevertex[2] - 1];

                if ((vertextextures != null) && (vertextexturefaceArray.Count > index))
                {
                    int[] facetexture = (int[])vertextexturefaceArray[index];
                    vertextextures[(index * 3) + 0] = (float[])verttextureArray[facetexture[0] - 1];
                    vertextextures[(index * 3) + 1] = (float[])verttextureArray[facetexture[1] - 1];
                    vertextextures[(index * 3) + 2] = (float[])verttextureArray[facetexture[2] - 1];
                }

                if ((vertexnormals != null) && (vertexnormalfaceArray.Count > index))
                {
                    int[] facenormal = (int[])vertexnormalfaceArray[index];
                    vertexnormals[(index * 3) + 0] = (float[])vertnormalArray[facenormal[0] - 1];
                    vertexnormals[(index * 3) + 1] = (float[])vertnormalArray[facenormal[1] - 1];
                    vertexnormals[(index * 3) + 2] = (float[])vertnormalArray[facenormal[2] - 1];
                }

                faces[index][0] = (index * 3) + 0;
                faces[index][1] = (index * 3) + 1;
                faces[index][2] = (index * 3) + 2;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="line"></param>
        /// <param name="keyword"></param>
        /// <param name="paramList"></param>
        private void SplitLine(string line, out string keyword, ref List<string> paramList)
        {
            keyword = string.Empty;

            if (paramList == null)
            {
                paramList = new List<string>();
            }
            else if (paramList.Count != 0)
            {
                paramList.Clear();
            }

            if ((string.IsNullOrEmpty(line) == true) ||
                (line.IsNullOrWhiteSpace() == true))
            {
                return;
            }

            int index = 0;
            keyword = GetNextString(line, ref index);

            string nextStr = string.Empty;
            bool finished = false;
            while (finished == false)
            {
                nextStr = GetNextString(line, ref index);
                if (string.IsNullOrEmpty(nextStr) == false)
                {
                    paramList.Add(nextStr);
                }
                else
                {
                    finished = true;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="line"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private string GetNextString(string line, ref int index)
        {
            // Skip whitespace if any
            while ((index < line.Length) &&
                   (char.IsWhiteSpace(line[index]) == true))
            {
                index++;
            }

            if (index == line.Length)
            {
                // Nothing found just more whitespace
                return string.Empty;
            }

            // Found something and we are not at the end of the string.
            // Find the next whitespace character or end of string to
            // determine the beginning and the end of the string item.
            int startIndex = index;
            while ((index < line.Length) &&
                   (char.IsWhiteSpace(line[index]) == false))
            {
                index++;
            }
            int endIndex = index;
            int itemLength = endIndex - startIndex;

            return line.Substring(startIndex, itemLength);
        }

    } // class Model

    internal static class ModelExtentions
    {
        /// <summary>
        /// Extension method that turns a string to a stream.
        /// </summary>
        /// <param name="str">The string to be converted.</param>
        /// <returns>The corresponding stream.</returns>
        public static Stream ToStream(this string str)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(str);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Extension method that splits a string made of one or more lines,
        /// placing each line in a list of strings.
        /// </summary>
        /// <param name="stringToSplit">The string that must be split.</param>
        /// <param name="linesList">The list that will receive the lines.</param>
        public static void SplitToLines(this string stringToSplit, List<string> linesList)
        {
            if (string.IsNullOrEmpty(stringToSplit) == false)
            {
                // Convert the string to a stream and read line by line
                Stream resultsStream = stringToSplit.ToStream();
                using (TextReader resReader = new StreamReader(resultsStream))
                {
                    string strLine = resReader.ReadLine();
                    while (strLine != null)
                    {
                        linesList.Add(strLine);
                        strLine = resReader.ReadLine();
                    }
                }
            }
        }

        /// <summary>
        /// Check if a string is null or is made entirely of whitespace characters.
        /// </summary>
        /// <param name="stringContent">The string to check.</param>
        /// <returns>True if the string is null or made entirely of whitespace, false otherwise.</returns>
        public static bool IsNullOrWhiteSpace(this string stringContent)
        {
            if (stringContent == null)
            {
                return true;
            }
            return string.IsNullOrEmpty(stringContent.Trim());
        }
    }

} // namespace OBJ

