﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Meta.DataClasses.Models;
using Newtonsoft.Json;
using Smooth.Slinq;
using Start.RightsAndAccess;
using UnityEngine;

namespace Models
{
    public sealed class ModelInformation : Metas.Meta
    {
        [JsonProperty("@type")]
        private string Type => "ModelInformation";

        [JsonProperty("description")]
        public string Description { get; private set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; private set; }

        [JsonProperty("modelSet")]
        public List<Model> ModelSet { get; private set; }

        [JsonProperty("textureMap")]
        public Dictionary<string, ModelTexture> Textures { get; private set; }
        
        public bool IsEditable => _metaAccessRights.HasAccessRights(MetaAccessRights.Edit);

        private MetaAccessRights _metaAccessRights;
        [UsedImplicitly, JsonProperty("@access")]
        private string[] RawAccesses { set { _metaAccessRights = MetaAccessHelper.GetAccess(value); } }

        public Model GetMeshWithInternalTexturesOfQuality(float modelQuality = 1.0f)
        {
            return ModelSet.Slinq()
                .OrderBy((m, quality) => Math.Abs(m.ModelQuality - quality), modelQuality)
                .First();
        }

        public Bounds BoundsMetric => ModelSet.Count > 0 ? ModelSet.First().BoundsMetric : new Bounds(Vector3.zero, Vector3.zero);
    }
}
