﻿using System.Collections.Generic;
using Smooth.Algebraics;
using Smooth.Slinq;
using Utils.Collections;

namespace Start.RightsAndAccess
{
    public static class MetaAccessHelper
    {
        private static readonly Dictionary<string, MetaAccessRights> AccessRights
            = new Dictionary<string, MetaAccessRights>
            {
                {"r", MetaAccessRights.Read},
                {"e", MetaAccessRights.Edit},
                {"s", MetaAccessRights.Share},
                {"m", MetaAccessRights.Manage},
                {"a", MetaAccessRights.All}
            };

        public static MetaAccessRights GetAccess(string[] rawAccess)
            => rawAccess
                .Slinq()
                .Where(role => AccessRights.ContainsKey(role))
                .Select(role => AccessRights[role])
                .Aggregate(MetaAccessRights.None, (a, b) => a | b);

        public static string[] GetAccessString(MetaAccessRights accessRights)
            => AccessRights.Slinq()
                .SelectMany((pair, ar) => ar.HasAccessRights(pair.Value) ? pair.Key.ToSome() : Option<string>.None, accessRights)
                .ToArray();
    }
}