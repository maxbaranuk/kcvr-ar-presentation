﻿using System;
using Meta.JsonConverters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Start.Settings;
using UnityEngine;

namespace Meta.DataClasses.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    [JsonConverter(typeof(ModelConverter))]
    public abstract class Model
    {
        public const string TYPE_PARAMETER_NAME = "@type";
        
        [JsonProperty(TYPE_PARAMETER_NAME)]
        [JsonConverter(typeof (StringEnumConverter))]
        // TODO: make it abstract
        public ModelType Type { get; protected set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("format")]
        public Format Format { get; set; }

        // TODO: remove 'NullValueHandling' when cloud will be ok
        [JsonProperty("modelQuality", NullValueHandling = NullValueHandling.Ignore)]
        public float ModelQuality { get; set; }

        [JsonProperty("bounds"), JsonConverter(typeof (BoundsJsonConverter))]
        protected Bounds Bounds { get; set; }

        public Bounds BoundsMetric => Bounds.ToMetric(UnitOfLength);

        [JsonProperty("scalingPolicy")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ScalingPolicy ScalingPolicy { get; set; }

        [JsonProperty("unitOfLength")]
        [JsonConverter(typeof(StringEnumConverter))]
        public UnitOfLength UnitOfLength { get; set; }

        [JsonProperty("data")]
        [JsonConverter(typeof(UriJsonConverter))]
        public Uri Data { get; set; }

        [JsonProperty("axesConvention")]
        [JsonConverter(typeof(StringEnumConverter))]
        public AxesConvention AxesConvention { get; set; }

        public override string ToString()
        {
            return $"model desc={Description}, format={Format},\n" +
                   $"quality={ModelQuality}, bounds={Bounds}, policy={ScalingPolicy},\n" +
                   $"units={UnitOfLength}";
        }
    }
}