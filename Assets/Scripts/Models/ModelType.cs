﻿namespace Meta.DataClasses.Models
{
    public enum ModelType
    {
        MeshWithInternalTextures,
        Cuboid,
        MeshWithMaterials,
        CuboidWithAtlas
    }
}