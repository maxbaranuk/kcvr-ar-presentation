﻿using System.Collections.Generic;
using System.Linq;
using ContentTransformation.ShadersWorkers.StandardUnlitShader;
using Meta.DataClasses.Geometry;
using ShadersWorkers.StandardUnlitShader;
using Smooth.Algebraics;
using UnityEngine;
using Utils.Collections;

namespace Models
{
    public sealed class CuboidAtlasProvider
    {
        private readonly int[] _primes = { 31, 37, 41, 43, 47, 53 };

        private readonly int _maxAtlasSize;
        private readonly int _padding;

        private readonly Dictionary<int, AtlasResult> _cachedAtlases =
            new Dictionary<int, AtlasResult>();

        public CuboidAtlasProvider(int padding = 2, int maxAtlasSize = 512)
        {
            _maxAtlasSize = maxAtlasSize;
            _padding = padding;
        }

        public AtlasResult GetAtlas(Dictionary<QuadFace, Texture2D> textures)
        {
            var identifier = GetTexturesIdentifier(textures.Select(pair => pair.Value));
            var result = _cachedAtlases.TryGet(identifier).ValueOr(BuilAtlas, textures);
            _cachedAtlases[identifier] = result;
            return result;
        }

        public Material GetMaterialForAtlas(Texture2D atlas)
        {
            if (atlas.format == TextureFormat.ARGB32 || atlas.format == TextureFormat.RGB24)
            {
                atlas.Compress(false);
                atlas.Apply(false, true);
            }

            return StandardShaderSetter.CreateMaterial(StandardShaderProperties.CreateDefault()
                .SetAsCutout(0.5f)
                .SetMainTexture(atlas)
                .EnableLightMapColorSettings()
            );
        }

        private AtlasResult BuilAtlas(Dictionary<QuadFace, Texture2D> textures)
        {
            var allTextures = textures.Values.ToArray();
            var texturAndUvs = BuildTexture(allTextures);
            var quads = textures.Keys.ToArray();
            var rects = texturAndUvs.Item2.ToArray();
            var dict = Enumerable.Range(0, quads.Length).ToDictionary(i => quads[i], i => rects[i]);
            var result = new AtlasResult(texturAndUvs.Item1, dict);
            return result;
        }

        private Tuple<Texture2D, Rect[]> BuildTexture(Texture2D[] textures)
        {
            var atlas = new Texture2D(2, 2, TextureFormat.ARGB32, true);
            var uvs = atlas.PackTextures(textures, _padding, _maxAtlasSize);
            return Tuple.Create(atlas, uvs);
        }

        private int GetTexturesIdentifier(IEnumerable<Texture2D> textures)
        {
            unchecked
            {
                return textures
                    .Select((tex, index) => new {tex, index})
                    .Aggregate(17, (accumulated, indexedTexture)
                        => accumulated*(indexedTexture.tex?.GetInstanceID() ?? _primes[indexedTexture.index]));
            }
        }

        public struct AtlasResult
        {
            public readonly Texture2D Texture;
            public readonly Dictionary<QuadFace, Rect> Uvs;

            public AtlasResult(Texture2D texture, Dictionary<QuadFace, Rect> uvs)
            {
                Texture = texture;
                Uvs = uvs;
            }
        }
    }
}