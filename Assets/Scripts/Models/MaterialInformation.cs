﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Meta.DataClasses.Models
{
    public sealed class MaterialInformation
    {
        [JsonProperty("materialName")]
        public string MaterialName { get; private set; }

        [JsonProperty("predefinedShader")]
        public PredefinedShader PredefinedShader { get; private set; }

        [JsonProperty("shaderColorSet")]
        public List<ShaderColorKeyValue> ShaderColors { get; private set; }

        [JsonProperty("shaderTextureSet")]
        public List<ShaderTextureKeyValue> ShaderTextures { get; private set; }

        [JsonProperty("shaderParamSet")]
        public List<ShaderParameterKeyValue> ShaderParameters { get; private set; }
    }
}