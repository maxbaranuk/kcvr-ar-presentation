﻿using Meta.JsonConverters;
using Newtonsoft.Json;
using UnityEngine;

namespace Meta.DataClasses.Models
{
    public class ShaderColorKeyValue
    {
        [JsonProperty("shaderColorKey")]
        public string Key { get; private set; }

        [JsonProperty("shaderColorValue")]
        [JsonConverter(typeof (ColorNumberConverter))]
        public Color Value { get; private set; }
    }
}