﻿using Newtonsoft.Json;

namespace Meta.DataClasses.Models
{
    public class ShaderParameterKeyValue
    {
        [JsonProperty("shaderParamKey")]
        public string Key { get; private set; }

        [JsonProperty("shaderParamValue")]
        public float Value { get; private set; }
    }
}