﻿using System.Collections.Generic;
using Meta.DataClasses.Geometry;
using Meta.DataClasses.Models;
using Newtonsoft.Json;

namespace Models
{

    [JsonObject(MemberSerialization.OptIn)]
    public class CuboidModel : Model
    {
        [JsonProperty("quad")]
        public List<Quad> Quads { get; set; }
    }
}