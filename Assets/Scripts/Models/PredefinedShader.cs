﻿namespace Meta.DataClasses.Models
{
    public enum PredefinedShader
    {
        UNKNOWN = 0,
        COLORED_TRANSPARENT_SHADER,
        DIFFUSE_SHADER,
        REFLECTIVE_SHADER,
        TRANSPARENT_SHADER,
        UNLIT_SHADER,
        UNLIT_TRANSPARENT_SHADER
    }
}