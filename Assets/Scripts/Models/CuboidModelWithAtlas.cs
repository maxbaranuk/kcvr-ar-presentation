﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Meta.DataClasses.Geometry;
using Newtonsoft.Json;
using Smooth.Foundations.Algebraics;
using UnityEngine;
using Utils.Algebraic;

namespace Meta.DataClasses.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class CuboidModelWithAtlas : Model
    {
        [JsonProperty("uvs")]
        public AtlasUvsCollection Uvs { get; private set; }

        public Task<ValueOrError<Texture2D>> Atlas => AssetProvider.GetTexture(Data);

        public CuboidModelWithAtlas()
        {
            Type = ModelType.CuboidWithAtlas;
            Uvs = new AtlasUvsCollection();
        }

        public CuboidModelWithAtlas(Uri dataUri, Dictionary<QuadFace, Rect> facesDictionary, Bounds modelBounds)
        {
            Data = dataUri;
            Type = ModelType.CuboidWithAtlas;
            Uvs = new AtlasUvsCollection();
            Uvs.FaceToRectDictionary = facesDictionary;
            Format = new Format {FormatFamily = FormatFamily.PNG};
            UnitOfLength = UnitOfLength.METRE;
            Bounds = modelBounds;
        }
    }
}