﻿using System;
using System.Threading.Tasks;
using Meta.JsonConverters;
using Newtonsoft.Json;
using Smooth.Dispose;
using Smooth.Foundations.Algebraics;
using UnityEngine;
using Utils.Algebraic;

namespace Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class ModelTexture
    {
        [JsonProperty("@type")]
        public string Type => "Texture";

        public Task<ValueOrError<Texture2D>> Texture => AssetProvider.GetTexture(Data);
        public Task<ValueOrError<Disposable<Texture2D>>> ReadableTexture 
            => AssetProvider.GetTexture(Data).ContinueWithAsync(texure => Disposable<Texture2D>.Borrow(texure, _ => { }));

        [JsonProperty("data")]
        [JsonConverter(typeof(UriJsonConverter))]
        public Uri Data { get; private set; }

        public override string ToString()
        {
            return $"Data={Data}";
        }
    }
}
