﻿using System.Threading.Tasks;
using Newtonsoft.Json;
using Smooth.Foundations.Algebraics;
using UnityEngine;

namespace Meta.DataClasses.Models
{

    [JsonObject(MemberSerialization.OptIn)]
    public class AssetBundleModel : Model
    {
        public Task<ValueOrError<GameObject>> Prefab => Task.FromResult(ValueOrError<GameObject>.FromValue(new GameObject()));
        // TODO: move getting asset here
    }
}